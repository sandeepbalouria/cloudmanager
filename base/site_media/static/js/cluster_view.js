//deployer view animation not being used right but kept for reference
$("input[name='cluster_type']").change(function(){
	if ($(this).val() == "mn"){
		$('#aws_sn_options').removeClass('bounceInUp');
		$('#aws_sn_options').addClass('animated bounceOutDown');
		$('#aws_mn_options').removeClass('bounceOutRight');
		$('#aws_mn_options').addClass('animated bounceInRight');
		$('#aws_mn_options').css("display", "block");
		$('#aws_sn_options').css("display", "none");

	};
	if ($(this).val() == "sn"){
		$('#aws_mn_options').removeClass('bounceInRight');
		$('#aws_mn_options').addClass('animated bounceOutRight');
		$('#aws_sn_options').removeClass('bounceOutDown');
		$('#aws_sn_options').addClass('animated bounceInUp'); 
		$('#aws_sn_options').css("display", "block");
		$('#aws_mn_options').css("display", "none");
		};
});


/*=====================================================================
Cluster_view
=======================================================================*/
$(function(){


	if($('.cluster_view').length){
		$(".cluster_item").click(function(){
			var cluster_id = this.id;
			var cluster = cluster_id.replace("#","");
			
			if($(cluster_id+'_cluster').hasClass("in")){                           
				if(cluster_view.firstrun_timerid){
					clearTimeout(cluster_view.firstrun_timerid);
				}
				if(cluster_view.clusterServiceStatus_timerid){
					clearTimeout(cluster_view.clusterServiceStatus_timerid);                
				}
				if(cluster_view.systemServiceStatus_timerid){
					clearTimeout(cluster_view.systemServiceStatus_timerid);                
				}
			}
			else{
				if(cluster_view.firstrun_timerid){
					clearTimeout(cluster_view.firstrun_timerid);
				}
				if(cluster_view.clusterServiceStatus_timerid){
					clearTimeout(cluster_view.clusterServiceStatus_timerid);                
				}
				if(cluster_view.systemServiceStatus_timerid){
					clearTimeout(cluster_view.systemServiceStatus_timerid);                
				}
				          
				if(cluster_view.cluster_id){
					$("#"+cluster_view.cluster_id+'_cluster').removeClass("in");
				}				
				cluster_view.cluster_id = cluster;
				cluster_view.cluterFirstrunData();                

			}

		});
		

		$(".ss_button").click(function(){
			var button_id = this.id;
			
			service = button_id.slice(button_id.indexOf("_")+1,button_id.lastIndexOf("_"))

			if($("#"+button_id).text() == 'stop'){				
				$("#pleaseWait").modal('show');
				cluster_view.action = 'stop';
				cluster_view.service = service;
				cluster_view.clusterServiceStart();
			}
			if($("#"+button_id).text() == 'start'){				
				$("#pleaseWait").modal('show');
				cluster_view.action = 'start';
				cluster_view.service = service;
				cluster_view.clusterServiceStart();
			}

		});
		$(".system_ss_button").click(function(){
			var button_id = this.id;
			
			service = button_id.slice(button_id.indexOf("_")+1,button_id.lastIndexOf("_"))

			if($("#"+button_id).text() == 'stop'){
				$("#pleaseWait").modal('show');
				cluster_view.action = 'stop';
				cluster_view.service = service;
				cluster_view.systemServiceStart();
			}
			if($("#"+button_id).text() == 'start'){
				$("#pleaseWait").modal('show');
				cluster_view.action = 'start';
				cluster_view.service = service;
				cluster_view.systemServiceStart();
			}

		});
		$(".cluster_wide_operation").click(function(){
			var action_id = this.id;
			if($("#"+action_id).text() == 'Start'){
				$("#pleaseWait").modal('show');
				cluster_view.action = 'start';
				cluster_view.service = 'all';
				cluster_view.clusterServiceStart();
			}
			if($("#"+action_id).text() == 'Stop'){
				$("#pleaseWait").modal('show');
				cluster_view.action = 'stop';
				cluster_view.service = 'all';
				cluster_view.clusterServiceStart();
			}
			if($("#"+action_id).text() == 'Undeploy'){
				$("#pleaseWait").modal('show');
				cluster_view.action = 'undeploy';
				cluster_view.service = 'all';
				cluster_view.clusterServiceStart();
			}
			if($("#"+action_id).text() == 'Deploy'){
				var $modal = $('#ajax-modal');
				var url = $(this).data("url");
				var cluster_id = $(this).data("cluster");			
				$('body').modalmanager('loading');
				setTimeout(function(){
					$modal.load(
						url,
						$.param({clusterid: cluster_id}),function(){
						$modal.modal();
						});
				}, 1000);
			}
			
		});
		$(".jvmExtraOptions").click(function(){
			var config_id = this.id;
			
			if($("#"+config_id).text() == 'jvmExtraOptions'){
				var $modal = $('#ajax-modal');
				var url = $(this).data("url");
				var cluster_id = $(this).data("cluster");
				var service = $(this).data("service");
				$('body').modalmanager('loading');
				setTimeout(function(){
					$modal.load(
						url,
						$.param({clusterid: cluster_id,service: service}),function(){
						$modal.modal();
						});
				}, 1000);
			}
			
		});
		$(".loggerLevels").click(function(){
			var config_id = this.id;
			
			if($("#"+config_id).text() == 'loggerLevels'){
				var $modal = $('#ajax-modal');
				var url = $(this).data("url");
				var cluster_id = $(this).data("cluster");
				var service = $(this).data("service");
				$('body').modalmanager('loading');
				setTimeout(function(){
					$modal.load(
						url,
						$.param({clusterid: cluster_id,service: service}),function(){
						$modal.modal();
						});
				}, 1000);
			}
			
		});
		
		$(".system_service_status").click(function(){
			cluster_view.systemServiceStatus();
		});
		
		$(document).on('click', '#submit_deploy_platform', function(){
			var $myForm = $("#deployer_modal_form");
			var $modal = $('#ajax-modal');
			$myForm.submit();
			$modal.modal('hide');
		});
	
	};
});

cluster_view = {

	cluster_id: undefined ,
	service: undefined,
	action: undefined,
	tmversion: undefined,
	firstrun_timerid: undefined,
	clusterServiceStatus_timerid: undefined,
	systemServiceStatus_timerid: undefined,


	clusterServiceStatus: function(){

	var url = $('#cluster_service_status').data('url');
	var cluster = {'id':this.cluster_id};    
	$.ajax({
		type: 'GET',
		url: url,
		data: cluster,
		dataType: 'json',
			aysync: true,
			timeout: 30000,
			success: function(data) {
				$.each($(data),function(){
					$.each(this, function(k,v){

						$("#" + cluster.id + "_"+k).text(v);
						if(v == 'running'){ 
							$("#"+cluster.id+"_"+k+"_ss").text('stop')
							$("#"+cluster.id+"_"+k+"_ss").removeClass("btn-green").addClass("btn-red");
							$("#" + cluster.id + "_"+k).removeClass("btn-red").addClass("btn-green");
						}
						if(v == 'stopped'){
							$("#"+cluster.id+"_"+k+"_ss").text('start')
							$("#"+cluster.id+"_"+k+"_ss").removeClass("btn-red").addClass("btn-green");
							$("#" + cluster.id + "_"+k).removeClass("btn-green").addClass("btn-red");
						} 
						if(v == 'Undeployed'){ 
							$("#" + cluster.id + "_"+k).removeClass("btn-green").addClass("btn-red");
							$("#"+cluster.id+"_"+k+"_ss").removeClass("btn-green").addClass("btn-red");
						} 
						if(v == 'Deployed'){ 
							$("#" + cluster.id + "_"+k).removeClass("btn-red").addClass("btn-green");
							$("#"+cluster.id+"_"+k+"_ss").removeClass("btn-red").addClass("btn-green");
						}
					});
				});   
				cluster_view.clusterServiceStatus_timerid = setTimeout($.proxy(cluster_view.clusterServiceStatus,cluster_view), 30000);
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			setTimeout($.proxy(cluster_view.clusterServiceStatus,cluster_view), 60000);
		}
	});
},
	systemServiceStatus: function(){
    
	var url = $('#system_service_status_'+this.cluster_id).data('url');
	var cluster = {'id':this.cluster_id};    
	$.ajax({
		type: 'GET',
		url: url,
		data: cluster,
		dataType: 'json',
			aysync: true,
			timeout: 30000,
			success: function(data) {
				$.each($(data),function(){
					$.each(this, function(k,v){

						$("#" + cluster.id + "_"+k).text(v);
						if(v == 'running'){ 
							$("#"+cluster.id+"_"+k+"_ss").text('stop')
							$("#"+cluster.id+"_"+k+"_ss").removeClass("btn-green").addClass("btn-red");
							$("#" + cluster.id + "_"+k).removeClass("btn-red").addClass("btn-green");
						}
						if(v == 'stopped'){
							$("#"+cluster.id+"_"+k+"_ss").text('start')
							$("#"+cluster.id+"_"+k+"_ss").removeClass("btn-red").addClass("btn-green");
							$("#" + cluster.id + "_"+k).removeClass("btn-green").addClass("btn-red");
						} 
						
					});
				});   
				cluster_view.systemServiceStatus_timerid = setTimeout($.proxy(cluster_view.systemServiceStatus,cluster_view), 45000);
		},
		error: function (XMLHttpRequest, textStatus, errorThrown) {
			setTimeout($.proxy(cluster_view.systemServiceStatus,cluster_view), 60000);
		}
	});
},
	cluterFirstrunData: function(){

	var l = Ladda.create( document.querySelector( "#tm_deploy_button_"+this.cluster_id ) );    
	var cluster = {'id':this.cluster_id};
	var url = $('#accordion').data('url');
	$.ajax({
		type: 'GET',
		url: url,
		data: cluster,
		dataType: 'json',
			aysync: true,
			timeout: 45000,
			success: function(data) {
				if(data.ready == 'yes'){                    
					l.stop();
					cluster_view.clusterServiceStatus();
					
				}
				if(data.ready == 'no'){                    
					l.start();
					cluster_view.firstrun_timerid = setTimeout($.proxy(cluster_view.cluterFirstrunData,cluster_view), 45000);
				}
	},
	error: function (XMLHttpRequest, textStatus, errorThrown) {
		setTimeout($.proxy(cluster_view.cluterFirstrunData,cluster_view), 60000);
	}
});
},
	clusterServiceStart: function(){
		var request_data = {'cluster_id': this.cluster_id,'service': this.service,'action': this.action};   

		var url = $('#cluster_service_action').data('url');
		$.ajax({
			type: 'GET',
			url: url,
			data: request_data,
			dataType: 'json',
			aysync: true,
			timeout: 60000,
			success: function(data) {
				$.each($(data),function(){
					$.each(this, function(k,v){
						if(v == 'running'){
							$("#" + request_data.cluster_id + "_"+k).text('running');
							$("#" + request_data.cluster_id + "_"+k+"_ss").text('stop');
							$("#" + request_data.cluster_id + "_"+k).removeClass("btn-red").addClass("btn-green");
							$("#" + request_data.cluster_id + "_"+k+"_ss").removeClass("btn-green").addClass("btn-red");
						}
						if(v == 'stopped'){
							$("#" + request_data.cluster_id + "_"+k).text('stopped');
							$("#" + request_data.cluster_id + "_"+k+"_ss").text('start');
							$("#" + request_data.cluster_id + "_"+k).removeClass("btn-green").addClass("btn-red");
							$("#" + request_data.cluster_id + "_"+k+"_ss").removeClass("btn-red").addClass("btn-green");
						}
					});
				});
				$("#pleaseWait").modal('hide');

			}
	});
},
	clusterDeploy: function(){
		var request_data = {'cluster_id': this.cluster_id,'version': this.tmversion};   

		var url = $('#cluster_service_action').data('url');
		$.ajax({
			type: 'GET',
			url: url,
			data: request_data,
			dataType: 'json',
			aysync: true,
			timeout: 60000,
			success: function(data) {
				$.each($(data),function(){
					$.each(this, function(k,v){
						if(v == 'running'){
							$("#" + request_data.cluster_id + "_"+k).text('running');
							$("#" + request_data.cluster_id + "_"+k+"_ss").text('stop');
							$("#" + request_data.cluster_id + "_"+k).removeClass("btn-red").addClass("btn-green");
							$("#" + request_data.cluster_id + "_"+k+"_ss").removeClass("btn-green").addClass("btn-red");
						}
						if(v == 'stopped'){
							$("#" + request_data.cluster_id + "_"+k).text('stopped');
							$("#" + request_data.cluster_id + "_"+k+"_ss").text('start');
							$("#" + request_data.cluster_id + "_"+k).removeClass("btn-green").addClass("btn-red");
							$("#" + request_data.cluster_id + "_"+k+"_ss").removeClass("btn-red").addClass("btn-green");
						}
					});
				});
				$("#pleaseWait").modal('hide');
				
			}
	});
},
	systemServiceStart: function(){
		var request_data = {'cluster_id': this.cluster_id,'service': this.service,'action': this.action};   

		var url = $('#system_service_action').data('url');
		$.ajax({
			type: 'GET',
			url: url,
			data: request_data,
			dataType: 'json',
			aysync: true,
			timeout: 30000,
			success: function(data) {
				if(data.status == 'running'){
					$("#" + request_data.cluster_id + "_"+request_data.service).text('running');
					$("#" + request_data.cluster_id + "_"+request_data.service+"_ss").text('stop');
					$("#" + request_data.cluster_id + "_"+request_data.service).removeClass("btn-red").addClass("btn-green");
					$("#" + request_data.cluster_id + "_"+request_data.service+"_ss").removeClass("btn-green").addClass("btn-red");
					$("#pleaseWait").modal('hide');
				}
				if(data.status == 'stopped'){
					$("#" + request_data.cluster_id + "_"+request_data.service).text('stopped');
					$("#" + request_data.cluster_id + "_"+request_data.service+"_ss").text('start');
					$("#" + request_data.cluster_id + "_"+request_data.service).removeClass("btn-green").addClass("btn-red");
					$("#" + request_data.cluster_id + "_"+request_data.service+"_ss").removeClass("btn-red").addClass("btn-green");
					$("#pleaseWait").modal('hide');
				}

	},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				setTimeout($.proxy(cluster_view.cluterFirstrunData,cluster_view), 60000);
		}
	});
},
	
}


// var url = $('#cluster_IDS').data('url');
	//   $.getJSON( url, function( data ) {

	//     $(data).each( data, function( key, val ) {
	//         var l = Ladda.create( document.querySelector( "#tm_deploy_button_"+ key));
	//         l.start(); 
	//     });
	//  }); 
