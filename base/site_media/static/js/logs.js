//Hold the last keys that the backend processed for subsequent requests. Both for the older records and the newer ones.
var searchRangeEnd = 0;
var searchRangeStart = 0;


var newestRecordTimestamp = 0;
var lastEvaluatedKeyTimestamp = null;
var logCountLimit = 100;
var autoLoadTimer = null;
var AUTOLOAD_TIMEOUT = 2500;
var scrollLock = false;
var prevTimestamp = null;
var timeFormatString = "YYYY/MMM/DD HH:mm:ss Z";
var clusterMap = {};

// Use the browser's built-in functionality to quickly and safely escape the
// string
function escapeHtml(str) {
    /*var div = document.createElement('div');

     //str = str.replace(/(['"&:;])/g, "\\$1");
     div.appendChild(document.createTextNode(str));
     var result=div.innerHTML;
     return result;*/

    return String(str)
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/\n/g, '<br/>');


};

function toggleVisibility(element, force) {
    if (force) {
        element.css('visibility', force);
        return;
    }

    if (element.css('visibility') == 'hidden') {
        element.css('visibility', 'visible');
    }
    else {
        element.css('visibility', 'hidden');
    }
}

function fillSelectComponent(tmid) {
    var select_component = $("#select_component");
    //select_component.find('option').remove().end();
    select_component.empty();
    select_component.append("<option data-component=''></option>");
    select_component.trigger("chosen:updated");
    //set to loading

    if (!tmid) {
        //end early
        return;
    }
    var component_loading = $("#component_loading > div");
    toggleVisibility(component_loading, "visible");
    var url = "/cm/logger/cluster/" + tmid + "/services";
    $.ajax({
        type: "GET",
        url: url,
        dataType: 'json',
        success: function (data) {
            var theServices = data['services'];
            theServices.forEach(function (service) {
                var prettyService = service.replace(/_/g, " ");
                var elementText = "<option data-component=" + service + ">" + prettyService + "</option>";
                select_component.append(elementText)
            });
            toggleVisibility(component_loading, "hidden");
            select_component.trigger("chosen:updated");
        },
        error: function (err) {
            toggleVisibility(component_loading, "hidden");
        }
    });

}

function loggerTimestampToSeconds(loggerTimestamp){
    var seconds;
    if (loggerTimestamp >= 100000000000000) {
        //means that the timestamp is in microseconds
        return (loggerTimestamp / 1000000);
    }
    else {
        return loggerTimestamp / 1;
    }
}

function loggerTimestampToDate(loggerTimestamp) {
    var seconds;
    if (loggerTimestamp >= 100000000000000) {
        //means that the timestamp is in microseconds
        seconds = (loggerTimestamp / 1000000);
    }
    else {
        seconds = loggerTimestamp;
    }

    var d = new Date(0); // The 0 there is the key, which sets the date to the epoch
    d.setUTCSeconds(seconds);
    return d;
}

function numberToLevel(number) {
    /*
     * ERROR 0
     * WARN 1
     * INFO 2
     * DEBUG 3
     * TRACE 4
     * */

    var numberToLevelMap;
    numberToLevelMap = {
        0: "ERROR",

        1: "WARNING",

        2: "INFO",

        3: "DEBUG",

        4: "TRACE"
    };

    if (number in numberToLevelMap) {
        return numberToLevelMap[number];
    }
    else {
        return "UNKNOWN";
    }
}

function getClusterIDFromClusterHash(clusterHash) {
    //8c81f480-5f32-4a81-bf9d-f96acfa1b4bd_tomcat_1_main
    var re = new RegExp("([a-zA-Z0-9\-]*)_.*");
    var matches = clusterHash.match(re);
    if (matches.length > 1) {
        return matches[1];
    }

    return null;

}

function loadLogSnippet(snippethash, pInsertionPoint, clusterDiv, componentDiv, successCallback, errorCallback) {
    if (snippethash) {
        var url = "/cm/logger/snippet/" + String(snippethash)+"/load";
        var insertionPoint=pInsertionPoint
        if(!insertionPoint){
            insertionPoint=$("#newer_holder");
        }

        if(!clusterDiv){
            clusterDiv=$("#cluster_name");
        }
        if(!componentDiv){
            componentDiv=$("#component_name");
        }

        $.ajax({
            type: "GET",
            url: url,
            dataType: 'json',
            success: function (data) {
                var recordsHTML = "";

                if (data["error"] != "") {
                    alert(data["error"]);
                }
                else {
                    var theSnippet = data["snippet"];
                    var theRecords = [];
                    theRecords = theRecords.concat(theSnippet["context"]["after"]);
                    theRecords = theRecords.concat([theSnippet["entry"]]);
                    theRecords = theRecords.concat(theSnippet["context"]["before"]);

                    //GENERATE THE HTML
                    recordsHTML = createHTMLFromRecords(theRecords);
                    insertionPoint.after(recordsHTML)

                    clusterDiv.html(theSnippet["cluster"]);
                    componentDiv.html((theSnippet["entry"]["component"]).replace(/_/g, " "));




                    if (successCallback != null) {
                        successCallback(data);
                    }
                }
            },
            error: function (err) {
                if (errorCallback != null) {
                    errorCallback(err);
                }
            }
        });
    }
}

function createHTMLFromRecords(theRecords) {

    var resultingBlock = ""
    theRecords.forEach(function (record) {
        var theAll = record["payload"]["all"];
        var message = record["payload"]["message"];
        var message = record["payload"]["all"];
        var timestamp = record["timestamp"];
        var component = record["component"];
        var level = numberToLevel(record['level']);


        var rowClass = "";
        switch (level) {
            case "ERROR":
                rowClass = "danger";
                break;
            case "WARNING":
                rowClass = "warning";
                break;
            case "DEBUG":
            case "INFO":
                rowClass = "success";
                break;
            default:
                rowClass = ""
        }


        //var d = loggerTimestampToDate(timestamp);
        var d = loggerTimestampToSeconds(timestamp);
        var formattedDate = moment.utc(d).format("YYYY/MMM/DD HH:mm:ss.SSS Z")

        var elementText = "";

        //if we have changed hours or days, insert a hour/date change row
        if (prevTimestamp != null) {
            var prev = moment.utc(loggerTimestampToSeconds(prevTimestamp));
            var current = moment.utc(loggerTimestampToSeconds(timestamp));


            var prevHour = prev.hour();
            var currentHour = current.hour();
            var prevDay = prev.day();
            var currentDay = current.day();
            var prevMonth = prev.month();
            var currentMonth = current.month();
            if (prevHour != currentHour || prevDay != currentDay || prevMonth != currentMonth) {
                var roundedDate = current.startOf("hour").format(timeFormatString);
                elementText += "<tr class='logSectionSeparator'><td colspan='3'>" + roundedDate + " <span class='glyphicon glyphicon-chevron-down' aria-hidden='true'></span></td></tr>";
            }
        }

        var clusterID = getClusterIDFromClusterHash(record["clusterhash"]);
        var clusterName = clusterMap[clusterID];
        elementText += "<tr class='" + rowClass + " logRow cur-pointer' " +
        "data-payload='" + escapeHtml(record["payload"]["all"]) + "'" +
        " data-clusterhash='" + record["clusterhash"] + "'" +
        " data-timestamp='" + record["timestamp"] + "'" +
        " data-clusterid='" + clusterID + "'" +
        " data-clustername='" + clusterName + "'" +
        " data-component='" + record["component"] + "'"
        + ">";

        elementText += "<td class='col-md-2 break-words longtext'>" + formattedDate + "</td>";
        elementText += "<td class='col-md-1 break-words longtext'>" + level + "</td>";
        elementText += "<td class='col-md-9 break-words longtext'>" + escapeHtml(message) + "</td>";

        elementText += "</tr>";
        resultingBlock += elementText;

        prevTimestamp = timestamp
    });

    return resultingBlock;
}

function _loadLogs(direction, insertionPoint, cluster, component, subcomponent, levels, limit, startRange, endRange, successCallback, errorCallback) {

    if (cluster && component && subcomponent && levels && limit) {
        //alert("All good");
        var resultsBody = $("#results_body");


        var url = "/cm/logger/cluster/" + cluster + "/component/" + component;
        getData = {
            "subcomponent": subcomponent,
            "level": levels,
            "limit": limit,
            "startRange": startRange,
            "endRange": endRange
        };

        console.log("startRange:" + startRange);
        console.log("endRange:" + endRange);


        $.ajax({
            type: "GET",
            url: url,
            data: getData,
            dataType: 'json',
            success: function (data) {
                var recordsHTML = "";
                var theRecords = data['records'];

                //GENERATE THE HTML
                recordsHTML = createHTMLFromRecords(theRecords);

                //update our newest record
                if (theRecords.length > 0) {

                    var tmpNew = (theRecords[0]["timestamp"]);
                    if (tmpNew > newestRecordTimestamp) {
                        newestRecordTimestamp = tmpNew;
                        console.log("New newest! " + loggerTimestampToSeconds(newestRecordTimestamp));
                    }
                }

                //update our oldest record, it could be the last one returned, OR, LastEvaluatedKey return value,
                //whichever is smallest.
                if (theRecords.length > 0) {
                    var tmpOld = (theRecords[theRecords.length - 1]["timestamp"]);
                    if (tmpOld < lastEvaluatedKeyTimestamp || lastEvaluatedKeyTimestamp == null) {
                        lastEvaluatedKeyTimestamp = tmpOld;
                        console.log("New oldest! " + loggerTimestampToSeconds(lastEvaluatedKeyTimestamp));
                    }
                }
                //the LastEvaluatedKey result that might or might not be in data is the last scanned record, but it
                //is not necessarily the oldest one in the records results (might not match our query),
                // still we sould keep track.
                if (data.hasOwnProperty("LastEvaluatedKey")) {
                    var tmpOld = (data["LastEvaluatedKey"]["timestamp"]);
                    if (tmpOld < lastEvaluatedKeyTimestamp || lastEvaluatedKeyTimestamp == null) {
                        lastEvaluatedKeyTimestamp = tmpOld;
                        console.log("New oldest! " + loggerTimestampToSeconds(lastEvaluatedKeyTimestamp));
                    }
                }

                if (direction > 0) {
                    //insert after insertionPoint (after the firstHolder)
                    //NEWER RECORDS
                    insertionPoint.after(recordsHTML)

                }
                else {
                    //insert before insertion point (e.g. above the lastHolder)
                    //OLDER RECORDS
                    insertionPoint.before(recordsHTML)
                }

                //elipsify our long records
                //$(".longtext").each(function(item){
                //    item.dotdotdot({
                //        height: 10
                //    });
                //})

                if (successCallback != null) {
                    successCallback(data);
                }

            },
            error: function (err) {
                if (errorCallback != null) {
                    errorCallback(err);
                }
            }
        });
    }
    else {
        scrollLock = false;
    }

}

function loadLogs_ui() {
    clearLogRows();
    loadLogs_older();
}

function clearLogRows() {

    //do some animation here?
    newestRecordTimestamp = null;
    lastEvaluatedKeyTimestamp = null;
    $("#results_body > tr").not(":first").not(":last").not("#older_holder").not("#newer_holder").remove().end();
    newestRecordTimestamp = 0;
    lastEvaluatedKeyTimestamp = null;

}

function animate(selector, animationType, duration) {
    var x = $(selector);
    x.addClass(animationType);
    x.addClass("animated");

    window.setTimeout(function () {
            x.removeClass(animationType);
            x.removeClass("animated");
        },
        duration
    );


}

function updateRange() {
    var extraInfoFooter = $("#extraInfoFooter");
    var extraInfoHeader = $("#extraInfoHeader");
    var newestRecord = loggerTimestampToSeconds(newestRecordTimestamp);
    var oldestRecord = loggerTimestampToSeconds(lastEvaluatedKeyTimestamp);

    var startRangeDivs = $.find(".startRange");
    var endRangeDivs = $.find(".endRange");

    $(startRangeDivs).each(function () {
        var oldValue = $(this).html();
        var newValue = moment.utc(newestRecord).format(timeFormatString)
        if (oldValue != newValue) {
            $(this).html(newValue);
            animate(this, "pulse", 1000);
        }

    })

    $(endRangeDivs).each(function () {
        var oldValue = $(this).html();
        var newValue = moment(oldestRecord).format(timeFormatString)
        if (oldValue != newValue) {
            $(this).html(newValue);
            animate(this, "pulse", 1000);
        }
    })
    //extraInfoFooter.html(searchRangeString);
    //extraInfoHeader.html(searchRangeString);

    //var rangePicker = $("#daterangepicker").data('daterangepicker');
    //rangePicker.setStartDate(oldestRecord);
    //rangePicker.setEndDate(newestRecord);
    //console.log(rangePicker.startDate.toDate());
    //console.log(rangePicker.endDate.toDate());

}

function loadLogs_newer() {
    var cluster = $("option:selected", "#select_cluster").attr("data-tmid");
    var component = $("option:selected", "#select_component").attr("data-component");
    var subcomponent = $("option:selected", "#select_subcomponent").attr("data-subcomponent");
    var level = $("option:selected", "#select_level").attr("data-level");

    if (cluster && component && subcomponent && level) {
        var theButton = $("#load_newer_button");
        startLaddaButton('#load_newer_button');
        var insertionPoint = $("#newer_holder");
        _loadLogs(1, insertionPoint, cluster, component, subcomponent, level, logCountLimit, null, newestRecordTimestamp,
            function (data) {
                theButton.attr("disabled", false);
                Ladda.stopAll();
                updateRange();
                setAutoloadTimer();
            },
            function (err) {
                theButton.attr("disabled", false);
                Ladda.stopAll();
                setAutoloadTimer();

            }
        );
    }
    else {
        scrollLock = false;
    }
}

function loadLogs_older(successCallback, errorCallback) {
    var cluster = $("option:selected", "#select_cluster").attr("data-tmid");
    var component = $("option:selected", "#select_component").attr("data-component");
    var subcomponent = $("option:selected", "#select_subcomponent").attr("data-subcomponent");
    var level = $("option:selected", "#select_level").attr("data-level");

    if (cluster && component && subcomponent && level) {
        var theButton = $("#load_older_button");
        startLaddaButton('#load_older_button');
        theButton.attr("disabled", "disabled");
        var insertionPoint = $("#older_holder");
        _loadLogs(-1, insertionPoint, cluster, component, subcomponent, level, logCountLimit, lastEvaluatedKeyTimestamp, null,
            function (data) {
                theButton.attr("disabled", false);
                Ladda.stopAll();
                updateRange();
                if (successCallback != null) {
                    successCallback(data);
                }
            },
            function (err) {
                theButton.attr("disabled", false);
                Ladda.stopAll();
                if (errorCallback != null) {
                    errorCallback(data);
                }
            }
        );
    }
    else {
        scrollLock = false;
    }
}

function startLaddaButton(selector) {
    //Ladda.create(document.querySelector(selector)).start();
    var theObject = selector;
    if (selector.jquery) {
        theObject = selector.get(0);
    }
    else {
        if (typeof selector == "string") {
            theObject = document.querySelector(selector);
        }
        else {
            theObject = selector;
        }
    }


    Ladda.create(theObject).start();


}

function stopLaddaButton(selector) {
    var theObject = selector;
    if (selector.jquery) {
        theObject = selector.get(0);
    }
    else {
        if (typeof selector == "string") {
            theObject = document.querySelector(selector);
        }
        else {
            theObject = selector;
        }
    }


    Ladda.create(theObject).stop();
}

function setAutoloadTimer() {

    var theState = $("#autoload").bootstrapSwitch("state");

    if (theState) {
        autoLoadTimer = setTimeout(function () {
            loadLogs_newer();
        }, AUTOLOAD_TIMEOUT);
    }
    else {
        clearTimeout(autoLoadTimer);
    }
}


function rowClickHandler(row, e) {
    var theModal = $("#rowModal");
    var options = {}
    var payload = $(row).attr("data-payload");
    $("#modalOriginalEntry").html(payload);

    var clusterName = $(row).attr("data-clustername");
    $("#modalCluster").html(clusterName)

    var component = $(row).attr("data-component");
    var subcomponent = $(row).attr("data-subcomponent");
    $("#modalComponent").html(component.replace(/_/g, " "));

    var clusterhashInput = $("#id_clusterhash");
    var timestampInput = $("#id_timestamp");

    clusterhashInput.val($(row).attr("data-clusterhash"));
    timestampInput.val($(row).attr("data-timestamp"));


    var emailFormContainer = $("#emailFormContainer");

    //collapse the email container if it's open
    if (emailFormContainer.hasClass("in")) {
        emailFormContainer.collapse('hide');
    }

    theModal.modal(options);
}

function updateEmailStatus(message, theClass) {
    var theDiv = $("#emailResult");
    theDiv.html(message);
    theDiv.addClass(theClass);
    theDiv.show("fade", 1000);
    window.setTimeout(function () {
            theDiv.hide("fade", 3000, function () {
                theDiv.removeClass(theClass);
            })


        },
        6000
    );
}

function sendLogEntrylHandler(e, button) {
    var theFormSelector = "#sendLogEntryForm"
    var theForm = $(theFormSelector)
    var theData = theForm.serialize()
    var url = "/cm/logger/sendmail";
    startLaddaButton(button);
    $.ajax({
        type: "POST",
        url: url,
        data: theData,
        dataType: 'json',
        success: function (data) {
            var error = data["error"];
            console.log("Sucess");

            theForm.replaceWith(data['form_string']);


            if (error == "") {
                updateEmailStatus("Email sent!", "alert-success");
            }
            else {
                updateEmailStatus("There was a problem sending your email!", "alert-danger");
            }

            console.log("Stopping ladda");
            stopLaddaButton(button);
        },
        error: function (err) {
            updateEmailStatus("There was a problem sending your email!: " + err, "alert-danger");
            stopLaddaButton(button);
        }
    });
}

function initBackToTop() {
    var offset = 300;
    //browser window scroll (in pixels) after which the "back to top" link opacity is reduced
    var offset_opacity = 1200;
    //duration of the top scrolling animation (in ms)
    var scroll_top_duration = 700;
    //grab the "back to top" link
    var $back_to_top = $('.logs-cd-top');

    //hide or show the "back to top" link
    $(window).scroll(function () {
        ( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
        if ($(this).scrollTop() > offset_opacity) {
            $back_to_top.addClass('cd-fade-out');
        }
    });

    //smooth scroll to top
    $back_to_top.on('click', function (event) {
        event.preventDefault();
        $('body,html').animate({
                scrollTop: 0,
            }, scroll_top_duration
        );
    });
}

function initAutoLoadOnScroll() {
    $(window).scroll(function (e) {

        var scrollPos = $(window).scrollTop() + $(window).height();
        var threshold = 0.95;
        var thresholdHeight = $(document).height() * threshold;
        if (scrollPos >= thresholdHeight) {
            if (scrollLock == false) {
                scrollLock = true;
                loadLogs_older(function (data) {
                    scrollLock = false
                }, function (err) {
                    scrollLock = false
                });
                console.log("Scroll load! " + String(scrollPos) + "/" + String(thresholdHeight));
            }
        }
    });
}

function logsInit() {

    //AUTOLOAD

    $("#autoload").bootstrapSwitch({size: "mini"});
    setAutoloadTimer();

    $("#autoload").on("switchChange.bootstrapSwitch", function (event, state) {
        setAutoloadTimer();
    });

    //BACK TO TOP
    initBackToTop();

    //PARAMETER CONTROLS
    $("#select_cluster").on("change", function () {
        var selected = $("option:selected", this);
        var tmid = selected.data("tmid");
        fillSelectComponent(tmid);

    });

    $("#select_cluster").chosen();
    $("#select_component").chosen();
    $("#select_subcomponent").chosen();
    $("#select_level").chosen();

    $("#select_cluster, #select_component, #select_subcomponent, #select_level").on("change", function () {
        clearLogRows();
    });

    //AUTOLOAD ON SCROLL.
    initAutoLoadOnScroll();


    //BUTTONS
    $("#load_newer_button").on("click", function () {
        loadLogs_newer();
    });

    $("#load_older_button").on("click", function () {
        loadLogs_older();
    });

    $("#go_button").on("click", function () {
        //load by range
        loadLogs_newer();
    });


    //click on log row functionality
    $(document).on("click", ".logRow",
        function (e) {
            rowClickHandler(this, e);
        }
    )

    $(document).on("click", "#sendLogEntry",
        function (e) {
            var theButton = this;
            sendLogEntrylHandler(e, theButton);
        }
    )

    $(document).on("submit", "#sendLogEntryForm",
        function (e) {
            e.preventDefault();
            var theButton = $("#sendLogEntry");
            sendLogEntrylHandler(e, theButton);
        }
    )

    $("#emailResult").hide();


    var datePicker = $("#daterangepicker");
    datePicker.daterangepicker({
        timePicker: true,
        timePickerSeconds: true,
        timePickerIncrement: 1,
        timePicker12Hour: false,
        format: 'YYYY/MM/DD HH:mm:ss',
    });

    datePicker.on("apply.daterangepicker", function (e, picker) {
        clearLogRows();
    })

}

function logSnippetInit(snippethash) {

    //AUTOLOAD

    //BACK TO TOP
    initBackToTop();

    //click on log row functionality
    $(document).on("click", ".logRow",
        function (e) {
            rowClickHandler(this, e);
        }
    )

    //LOAD THE SNIPPET
    loadLogSnippet(snippethash);

    //TODO: MAYBE ALLOW SENDING FROM SNIPPET?
    //$(document).on("click","#sendLogEntry",
    //    function(e) {
    //        var theButton=this;
    //        sendLogEntrylHandler(e, theButton);
    //    }
    //)

    //$(document).on("submit","#sendLogEntryForm",
    //    function(e) {
    //        e.preventDefault();
    //        var theButton=$("#sendLogEntry");
    //        sendLogEntrylHandler(e, theButton);
    //    }
    //)

    //$("#emailResult").hide();


}