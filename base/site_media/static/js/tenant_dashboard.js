/*
 * Loading modal form into ajax-modal container
 * Use call back for post-processing
 */
function loadModalForm(uri, options, callback){
	$("#spinner-modal").modal({backdrop: false})
	window.setTimeout(function(){
		 $.get(uri, options, function(data){
			 $("#ajax-modal").html(data)
			
			 if ($.isFunction(callback)){
				 callback()
			 }
			 $("#spinner-modal").modal('hide')
			 $("#ajax-modal").modal('show')
			  
	    });
	  }, 1000);
}

/*
 * Common events for AJAX_SUBMIT forms
 */
$(document)
	.on("confirm", ".ajax-submit", function(){
		var $form = $(this)
		var ajaxParams = $form.serializeArray();
				$.ajax({
			        type: $form.attr("method"),
			        url: $form.attr("action"),
			        data: ajaxParams,
			        dataType: 'json',
			        timeout: 5000,
			        success: function(data) {
			        	$form.find('.help-block').remove();
			        	$form.find('.has-error').removeClass('has-error');
			        	if (!data.errors){
			        		$form.parents(".modal.fade").modal("hide");
		           			location.reload();
		           		}
		           		else {
		           			var errors = data.errors;
				           	for (error in errors) {
				           		$form.find("[name="+error+"]").parent("div.form-group").addClass("has-error");
				           		$form.find("[name="+error+"]").parent().append($('<small style="" class="help-block">'+errors[error]+'</small>'));
				            }
				           	if ('__all__' in errors){
				           		$form.find(".modal-body").prepend('<small class="help-block" style="color: #a94442;">'+errors['__all__']+'</small>')
				           	}
		           		}
			        },
			        error: function( jqXHR,  textStatus,  errorThrown ){
						console.log(jqXHR);
			    	},
			    });
	})
	.on("change", ".ajax-submit select", function(){
		var $form = $(this).parents(".ajax-submit")
		$form.find(".form-group").removeClass("has-error")
		$form.find(".help-block").remove()
	})

/*
 * Tenant migration form
 */
function migrForm () {

	this.manage = function (){
		var jqMigrForm = $("#migration_form");
		// setting global destination clusters list values
		// it contains all clusters that are allowed for the user
		window.migrDestList = jqMigrForm.find("#destClusterList").children();
		
		// always disable destination cluster list
		jqMigrForm.find("#destClusterList").chsnAttr("disabled", "disabled");
		
	    function manageDestinationList (){
	    	if ($("#sourceClusterList").val() != "") {
		        // saving current state
		        var cur_dest_val =  $('#destClusterList').val();
		        // refreshing destination cluster list
		        $('#destClusterList').chsnEmpty();
		        $('#destClusterList').chsnAppend($('<option value=""></option>'));
		        var available_dests = []
		        $.each(window.migrDestList, function(){
		            if ($(this).attr('value') != "" && $(this).attr('value') != $('#sourceClusterList').val()) {
		            	$('#destClusterList').chsnAppend($(this));
		                available_dests.push ($(this).attr('value'));
		            }
		        });
		        // trying to restore previos state
		        if ($.inArray(cur_dest_val, available_dests) > -1) {
		        	$('#destClusterList').chsnVal(cur_dest_val);
		        } else {
		        	$('#destClusterList').chsnVal("");
		        }
		        $("#destClusterList").chsnRemoveAttr("disabled");
		    } else {
		    	$("#destClusterList").chsnAttr("disabled", "disabled");
		    }
	    };

		jqMigrForm.on("change", "#sourceClusterList", function() {
	    	manageDestinationList ();
	        // triggering change for the  field to be validated
	        $("#destClusterList").trigger("change");
	    });
	}; // manage
	
	this.init = function (){
		this.manage();
		$('form.flip-confirmation').flipConfForm();
		$(".cm-chosen").chosen({width: "100%"});
	}
}; // migrForm


$(document).ready( function(){
	/*
	 * Setting global variable for tenant_id
	 */
	TENANT_ID = $(".tenant-header").data("tenant-id")
	
	$(document).on("click", ".coming-soon", function(){
		$("#ajax-modal").html('\
		<div class="modal-dialog">\
			<div class="modal-content">\
				<div class="modal-header">\
					<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>\
					<h4 class="modal-title">We are on it!</h4>\
				</div>\
				<div class="modal-body">\
					<h1 class="text-center">Coming soon!</h1>\
				</div>\
				<div class="modal-footer" style="margin-top: 0px;">\
					<button data-dismiss="modal" class="btn btn-primary" type="button">Awesome!</button>\
				</div>\
			</div>\
		</div>\
		'
		);
		$("#ajax-modal").modal();
	});
		
	$(document).on("click", ".restore-tenant", function(){
		var export_id = $(this).closest("[data-export-id]").data("export-id")
		
		function callBack (){
			$('.flip-confirmation').flipConfForm();
			$(".cm-chosen").chosen({width: "100%"});
			$("#tenant_restore_form").find("[name='export_id']").val(export_id)
		}
		loadModalForm($(this).data("url"), {'tenant_id': window.TENANT_ID}, callBack)
		
	});
	
	$(document).on("click", ".migrate-tenant", function(){
		function callBack (){
			var tenantMF = new migrForm ();
			tenantMF.init();
		}
		loadModalForm($(this).data("url"), {'tenant_id': window.TENANT_ID}, callBack)
		
	});
	
	$(document).on("click", ".clone-tenant", function(){
		function callback(){
			$('form.flip-confirmation').flipConfForm();
			$(".cm-chosen").chosen({width: "100%"});
		}
		loadModalForm($(this).data("url"), {'tenant_id': window.TENANT_ID}, callback)
	});

	$(document).on("click", ".backup-tenant", function(){
		function callback(){
			$('form.flip-confirmation').flipConfForm();
			$(".cm-chosen").chosen({width: "100%"});
		}
		loadModalForm($(this).data("url"), {'tenant_id': window.TENANT_ID}, callback)
	});
	$(document).on("click", ".sgs-tenant", function(){
		function callback(){
			$('form.flip-confirmation').flipConfForm();
			$(".cm-chosen").chosen({width: "100%"});
		}
		loadModalForm($(this).data("url"), {'tenant_id': window.TENANT_ID}, callback)
	});
	
	$(document).on("click", ".delete_gs", function(){
		var export_id = $(this).closest("[data-export-id]").data("export-id")
		function callback(){
			$('form.flip-confirmation').flipConfForm();
			$(".cm-chosen").chosen({width: "100%"});
		}
		loadModalForm($(this).data("url"), {'tenant_id': window.TENANT_ID, 'export_id': export_id}, callback)
	});
	
	
	// -------------
	// --Filling the cluster's table
	// -------------
	if ($("#tenant-clusters").length) {
		tbody = document.createElement("tbody")
		$.ajax({
			type: "GET",
			url: $("#tenant-clusters").data("clusters-url"),
			data: {"tenant_id": $("#tenant-clusters").data("tenant-id")},
			dataType: "json",
			aysync: false,
			timeout: 30000,
			success: function(data) {
				var counter = 0;
				$.each(data, function() {
					counter ++;
					var tr = document.createElement("tr");
					var td = document.createElement("td");
					// counter
					if (counter == 1){
						$(tr).addClass("tm-active")
					}
					var element = $(td).text(counter);
					$(tr).append(element);
					// cluster name
					var td = document.createElement("td");
					element = $(td).text(this.name);
					element.addClass("cluster-name");
					$(tr).append(element);
					// cluster version
					var td = document.createElement("td");
					element = $(td).text(this.version);
					$(tr).append(element);
					// tenant's status for cluster
					// for now it is spinner - we will update status by another request
					var td = document.createElement("td");
					var spiner = document.createElement("span");
					$(spiner)
						.addClass("imoon")
						.addClass("imoon-spinner5")
						.addClass("fa-spin")
						.addClass("cm2-spinner")
						.addClass("cm2-spinner")
					element = $(td);
					element.data("cluster-id", this.id);
					element.addClass("cluster-status");
					element.append(spiner);
					
					$(tr).append(element);
					$(tr).addClass("cur-pointer");
					$(tbody).append(tr);
				}); //each
			}, // success
			complete: function(){
				$("#tenant-clusters").append(tbody)
				$("#clusters-spinner").css("display", "none")
				$("#tenant-clusters").css("display", "table")
				updateClusterStatus();
				}
		}); //ajax
	}; // if #.tenant-clusters
	
	// -------------
	// --Updating clusters' status
	// -------------
	var updateClusterStatus = function (){
		$.each($("#tenant-clusters td.cluster-status"), function(){
			var statusTD = $(this);
			$.ajax({
				type: "GET",
				url: $(this).parents("#tenant-clusters").data("tenant-status-url"),
				data: {"tenant_id": $(this).parents("#tenant-clusters").data("tenant-id"), "cluster_id": $(this).data("cluster-id")},
				dataType: "json",
				aysync: false,
				timeout: 30000,
				success: function(data) {
					statusTD.empty();
					statusTD.text(data.tenant_status);
				}, // success
			}); // ajax
			
		}); //each
		window.setTimeout(updateClusterStatus, 30000); // update after next 30 sec
	}; // updateClusterStatus func
	
	function fillPanelTab (tabHeaderSelector, dataUrl, tenantId, clusterId){
		var tabContentSelector = $(tabHeaderSelector).attr("href")
		
		$(tabContentSelector).load(
			dataUrl,
			$.param({tenant_id: tenantId, cluster_id: clusterId}),
			// fixing height of tab content (it will not change height during Ajax-loading)
			function(){
				var maxHeight = Math.max($(tabContentSelector).parent().children().height())
				$(tabContentSelector).parent().children().each(function(){
					$(this).css("height", maxHeight)
				})
			}
		);
	}
	/*
	 * Tenant Backups information
	 */
	function fillBackups(){
		var tabHeader = $(".tenant-backups ul.panel-tabs li.active a");
		fillPanelTab(
			tabHeader, 
			$(tabHeader).parents(".tenant-backups").data("backups-url"), 
			$(tabHeader).parents(".tenant-backups").data("tenant-id"),
			$(tabHeader).data("cluster-id")
		)
		$(tabHeader).tab("show");
	}
	if ($(".tenant-backups").length){
		fillBackups();
		
		$(document).on("click", ".tenant-backups ul.panel-tabs a", function(e){
			fillBackups();
		}); // on click ul.panel-tabs a
	}
	
	/*
	 * Tenant Tasks information
	 */
	function fillTasks(){
		var tabHeader = $(".tenant-tasks ul.panel-tabs li.active a");
		fillPanelTab(
			tabHeader, 
			$(tabHeader).parents(".tenant-tasks").data("tasks-url"), 
			$(tabHeader).parents(".tenant-tasks").data("tenant-id"),
			$(tabHeader).data("cluster-id")
		)
		$(tabHeader).tab("show");
	}
	if ($(".tenant-tasks").length) {
		fillTasks();
		
		$(document).on("click", ".tenant-tasks ul.panel-tabs a", function(e){
			fillTasks();
		}); // on click ul.panel-tabs a
	};
	
	
	// AJAX pagination for tenant's tasks
	$(document).on("click", ".tenant-tasks .ajax-pgn", function(event) {
		if ($(this).data("page") != $(".tenant-tasks button.active").data("page")){
			// preventing the nervous clickers: 
			$(this).parent().attr('disabled', 'disabled');
			$(this).parent().children('button').removeClass("active");
			// global container where tenant-id and cluster-id are situated
			var tasksContainer = $(this).parents().find(".tasks-container");
			var tenantId = $(tasksContainer).data("tenant");
			var clusterId = $(tasksContainer).data("cluster");
			// container for task's html
			var htmlContainer = $(this).parents(".tab-pane.active");
			$(htmlContainer).empty();
			$(htmlContainer).load(
				$(this).parent().data("url"),
				$.param({tenant_id: tenantId, cluster_id: clusterId, page: $(this).data("page")})
			);
			$(this).parent().removeAttr('disabled');
			$(this).addClass("active");
		}
	});
	
	
	//update tasks status on the interval
	//only for visible task's container
	function updateTasksStatus() {
		$(".tasks-container").each(
			function(){
				if ($(this).is(":visible")) {
					var element = this;
					$.ajax({
						type: "GET",
						url: $(element).data("url"),
						data: {"tenant_id": $(this).data("tenant"), "cluster_id": $(this).data("cluster")},
						dataType: 'json',
						aysync: true,
						timeout: 10000,
						success: function(data) {
							$.each(data.tasks, function(){
								// updating global task's Status and Icon
								// updating stask's end time
								$("#task-"+$(element).data('tenant')+'-'+$(element).data('cluster')+'-'+this.id+"-end-time").text(this.end_time);
								// updating status Icon if status has changed:
								if ($("#task-"+$(element).data('tenant')+'-'+$(element).data('cluster')+'-'+this.id+"-status").length && this.status != $("#task-"+$(element).data('tenant')+'-'+$(element).data('cluster')+'-'+this.id+"-status").text()){
									var icoId = "task-"+$(element).data('tenant')+'-'+$(element).data('cluster')+'-'+this.id+"-stat-ico";
									var contIcoId = 'task-ico-cont-'+$(element).data('tenant')+'-'+$(element).data('cluster');
									// removing old icon DOM element
									$("#"+icoId).remove();
									// creating new icon element
									$("#task-"+$(element).data('tenant')+'-'+$(element).data('cluster')+"-"+this.id+"-status").text(this.status);
									if (this.status === 'Succeeded'){
										$("#"+contIcoId).append('<span id="'+icoId+'" class="glyphicons glyphicons-ok text-green"></span>');
									} else if (this.status === 'Started'){
										$("#"+contIcoId).append('<span id="'+icoId+'" class="imoon imoon-spinner5 fa-spin cm2-spinner cm2-spinner tm-blue"></span>');
									} else if (this.status === 'Canceled'){
										$("#"+contIcoId).append('<span id="'+icoId+'" class="glyphicons glyphicons-ban text-red"></span>');
									} else {
										$("#"+contIcoId).append('<span id="'+icoId+'" class="glyphicons glyphicons-remove text-red"></span>');
									}
								}
	
								// Updating subtask information
								$.each(this.subtasks, function(){
									$("#subtask-"+$(element).data('tenant')+'-'+$(element).data('cluster')+'-'+this.jid+"-start-time").text(this.start_time);
									$("#subtask-"+$(element).data('tenant')+'-'+$(element).data('cluster')+'-'+this.jid+"-end-time").text(this.end_time);
									$("#subtask-"+$(element).data('tenant')+'-'+$(element).data('cluster')+'-'+this.jid+"-status").text(this.status);
									$("#subtask-"+$(element).data('tenant')+'-'+$(element).data('cluster')+'-'+this.jid+"-description").text(this.description);
								});
							});
						},
						error: function() {
							$(element).find(".badge").remove();
							$(element).find("#status-"+$(element).data("tenant")+"-"+$(element).data("cluster")+"-loader").show();
						},
					});
				}
			}
	)};
	//Update task's status every 10 seconds
	window.setInterval(updateTasksStatus,10000);
	
	// Managing tenant's tab switches
	if ($(".tenant-clusters").length) {
		$(document).on('click', '.tenant-clusters tr', function(){
			$(this).parents(".tenant-clusters tbody").find("tr").removeClass("tm-active")
			$(this).addClass("tm-active")
			var clusterName = $(this).find("td.cluster-name").text()
			if (clusterName) {
				$('.panel-tabs a:contains("'+clusterName+'")').trigger('click')
			}
		})
		
	} 
	
	// -------------
	// --Filling the golden states table
	// -------------
	if ($("#tenants-golden-states").length) {
		var gsTable = $("#tenants-golden-states")
		$.ajax({
			type: "GET",
			url: gsTable.data("url"),
			data: {"tenant_id": gsTable.data("tenant-id")},
			dataType: "json",
			aysync: false,
			timeout: 30000,
			success: function(data) {
				var counter = 0;
				var tbody = document.createElement("tbody");
				if (data.data.length) {
					$.each(data.data, function() {
						counter ++;

						var tr = document.createElement("tr");
						$(tr).attr("data-tenant-id", this.attributes.tenant_id)
						$(tr).attr("data-export-id", this.attributes.export_id)
						
						if (this.comment) {
							$(tr).attr("data-container", "body")
							$(tr).attr("data-toggle", "popover")
							$(tr).attr("data-placement", "bottom")
							$(tr).attr("title", "Comment")
							$(tr).attr("data-content", this.attributes.comment)
						}
						
						var td = document.createElement("td");
						$(td).addClass("cluster-version")
						// cluster version
						// var element = 
						$(td).text(this.attributes.cluster_version);
						$(tr).append(td);
						
						// golden state create date
						var td = document.createElement("td");
						var unixTime = moment.unix(this.attributes.utc_timestamp)
						var formattedTime = unixTime.format("L HH:mm:ss")
						element = $(td).text(formattedTime);
						$(tr).append(element);
						
						// created by user
						var td = document.createElement("td");
						var element = $(td).text(this.attributes.user_name);
						$(tr).append(element);
						
						var td = document.createElement("td");
						$(td).addClass("tooltip-demo")
						
						var actions = Object.keys(this.actions)
						if (actions.length){
							var actionBtn = document.createElement("button")
							$(actionBtn)
								.addClass("btn")
								.addClass("btn-xs")
								.addClass("btn-default")
								.addClass("dropdown-toggle")
								.attr("data-toggle", "dropdown")
								.html("<span class='glyphicons glyphicons-cogwheel tm-blue'></span> <span class='caret'></span>")
							
							var actionList = document.createElement("ul")
							$(actionList)
								.addClass("dropdown-menu")
								.attr("role", "menu")
								
								
							if ($.inArray("download", actions) != -1) {
								var aElmt = document.createElement("a")
								$(aElmt)
									.addClass("restore-tenant")
									.attr("href", "javascript:void(0)")
									.attr("data-url", this.actions.download.url)
									.text("Download")
								
								var liElmt = document.createElement("li")
								$(liElmt).append(aElmt)
								$(actionList).append(liElmt)
							}
							
							if ($.inArray("delete", actions) != -1) {
								var aElmt = document.createElement("a")
								$(aElmt)
									.addClass("delete_gs")
									.attr("href", "javascript:void(0)")
									.attr("data-url", this.actions.delete.url)
									.text("Delete")
								
								var liElmt = document.createElement("li")
								$(liElmt).append(aElmt)
								$(actionList).append(liElmt)
							}
							
							var btnGrp = document.createElement("div")
							$(btnGrp)
								.addClass("btn-group")
								.append(actionBtn)
								.append(actionList)
								
							$(td).append(btnGrp)
						}	
						$(tr).append(td);
						
						$(tbody).append(tr)
					}); //each
					gsTable.append(tbody);
					// init popover
				    $("[data-toggle=popover]").popover({html:true, trigger:'hover'})
					gsTable.css("display", "table")
				} else {
					gsTable.after("<span> There are no any golden states for that tenant.</span>")
				}
				
			}, // success
			error: function(){
				gsTable.after("<span class='text-danger'>Error occurred while getting tenant's golden states information</span>")
			},
			complete: function(){
				$("#gs-spinner").css("display", "none")
			}
		}); //ajax
		
	}; // if #.tenants-golden-states

}); // document ready       
