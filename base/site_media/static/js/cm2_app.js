/*
 * Chosen wrapper.
 * We need to call updated function each time we do some changes.
 * Feel free to add new wrappers.
 */
(function( $ ) {
	
	$.fn.chsnEmpty = function() {
		this.empty.apply(this, arguments);
		this.trigger("chosen:updated");
		return this
	};
	
	$.fn.chsnAppend = function() {
		this.append.apply(this, arguments);
		this.trigger("chosen:updated");
		return this
	};
	
	$.fn.chsnRemoveAttr = function() {
		this.removeAttr.apply(this, arguments);
		this.trigger("chosen:updated");
		return this
	};
	
	$.fn.chsnAttr = function() {
		this.attr.apply(this, arguments);
		this.trigger("chosen:updated");
		return this
	};
	
	$.fn.chsnVal = function() {
		this.val.apply(this, arguments);
		if (arguments.length > 0){
			this.trigger("chosen:updated");
		}
		return this
	};
}( jQuery ));

/*
 * Form flip confirmation JQuery plug-in
 */
(function( $ ) {
	
	/*
	 * Make form with flipping confirmation.
	 * Form should has div.confirmation that is a container for the button(s).
	 * Button may have class .show-confirmation - then text from  data-confirmation-text
	 * attribute will be used as a confirmation text.
	 */		
	$.fn.flipConfForm = function(){
		this.each(function(){
			var formIsNew = false;
			
			$(this).find('.confirmation').each(function(){
				if (!$(this).children(".form-buttons").length){

					formIsNew = true
					var confirmContainer = this;
					var formButtons = []
					var containerData = $(this).data()
					
					/*
					 * Setting button's and confirmation text
					 */
					if (containerData.conftext) {
						var confirmationText = containerData.conftext
					} else {
						var confirmationText = 'Please, confirm action!'
					}
					
					if (containerData.confbtn) {
						var confirmBtnText = containerData.confbtn
					} else {
						var confirmBtnText = 'Confirm'
					}
					
					if (containerData.cancelbtn) {
						var cancelBtnText = containerData.cancelbtn
					} else {
						var cancelBtnText = 'Cancel'
					}
					
					
					if ($(this).hasClass('show-confirmation')) {
						confirmationText = $(this).data('confirmation-text')
					}
					
					$(confirmContainer).children("*").each(function(index){
						formButtons[index] = $(this).detach();
					})
					
					$(confirmContainer).prepend('<div class="well well-sm animated-shortest form-buttons clearfix" style="margin-bottom:0px;"></div>');
					$.each(formButtons, function(index, value){
						$(confirmContainer).children('.form-buttons').prepend(value);
					});
					$(confirmContainer).prepend(' \
						<div class="well well-sm animated-shortest confirmation-buttons" style="display:none; margin-bottom:0px;"> \
					    	<div class="row"> \
					    		<div class="col-md-2 col-sm-2 col-xs-2"> \
					    			<button type="button" class="btn btn-danger btn-gradient pull-left cancel-submit" >'+cancelBtnText+'</button> \
					    		</div> \
					    		<div class="col-md-8 col-sm-8 col-xs-8" style="text-align:center;"> \
					    			<h4>'+confirmationText+'</h4> \
					    		</div> \
					    		<div class="col-md-2 col-sm-2 col-xs-2"> \
					    			<button type="button" class="btn btn-success btn-gradient pull-right confirm-submit" >'+confirmBtnText+'</button> \
					    		</div> \
					    	</div> \
					    </div>'
					);
				};
			});
			
			/* 
			 * *********************************
			 * ****** BINDING EVENTS 
			 * *********************************
			 */
			if (formIsNew){
			
				$(this).on("reset", function(){
					$(this).find(".cancel-submit").trigger("click");
				})
				
				$(this).find(".show-confirmation").on("click", function(){
					var _this = this
					$(_this).parent().addClass("flipOutX");
					
					var to = window.setTimeout(function(){
						$(_this).parent().hide();
						$(_this).parent().removeClass("flipOutX");
						$(_this).closest(".confirmation").find(".confirmation-buttons").show();
						$(_this).closest(".confirmation").find(".confirmation-buttons").addClass("flipInX");
						var wait = window.setTimeout(
							function(){$(_this).closest(".confirmation").find(".confirmation-buttons").removeClass("flipInX")},
							400
						);
					},
					400
					);
				});
				$(this).find(".cancel-submit").on("click", function(){
					var _this = this
					$(_this).parents('.confirmation-buttons').addClass("flipOutX");
					
					var to = window.setTimeout(function(){
						$(_this).parents('.confirmation-buttons').hide();
						$(_this).parents('.confirmation-buttons').removeClass("flipOutX");
						$(_this).closest(".confirmation").find(".form-buttons").show();
						$(_this).closest(".confirmation").find(".form-buttons").addClass("flipInX");
						var wait = window.setTimeout(
							function(){$(_this).closest(".confirmation").find(".form-buttons").removeClass("flipInX")},
							400
						);
					},
					400
					);
				});
				
				$(this).on('submit', function(event){
					event.preventDefault();
					if (!$(this).find(".confirmation-buttons").is(":visible")){
						$(this).find(".show-confirmation").trigger("click");
					}
				});
				
				$(this).find('.confirm-submit').on('click', function(){
					$(this).trigger('confirm')
				});
			}
		});
		return this
	}
	
	
}( jQuery ));

// Hides all animated elements with fadeOutDownBig effect
// and shows particular element after
function makeAnimation(elementId) {
	// if we have visible panels - hiding them with animation
	if ($(".tm-animated").is(":visible")) {
		$(".tm-animated").addClass("fadeOutDownBig");
		var to = window.setTimeout(function(){
			$(".tm-animated").hide();
			$(".tm-animated").removeClass("fadeOutDownBig");
			elementShow(elementId);
		},
		900
		);
	} else {
		$(".tm-animated").hide();
		elementShow(elementId);
	}
};

// Shows particular panel with animation effect
function elementShow(elementId) {
	var animationType = "bounceInRight";
	$(elementId).show();
	$(elementId).addClass(animationType);
	var wait = window.setTimeout( function(){
		$(elementId).removeClass(animationType)},
		900
	);

}

$(document).ready( function(){
	
	if($('#cluster_badge').length || $('#tenant_badge').length){cmt.dashboardBages();};
	
	if ($('.dashboard').length) {
		cmt.dashboardAnimation();
	} else {
		cmt.dashboardSideMenu();
	};
	if($(".cm-chosen").length){
		$(".cm-chosen").chosen({width: "100%"});
	};
	/*
	if( $('form > div.confirmation').length){
		FormFlipConf();
	}
	*/
	// init tooltip
	$('.tm-tooltip').tooltip({
		selector: "[data-toggle=tooltip]",
		container: "body"
	});

	// show pop-up messages
	if ($("#message-modal").children().length > 0) {
		$.each($("#message-modal").children(), function() {
			$.gritter.add({
				title: 'Notice',
				text: $(this).text(),
				sticky: false,
				time: '',
			});
		})
	}
	
	if ($('.click-animation.animated').length) {
		$('.click-animation').click(function(){
			var animationCls=$(this).data('animation-class');
			$(this).addClass(animationCls);
			var wait = window.setTimeout( function(){
				$(this).removeClass(animationCls)},
				130
			);
		});
	};

}); // document.ready


// cleaning modal window after on close event:
$(document).on('hidden.bs.modal', '#ajax-modal', function(){
	$(this).html('\
		<div class="modal-header">\
			<button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>\
			<h4 class="modal-title text-center">No data</h4>\
		</div>\
		<div class="modal-body" >\
			<center>No data found.</center>\
		</div>\
		<div class="modal-footer">\
			<button type="button" data-dismiss="modal" class="btn btn-info">Close</button>\
		</div>\
		');
	}
);


cmt = {

	dashboardAnimation:function(){

		// Init Theme Core 	  
		Core.init();
	
		// Create an example page animation. Really
		// not suitable for production enviroments
		var headerAnimate = setTimeout(function() {
			// Animate Header from Top
			$('header').css("display", "block").addClass('animated bounceInDown');
		},300);
	
		// Add an aditional delay to hide the loading spinner
		// and animate body content from bottom of page
		var bodyAnimate = setTimeout(function() {
		// hide spinner
			$('#page-loader').css("display", "none");
	
		// show body and animate from bottom. At end of animation 
		// add several css properties because the animation will bug 
		// existing psuedo backgrounds(:after)
		$('#main').css("display", "block").addClass('animated animated-short bounceInUp').one('webkitAnimationEnd oanimationend msAnimationEnd animationend', function(){
			$('header').removeClass('animated bounceInDown');
			$('#main').removeClass('animated animated-short bounceInUp')
			$('body').css({background: "#FFFFFF", overflow: "visible"});
			$('#content, #sidebar').addClass('refresh');
			// Init sparkline animations
			//sparkload();
			});				
		},1150);
	
	
		// Example animations ran on Header Buttons - For Demo Purposes
		$('.header-btns > div').on('show.bs.dropdown', function() {
			$(this).children('.dropdown-menu').addClass('animated animated-short flipInY');
		});
		$('.header-btns > div').on('hide.bs.dropdown', function() {
			$(this).children('.dropdown-menu').removeClass('animated flipInY');
		});
	},
	dashboardBages: function(){
		var url = $('#tenant_badge').data('url');
	
		$.ajax({
			type: 'GET',
			url: url,
			dataType: 'json',
			aysync: true,
			timeout: 30000,
			success: function(data) {
				$('#tenant_badge').text(data.tenant_cnt);
				$('#cluster_badge').text(data.cluster_cnt);
				window.setTimeout(cmt.dashboardBages, 15000);
			},
			error: function (XMLHttpRequest, textStatus, errorThrown) {
				$("#notice_div").html('Timeout contacting server..');
				window.setTimeout(cmt.dashboardBages, 60000);
			}
		});
	},
	dashboardSideMenu: function() {
		Core.runSideMenu();
	}
};

/* ---------------------------------------------------------------------- */
/*    SelectMenu
/* ---------------------------------------------------------------------- */
$(function(){

	var url = window.location.pathname,
	// create regexp to match current url pathname and remove trailing slash 
	//if present as it could collide with the link in navigation in case trailing slash wasn't present there
	urlRegExp = new RegExp(url.replace(/\/$/,'') + "$"); 
		// now grab every link from the navigation
	$('.nav a').each(function(){
		// and test its normalized href against the url pathname regexp
		if(urlRegExp.test(this.href.replace(/\/$/,''))){
	$(this.parentNode.parentNode).addClass('in');
		$(this.parentNode.parentNode.parentNode).addClass('active');
		$(this.parentNode).addClass('menu-open active');
		$(this.parentNode.parentNode.parentNode.childNodes[1]).addClass('menu-open active');
			}
			});


});

