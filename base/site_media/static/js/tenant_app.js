// binding subtask information button onClick event
// this code is outside the library function because
// we don't have a button in the DOM on document ready moment
// button loads dynamically
$(document).on('click', '.subtask-info', function(){
	var $modal = $('#ajax-modal');
	var url = $(this).data("url");
	var jidVal = $(this).data("jid");
	setTimeout(function(){
		$modal.load(
			url, 
			$.param({jid: jidVal}), function(){
				$modal.modal();
				});
	}, 1000);
	});