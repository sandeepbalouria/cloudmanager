__author__ = 'dgollas'

from django.conf.urls import patterns, url

from .views import LoggerEntries, LoggerClusterServices, LoggerEmailLogEntries, LoggerSnippet, LogsView, LogSnippetView


urlpatterns = patterns('',
				   url(r'^$', LogsView.as_view(), name="cm_logs"),
				   url(r'^snippet/([0-9a-f]{32})/$', LogSnippetView.as_view(), name="cm_logs"),
					url(r'^cluster/([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})/services$',
						LoggerClusterServices.as_view(), name="api_logger_cluster_services"),
					url(r'^cluster/([0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})/component/([^/]+)$',
						LoggerEntries.as_view(), name="api_logger_cluster_services"),
					url(r'^entries$', LoggerEntries.as_view(), name="api_logger_entries"),
					url(r'^sendmail$', LoggerEmailLogEntries.as_view(), name="api_logger_sendmail"),
					url(r'^snippet/([0-9a-f]{32})/load/$', LoggerSnippet.as_view(), name="api_logger_snippet"),
)

