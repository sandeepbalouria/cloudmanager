from django.db import models

# Create your models here.

class LoggerComponent(models.Model):
	"""
	What log components are available, e.g. compiler_1, gridslave_1, nginx, etc.
	"""
	name = models.CharField (max_length=100, null=False, blank=False)
	description =  models.CharField (max_length=100, null=False, blank=True)

	def __unicode__(self):
		return self.name


	class Meta:
		ordering = ['name']
