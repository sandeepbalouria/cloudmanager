import time
import re
import hashlib
import random
import datetime
import logging
from copy import deepcopy

from django.contrib.auth.models import User
from django.middleware.csrf import get_token
from django.conf import settings
from django.template.loader import render_to_string
from django.core.mail import EmailMultiAlternatives

from django.views.generic.base import TemplateResponseMixin
from django.views.generic.detail import BaseDetailView

from braces.views import JSONResponseMixin
from rest_framework.views import APIView

from cloudmanager.models import Cluster
from cloudmanager.forms import LoggerEmailForm
from cloudmanager.views import BaseCMView

from boto3.session import Session

logger = logging.getLogger(__name__)

############################################################
# Patch Dynamizer to support dict/list
############################################################
from boto.dynamodb.types import LossyFloatDynamizer as Dynamizer, get_dynamodb_type


def _get_dynamodb_type(self, attr):
	if isinstance(attr, dict):
		return 'M'
	if isinstance(attr, list):
		return 'L'
	return get_dynamodb_type(attr)


def _encode_m(self, attr):
	result = {}
	for k, v in attr.items():
		result[k] = self.encode(v)
	return result


def _decode_m(self, attr):
	result = {}
	for k, v in attr.items():
		result[k] = self.decode(v)
	return result


def _encode_l(self, attr):
	return [self.encode(v) for v in attr]


def _decode_l(self, attr):
	return [self.decode(v) for v in attr]


Dynamizer._get_dynamodb_type = _get_dynamodb_type
Dynamizer._encode_m = _encode_m
Dynamizer._decode_m = _decode_m
Dynamizer._encode_l = _encode_l
Dynamizer._decode_l = _decode_l
############################################################
# End patch Dynamizer to support dict/list
############################################################


class LogsView(BaseCMView, TemplateResponseMixin, BaseDetailView):
	"""
	Logs view.
	"""

	template_name = "cloudmanager/logs.html"
	def get( self, request, *args, **kwargs ):
		user = request.user

		user_groups = User.objects.get ( id = user.id ).groups.all()

		clusters = Cluster.objects.filter(group__in = user_groups)

		emailForm = LoggerEmailForm()

		return  self.render_to_response({'clusters': clusters, 'emailForm':emailForm})


class LogSnippetView(BaseCMView, TemplateResponseMixin, BaseDetailView):

	template_name = "cloudmanager/logs_snippet.html"

	def get( self, request, *args, **kwargs ):
		snippethash=args[0]
		return self.render_to_response({'snippethash': snippethash})


class LoggerClusterServices(APIView, JSONResponseMixin):
	"""
	Given a cluster ID, get a list of services attached to it:
	{ "cluster-id":"abc-dfg-123",
	  "error": ""
	  "services": ["compiler_1","gridslave_1","gridslave_2","process-flow-manager_1"]
	}

	Cluster to Service mapping should look something like this:
	{
	  "services":
	  [
	  "compiler_1",
	  "gridslave_1",
	  "gridslave_2"
	  ...
	  ]
	}

	testurl: http://127.0.0.1:8080/api/logger/cluster/aa5a92b5-0a51-4d7e-aad2-54ab04b68638/services
	"""

	def get(self, request, clusterid):
		data = {"error": ""}
		user = request.user
		cluster_id = clusterid
		user_groups = User.objects.get(id=user.id).groups.all()

		theClusters = Cluster.objects.filter(tmid__exact=cluster_id, group__in=user_groups)

		if len(theClusters) == 0:
			data["error"] = "Not authorized to read logs for this node or node does not exists"
		else:
			# get the service to host mapping and list them
			cluster = theClusters[0]
			# TODO: should eventually come from a service_location object
			services = cluster.attribute.get('services', None)
			if not services:
				data["error"] = "Cluster has no registered services"
			else:
				data["cluster_id"] = clusterid
				data["services"] = services

		return self.render_to_response(data)


def dynamoToDict(item):
	if type(item) is dict:
		dictItem = item

		if len(item) > 1:
			dictItem = {"M": item}
		else:
			if not dict.has_key("M"):
				dictItem = {"M": item}
	else:
		# convert it to a dict with M in front
		dictItem = {"M": item}

	dyn = Dynamizer()
	return dyn.decode(dictItem)


def dictToDynamo(item):
	dyn = Dynamizer()
	result = dyn.encode(item)
	return result


class LoggerSnippet(APIView, JSONResponseMixin):
	def get(self, request, snippethash):
		data = {"error": ""}

		session = Session(aws_access_key_id=settings.LOGGER_AWS_ACCESS_KEY_ID,
						  aws_secret_access_key=settings.LOGGER_AWS_SECRET_ACCESS_KEY,
						  region_name=settings.LOGGER_AWS_REGION)
		ddb = session.client("dynamodb")

		tableName = settings.LOGGER_SNIPPET_TABLE_NAME

		key = {
			"snippethash": {
				"S": snippethash
			}
		}
		# todo: filter out expired result at the dynamodb level using a queryfilter (eg. expires LT now)
		tmpResult = ddb.get_item(TableName=tableName, Key=key)

		if "Item" not in tmpResult:
			data["error"] = "Could not find a snippet with snippethash %s or it has already expired" % (snippethash)
		else:
			data["snippet"] = dynamoToDict(tmpResult["Item"])
		return self.render_to_response(data)


class LoggerEntries(APIView, JSONResponseMixin):
	"""
	API View to get log entries for a particular combination of cluster, node, component and level
	testurl: http://127.0.0.1:8080/api/logger/cluster/aa5a92b5-0a51-4d7e-aad2-54ab04b68638/component/compiler_1?subcomponent=main&level=all
	"""

	def get(self, request, clusterid, componentname):

		"""
		View uses a combination of clusterid, component name, subcomponent (main, stderr, stdout) (from query params)
		and level (from query params) to retrieve a list of log entries formatted in a json response.

		Three parameters control the range of entries that will be returned: startRange, endRange and limit.
		If both startRange and endRange are defined only entries between those ranges will be returned (although possibly \
		 not all of them).
		If only startRange is defined, only entries from that point and later (greater than), if only endRange is defined,
		than only entries previous to that point will be returned (less than).

		In any case, the number of records returned will depend on the consumed capacity and the value of
		settings.LOGGER_MAX_CAPACITY_UNITS_PER_REQUEST (in MB).
		"""


		data = {"error": ""}
		# do we have a convention for API requests? URLs or request params?
		user = request.user
		cluster_id = clusterid
		component = componentname
		subcomponent = request.GET.get('subcomponent', "main")
		level = int(request.GET.get('level', -1))

		limit = int(request.GET.get('limit', 100))
		if limit > settings.LOGGER_MAX_REQUESTED_LIMIT:
			limit = settings.LOGGER_MAX_REQUESTED_LIMIT

		range_start = request.GET.get("startRange", "");

		if range_start.strip() == "":
			range_start = None
		range_end = request.GET.get("endRange", "");
		if range_end.strip() == "":
			range_end = None

		lastEvaluatedKey = None

		user_groups = User.objects.get(id=user.id).groups.all()
		# basic security, user can see logs for a cluster if it belongs to a group that is part of it.
		theClusters = Cluster.objects.filter(tmid__exact=cluster_id, group__in=user_groups)

		if len(theClusters) == 0:
			data["error"] = "Not authorized to read logs for this node or node does not exists"


		else:
			cluster = theClusters[0]
			# get service-hostname mapping and build the log query.
			services = cluster.attribute.get('services', None)
			if not services:
				data["error"] = "Cluster has no registered services"
			else:

				if component not in services:
					data["error"] = "Could not find location for component %s in cluster %s" % (component, cluster_id)
				else:
					completeComponent = component + "_" + subcomponent
					clusterhash = cluster_id + "_" + completeComponent
					nowEpoch = int(time.time()) * 1e6
					queryFilter = {}

					queryParams = {
						"TableName": settings.LOGGER_TABLE_NAME,
						"QueryFilter": queryFilter,
						"ScanIndexForward": False,
						"ReturnConsumedCapacity": "TOTAL",
						"KeyConditions": {
							"clusterhash":
								{
									"ComparisonOperator": "EQ",
									"AttributeValueList": [{"S": clusterhash}]
								}
						}

					}

					timestampKeyCondition = {}

					if range_start != None and range_end != None:
						#special case when we are querying a particular range
						#use between
						timestampKeyCondition = {
							"ComparisonOperator": "BETWEEN",
							"AttributeValueList": [{"S": range_end}, {"S": range_start}]
						}

					else:

						if range_start:
							#probably when loading older records than the oldest received in the client
							#also probably used the first time anything is requested and client is only getting the latest.
							#use LE
							timestampKeyCondition = {
								"ComparisonOperator": "LT",
								"AttributeValueList": [{"S": range_start}]
							}

						else:
							if range_end:
								#probably when loading newer than the latest received in the client.
								#use GT
								timestampKeyCondition = {
									"ComparisonOperator": "GT",
									"AttributeValueList": [{"S": range_end}]
								}

						if level >= 0:
							queryFilter["level"] = {
								"ComparisonOperator": "LE",
								"AttributeValueList": [{"N": "%i" % (level)}]
							}
						else:
							#we should only limit the boto query if we have no other filter parameters
							queryParams["Limit"] = settings.LOGGER_MAX_UNFILTERED_ENTRIES

							queryParams["QueryFilter"] = queryFilter

					session = Session(aws_access_key_id=settings.LOGGER_AWS_ACCESS_KEY_ID,
									  aws_secret_access_key=settings.LOGGER_AWS_SECRET_ACCESS_KEY,
									  region_name=settings.LOGGER_AWS_REGION)
					ddb = session.client("dynamodb")
					dyn = Dynamizer()



				#only add range key conditions if we have them
				if len(timestampKeyCondition) > 0:
					queryParams["KeyConditions"]["timestamp"] = timestampKeyCondition

				data["records"] = []

				#when should we stop?

				#1. Reached record limit (soft on non ranged, hard on ranged request)
				resultRecordsSize = 0

				#2. Reached max capacity
				totalConsumedCapacity = 0

				#3. Reached a database wall (detected by LastEvaluatedKey not being in the results)
				rawLastEvaluatedKey = None
				prevRawLastEvaluatedKey = None

				#4. Tried to many times,
				maxRetries = settings.LOGGER_MAX_TRIES
				tries = 0

				while (True):
					tries += 1
					if rawLastEvaluatedKey != None:
						queryParams["ExclusiveStartKey"] = rawLastEvaluatedKey

					try:
						results = ddb.query(**queryParams)
					except Exception as err:
						data["error"] = "There was an error while querying for more results: " + str(err)
						break

					# print results['ConsumedCapacity']

					for r in results["Items"]:
						decoded = dynamoToDict(r)
						data["records"].append(decoded)

					print "Result for try %i (new/total) %i/%i" % (tries, len(results["Items"]), len(data["records"]))
					resultRecordsSize = len(data["records"])
					totalConsumedCapacity += float(results['ConsumedCapacity']["CapacityUnits"])

					if "LastEvaluatedKey" not in results:
						#if no LastEvaluatedKey, then there are no more results to search, break.
						print "No more keys to search, breaking"
						break

					rawLastEvaluatedKey = results["LastEvaluatedKey"]
					data["LastEvaluatedKey"] = dynamoToDict(rawLastEvaluatedKey)

					if totalConsumedCapacity >= settings.LOGGER_MAX_CAPACITY_UNITS_PER_REQUEST:
						print "Total Consumed Capacity exceeded (%0.2f/%0.2f), breaking" % (
						totalConsumedCapacity, settings.LOGGER_MAX_CAPACITY_UNITS_PER_REQUEST)
						break
					if resultRecordsSize >= limit:
						print "Max total records reached (%i/%i), breaking" % (resultRecordsSize, limit)
						break
					if tries >= maxRetries:
						print "Max retries reached (%i), breaking" % (tries)
						break

		return self.render_to_response(data)


def getLoggerEntry(clusterhash, timestamp, tableName=None, contextEntries=0):
	"""
	Will get the logger entry specified by clusterhash and timestamp from table TableName.
	If contextEntries is greater than 0, then the "surrounding" context entries will be retrieved as well
	(contextEntries count on each side, e.g. contextEntries=5 will get 5 entries before and 5 entries after)

	:param clusterhash: The primary hash key for the log entry (composed of clusterid_component_componentNumber_[main|stdout|stderr])
	:param timestamp: The processing timestamp of the entry.
	:param tableName: The name of the table where the log entry is found. If None (default), it is taken from settings.LOGGER_TABLE_NAME
	:param contextEntries:  If contextEntries is greater than 0, then the "surrounding" context entries will be retrieved as well
	 (contextEntries count on each side, e.g. contextEntries=5 will get 5 entries before and 5 entries after)
	:return: A dictionary with the following structure:
			{"entry": theRequestedEntry,
			"context": {
					"before": [contexEntries],
					"after": [contexEntries]
				}
			}
	"""
	results = {}
	session = Session(aws_access_key_id=settings.LOGGER_AWS_ACCESS_KEY_ID,
					  aws_secret_access_key=settings.LOGGER_AWS_SECRET_ACCESS_KEY,
					  region_name=settings.LOGGER_AWS_REGION)

	ddb = session.client("dynamodb")
	if tableName == None:
		tableName = settings.LOGGER_TABLE_NAME

	key = {
		"clusterhash": {
			"S": clusterhash
		},
		"timestamp": {

			"S": timestamp
		}
	}
	tmpResult = ddb.get_item(TableName=tableName, Key=key)
	results["entry"] = dynamoToDict(tmpResult["Item"])

	if contextEntries > 0:
		# also get N number of entries above and below the result
		results["context"] = {"before": [], "after": []}
		queryParams = {
			"TableName": tableName,
			"ScanIndexForward": False,
			"ReturnConsumedCapacity": "TOTAL",
			"Limit": contextEntries,
			"KeyConditions": {
				"clusterhash":
					{
						"ComparisonOperator": "EQ",
						"AttributeValueList": [{"S": clusterhash}]
					}
			}

		}
		queryParams1 = deepcopy(queryParams)
		queryParams1["KeyConditions"]["timestamp"] = {
			"ComparisonOperator": "LT",
			"AttributeValueList": [{"S": timestamp}]
		}
		queryParams1["ScanIndexForward"] = False

		tmpBefore = ddb.query(**queryParams1)
		for r in tmpBefore["Items"]:
			results["context"]["before"].append(dynamoToDict(r))

		queryParams2 = deepcopy(queryParams)
		queryParams2["KeyConditions"]["timestamp"] = {
			"ComparisonOperator": "GT",
			"AttributeValueList": [{"S": timestamp}]
		}
		queryParams2["ScanIndexForward"] = True
		tmpAfter = ddb.query(**queryParams2)
		for r in tmpAfter["Items"]:
			results["context"]["after"].insert(0, dynamoToDict(r))

	return results


class EMail(object):
	"""
	A wrapper around Django's EmailMultiAlternatives
	that renders txt and html templates.
	Example Usage:
	email = Email(to='oz@example.com', subject='A great non-spammy email!')
	ctx = {'username': 'Oz Katz'}
	email.text('templates/email.txt', ctx)
	email.html('templates/email.html', ctx)  # Optional
	email.send()
	"""

	def __init__(self, to, subject):
		self.to = to
		self.subject = subject.replace("\n","\\n")
		self._html = None
		self._text = None

	def _render(self, template, context):
		return render_to_string(template, context)

	def html(self, template, context):
		self._html = self._render(template, context)

	def text(self, template, context):
		self._text = self._render(template, context)

	def send(self, from_addr=None, fail_silently=False):
		if isinstance(self.to, basestring):
			self.to = [self.to]
		if not from_addr:
			from_addr = getattr(settings, 'DEFAULT_FROM_EMAIL')
		msg = EmailMultiAlternatives(
			self.subject,
			self._text,
			from_addr,
			self.to
		)
		if self._html:
			msg.attach_alternative(self._html, 'text/html')
		msg.send(fail_silently)


def color_log_entry(entry, fade=False):
	"""
	Appends bgcolor key to a log entry so that it can be used in a template to color the BG.
	:param entry: The entry to color
	:param fade: Whether to use a faded (disabled) tone.
	:return: Returns the modified entry.
	"""
	number_to_color = {
		False: {
			0: "#d9534f",  # ERROR",
			1: "#f0ad4e",  #WARNING",
			2: "#5cb85c",  #INFO",
			3: "#5bc0de",  #debug
			4: "#5bc0de"  #trace
		},
		True: {
			0: "#d9a09e",  # ERROR",
			1: "#f0dbbd",  #WARNING",
			2: "#9eb89e",  #INFO",
			3: "#b9d6de",  #debug
			4: "#b9d6de"  #trace
		}
	}

	if "bgcolor" not in entry:
		entry["bgcolor"] = number_to_color[fade][int(entry["level"])]

	return entry


def color_log_entries(list_of_entries, fade=False):
	"""
	Shortcut function to color a list of entries using color_log_entries
	:param list_of_entries: A list of entries to color
	:param fade: Whether to use a faded (disabled) tone.
	:return: A list of log entry items.
	"""
	results = []
	for entry in list_of_entries:
		results.append(color_log_entry(entry, fade=fade))

	return results


def unix_time(datetimeObject):
	"""
	Converts a date time object to a unix epoch (seconds)
	:param datetimeObject: The datetime object to convert
	:return: Total number of seconds from 1 January 1970 to passed dt
	"""
	epoch = datetime.datetime.utcfromtimestamp(0)
	delta = datetimeObject - epoch
	return delta.total_seconds()


def save_logger_snippet(clusterName, entry, contextEntries, creator, expireInSeconds=None, table_name=None):
	"""
	Takes the elements of a Logger snippet (clustername, creator, expires in , entry and contextEntries) and saves them
	to the specified tableName in DynamoDB.

	The hash for the DynamoDB entry is composed of the md5 hex digest of the str representation of the entry itself + 25 random bits.

	:param clusterName: The cluster name from which this snippet was taken
	:param entry: A dictionary object with the entry as retrieved from the log dynamodb table.
	:param contextEntries: A dictionary with two keys: before and after. Both contain a number of entries from before and after
			the entry parameter.
	:param creator: The username of who is creating this snippet.
	:param expireInSeconds: The snippet's expiration date will be now + expireInSeconds. Defaults to settings.LOGGER_SNIPPET_EXPIRATION_TIME
	:param table_name: The DynamoDB table where the snippet will be stored. Defaults to settings.LOGGER_SNIPPET_TABLE_NAME
	:return: The calculated hash key of the entry. It can be used to retrieve the snippet later on.
	"""
	session = Session(
		aws_access_key_id=settings.LOGGER_AWS_ACCESS_KEY_ID,
		aws_secret_access_key=settings.LOGGER_AWS_SECRET_ACCESS_KEY,
		region_name=settings.LOGGER_AWS_REGION
	)

	ddb = session.client("dynamodb")

	if table_name == None:
		table_name = settings.LOGGER_SNIPPET_TABLE_NAME

	if expireInSeconds == None:
		expireInSeconds = settings.LOGGER_SNIPPET_EXPIRATION_TIME

	expires = unix_time(datetime.datetime.now() + datetime.timedelta(seconds=expireInSeconds))

	hasher = hashlib.md5()

	theItem = {
		"creator": creator,
		"entry": entry,
		"context": contextEntries,
		"expires": expires,
		"cluster": clusterName
	}

	hasher.update(str(theItem))
	hasher.update(str(random.getrandbits(25)))
	snippethash = hasher.hexdigest()
	theItem["snippethash"] = snippethash

	dynamizedItem = dictToDynamo(theItem)["M"]
	tmpResult = ddb.put_item(TableName=table_name, Item=dynamizedItem)

	return snippethash


class LoggerEmailLogEntries(APIView, JSONResponseMixin):
	"""
	Calling this view while posting a LoggerEmailForm form will save a snippet of the log entry along with it's context
	and will then email the recipient an HTML formatted message similar to what the original sender was seeing when he sent it.

	 The email also contains a link to the snippet.
	"""

	def post(self, request):
		"""
		Calling this view while posting a LoggerEmailForm form will save a snippet of the log entry along with it's context
		and will then email the recipient an HTML formatted message similar to what the original sender was seeing when he sent it.

		The email also contains a link to the snippet.
		:param request: A POST request with a LoggerEmailForm encoded
		:return: A JSON response with either {"error": ""} (which means sucess)
				or {"error": "some error info",
					"form_errors": {},
					"emailForm": {},
					"csrf_token":  }

				The second form happens when there is a form validation error and the errors are contained in (form_errors)
				The html for the bound form is in "emailForm",
				The csrf_token field should be used in the next post to avoid csrf issues (passed through the form in a hidden field)
		"""

		data = {"error": ""}
		request.POST.get("")
		theForm = LoggerEmailForm(request.POST)

		if theForm.is_valid():
			# get the log entry
			clusterHash = theForm.cleaned_data['clusterhash']
			timestamp = theForm.cleaned_data['timestamp']
			theLogMessage = getLoggerEntry(clusterHash, timestamp, contextEntries=5)


			#other data for email
			clusterName = ""
			clusterID = ""
			component = ""
			m = re.search("([a-zA-Z0-9\-]*)_(.*)$", clusterHash)
			if m:
				clusterID = m.group(1)
				component = m.group(2)

			user_groups = User.objects.get(id=request.user.id).groups.all()
			theClusters = Cluster.objects.filter(tmid__exact=clusterID, group__in=user_groups)

			if len(theClusters) > 0:
				clusterName = theClusters[0].name


			#before sending the email, create a logger snippet and store it in dynamo.
			#we will include the snippet id in email.
			snippetHash = save_logger_snippet(clusterName, theLogMessage["entry"], theLogMessage["context"],
											request.user.email)


			#send the email
			subjectSizeLimit = 80
			payload = theLogMessage["entry"]["payload"]["message"]
			truncatedPayload = (payload[:subjectSizeLimit] + '...') if len(payload) > subjectSizeLimit else payload
			subject = "Log entry: " + truncatedPayload

			email = EMail(to=theForm.cleaned_data['emails'], subject=subject)
			ctx = {}
			ctx["sendername"] = request.user.get_full_name()
			ctx["extra_message"] = theForm.cleaned_data["message"]
			ctx["log_message"] = color_log_entry(theLogMessage["entry"])
			ctx["clusterName"] = clusterName
			ctx["component"] = component.replace("_", " ")
			ctx["before"] = color_log_entries(theLogMessage["context"]["before"], fade=True)
			ctx["after"] = color_log_entries(theLogMessage["context"]["after"], fade=True)
			ctx["bodyWidth"] = "1000"
			ctx["snippet_url"] = request.build_absolute_uri("/cm/logger/snippet/%s/" % (snippetHash))

			email.html("cloudmanager/logEmail.html", ctx)
			email.send()

		else:
			data["error"] = "Form errors"
			data["form_errors"] = theForm.errors

		theParams = {}
		theParams["emailForm"] = theForm
		theParams["csrf_token"] = get_token(request)
		data["form_string"] = render_to_string("cloudmanager/logEmailForm.html", theParams)
		return self.render_to_response(data)


