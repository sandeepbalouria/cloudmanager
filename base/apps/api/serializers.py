from rest_framework import serializers

from tenant.models import Tenant


class TenantSerializer(serializers.ModelSerializer):
	class Meta:
		model = Tenant
		fields = ('tmid', 'name')
