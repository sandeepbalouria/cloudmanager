import logging

from django.contrib.auth.models import User

from rest_framework.response import Response
from rest_framework.views import APIView
from braces.views import JSONResponseMixin

from tenant.models import Tenant
from saltlib.slib import ClusterData
from .serializers import TenantSerializer


logger = logging.getLogger(__name__)




class ClusterBase(object):
	"""
	View to get cluster object.
	"""

	def getClusters(self, user):
		"""
		Return a list of all users.
		"""
		cluster_ids = []
		groups = user.groups.all()
		for x in groups:
			for y in x.cluster_set.all():
				cluster_ids.append(y.tmid)

		return cluster_ids


class UserList(APIView, JSONResponseMixin):
	"""
	List all users. Demo(Remove later)
	"""

	def get(self, request, format=None, *args, **kwargs):
		return self.render_to_response('')


class TenantList(APIView):
	"""
	Get tenants list.
	"""

	def get(self, request, format=None):
		# FIXME : write dynamic filter (should use many parameters from GET as filter)
		tenant_name = request.GET.get('tenant_name')
		tenant = Tenant.objects.filter(name=tenant_name).all()
		serializer = TenantSerializer(tenant, many=True)
		logger.error(serializer.data)
		return Response(serializer.data)


class ListUsers(APIView, JSONResponseMixin):
	"""
	View to list all users in the system.

	* Requires token authentication.
	* Only admin users are able to access this view.
	"""


	def get(self, request, format=None):
		"""
		Return a list of all users.
		"""
		usernames = [user.username for user in User.objects.all()]
		return self.render_to_response(usernames)


class CntClusterTenent(APIView, JSONResponseMixin):
	"""
	View to list all users in the system.

	* Requires token authentication.
	* Only admin users are able to access this view.
	"""


	def get(self, request, format=None):
		"""
		Return a list of all users.
		"""
		data = self.get_cluster_and_tenant_count(request.user)
		return self.render_to_response(data)


class GetClusterData(APIView, JSONResponseMixin, ClusterBase, ClusterData):
	"""
	View to generate data  for cluter view widgets
	"""


	def get(self, request, format=None):
		"""
		Return a list of all users.
		"""
		cluster_ids = self.getClusters(request.user)
		data = self.getClusterStatus(cluster_ids)

		return self.render_to_response(data)


class GetFirstrunData(APIView, JSONResponseMixin, ClusterBase, ClusterData):
	"""
	Return true/false if cluster is done with firstrun
	"""

	def get(self, request):
		"""
		Return a list of all users.
		"""
		# cluster_ids = self.getClusters(request.user)
		# data = self.get_Firstrun_Status(cluster_ids)
		if request.is_ajax():
			cluster_id = request.GET.get('id', None)
		data = self.get_Firstrun_Status(cluster_id)

		return self.render_to_response(data)


class GetClusterServiceData(APIView, JSONResponseMixin, ClusterBase, ClusterData):
	"""
	View to get cluster services status
	"""

	def get(self, request):
		"""
		Return a dic of service status
		"""

		if request.is_ajax():
			cluster_id = request.GET.get('id', None)
		data = self.get_Cluster_Service_Status(cluster_id)

		return self.render_to_response(data)


class GetSystemServiceStatus(APIView, JSONResponseMixin, ClusterBase, ClusterData):
	"""
	View to get System services status
	"""

	def get(self, request):
		"""
		Return a dic of service status
		"""

		if request.is_ajax():
			cluster_id = request.GET.get('id', None)
		data = self.get_System_Service_Status(cluster_id)

		return self.render_to_response(data)


class ClusterServiceAction(APIView, JSONResponseMixin, ClusterBase, ClusterData):
	"""
	View to get initial status
	"""

	def get(self, request):
		"""
		Return a list of all users.
		"""
		if request.is_ajax():
			cluster_id = request.GET.get('cluster_id', None)
			service = request.GET.get('service', None)
			action = request.GET.get('action', None)
		data = self.cluster_Service_Action(cluster_id, service, action)
		return self.render_to_response(data)


class SystemServiceAction(APIView, JSONResponseMixin, ClusterBase, ClusterData):
	"""
	View to get initial status
	"""

	def get(self, request):
		"""
		Return a list of all users.
		"""
		if request.is_ajax():
			cluster_id = request.GET.get('cluster_id', None)
			service = request.GET.get('service', None)
			action = request.GET.get('action', None)
		data = self.system_Service_Action(cluster_id, service, action)
		return self.render_to_response(data)

