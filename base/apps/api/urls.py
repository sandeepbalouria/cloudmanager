from django.conf.urls import patterns, url

from .views import ListUsers, TenantList
from .views import GetClusterData, GetFirstrunData, GetClusterServiceData, ClusterServiceAction, \
	GetSystemServiceStatus, SystemServiceAction

from htools.decorators import staff_only

urlpatterns = patterns('',
					url(r'^users/$', staff_only(ListUsers.as_view()), name="api_users"),
					url(r'^cluster/data/$', staff_only(GetClusterData.as_view()), name="get_cluster_data"),
					url(r'^firstrun/data/$', staff_only(GetFirstrunData.as_view()), name="get_firstrun_data"),
					url(r'^service/data/$', staff_only(GetClusterServiceData.as_view()), name="get_cluster_service_data"),
					url(r'^system/service/status/$', staff_only(GetSystemServiceStatus.as_view()), name="get_system_service_status"),


					url(r'^cluster/service/action$', staff_only(ClusterServiceAction.as_view()), name="cluster_service_action"),
					url(r'^system/service/action$', staff_only(SystemServiceAction.as_view()), name="system_service_action"),
					url(r'^tenant/list$', staff_only(TenantList.as_view()), name="api_tenant_id"),
)
