from django.conf.urls import patterns, url

from htools.decorators import staff_only

from .views import (
	Dashboard, RepositoriesView, ModalDeployTm, JvmExtraOptions_modal, Modal_jvmoptions_tm,
	LoggerLevels_modal, Modal_loggerlevels_tm, BuildsView, ReportsView, TestingView, ModalDeployerView, ClustersView,
	DeployerView, DataListNodesStatusView, DataNodesTopView, DataTnCView
)


urlpatterns = patterns(
	'',
	url(r'^$', Dashboard.as_view(), name="cm_dashboard"),
	url(r'^clusters/$', staff_only(ClustersView.as_view()), name="cm_clusters"),
	url(r'^repositories/$', staff_only(RepositoriesView.as_view()), name="cm_repositories"),
	url(r'^builds/$', staff_only(BuildsView.as_view()), name="cm_builds"),
	url(r'^testing/$', staff_only(TestingView.as_view()), name="cm_testing"),
	url(r'^reports/$', staff_only(ReportsView.as_view()), name="cm_reports"),
	url(r'^deployer/$', staff_only(DeployerView.as_view()), name="cm_deployer"),
	url(r'^clusters/version/update$', staff_only(ModalDeployerView.as_view()), name="deployer_modal"),
	url(r'^cluster/code/deploy$', staff_only(ModalDeployTm.as_view()), name="modal_deploy_tm"),
	url(r'^cluster/service/jvm$', staff_only(JvmExtraOptions_modal.as_view()), name="jvmExtraOptions_modal"),
	url(r'^cluster/service/loggerupdate$', staff_only(Modal_loggerlevels_tm.as_view()), name="modal_loggerlevels_tm"),
	url(r'^cluster/service/jvmupdate$', staff_only(Modal_jvmoptions_tm.as_view()), name="modal_jvmoptions_tm"),
	url(r'^cluster/service/loggerLevels$', staff_only(LoggerLevels_modal.as_view()), name="loggerLevels_modal"),
	url(r'^data/t_and_c/cnt/$', staff_only(DataTnCView.as_view()), name="dt_tenant_and_cluster_cnt"),
	url(r'^data/nodes/status/$', staff_only(DataListNodesStatusView.as_view()), name="dt_nodes_status"),
	url(r'^data/nodes/top/$', staff_only(DataNodesTopView.as_view()), name="dt_nodes_top"),
)

