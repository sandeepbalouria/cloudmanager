from django import forms
from django.forms.fields import Field
from ctypes import c_double
from django.forms.util import ErrorList
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.core.validators import validate_email, EMPTY_VALUES


class ClusterForm(forms.Form):
	CLUSTER_TYPE = (
		('SN', 'Single Node'),
		('MN', 'Multi Node'),
	)

	cluster_name = forms.CharField(label="Cluster Name", max_length=100)
	cluster_type = forms.ChoiceField(label="Cluster Type", choices=CLUSTER_TYPE, required=True)
	platform_version = forms.ChoiceField(label="Platform version", required=True)

	def __init__(self, *args, **kwargs):
			super(ClusterForm, self).__init__(*args, **kwargs)
			if self.initial.get('platform_version', None):
				self.fields['platform_version'].choices = self.initial['platform_version']



class TmVersionForm(forms.Form):
	PLATFORM_TYPE = (
		        ('release', 'Release'),
		        ('snapshot', 'Snapshot'),
		    )	
	
	platform_type = forms.ChoiceField(label='Platform type', choices=PLATFORM_TYPE)
	cluster_id = forms.CharField(widget=forms.HiddenInput())
	platform_version = forms.ChoiceField(label="Platform version", required=True)
	
	def __init__(self, *args, **kwargs):
		super(TmVersionForm, self).__init__(*args, **kwargs)
		if self.initial.get('platform_version', None):
			self.fields['platform_version'].choices = self.initial ['platform_version']
		
		
			
class JvmExtraOptions(forms.Form):
	
	jvmExtraOptions = forms.CharField()
	cluster_id = forms.CharField(widget=forms.HiddenInput())
	service = forms.CharField(widget=forms.HiddenInput())
	
	
	
class loggerLevels(forms.Form):
	
	cluster_id = forms.CharField(widget=forms.HiddenInput())
	service = forms.CharField(widget=forms.HiddenInput())	
	
	org_hibernate_compiler = forms.CharField(required=False)
	org_proferi_compiler = forms.CharField(required=False)
	org_infinispan_compiler =  forms.CharField(required=False)
	org_gridgain_compiler = forms.CharField(required=False)
	com_proferi_platform_gridmanager_gridgain_tcplite_compiler = forms.CharField(required=False)
	com_proferi_platform_gridmanager_job_DumpJob_compiler= forms.CharField(required=False)
	com_tidemark_compiler= forms.CharField(required=False)
	com_hazelcast_compiler= forms.CharField(required=False)
	root_compiler 	= forms.CharField(required=False)
		
	org_hibernate_dataio= forms.CharField(required=False)
	org_proferi_dataio= forms.CharField(required=False)
	org_infinispan_dataio= forms.CharField(required=False)
	org_gridgain_dataio= forms.CharField(required=False)
	com_proferi_platform_gridmanager_gridgain_tcplite_dataio = forms.CharField(required=False)
	com_proferi_platform_gridmanager_job_DumpJob_dataio = forms.CharField(required=False)
	com_tidemark_dataio= forms.CharField(required=False)
	com_hazelcast_dataio= forms.CharField(required=False)
	org_linkedin_norbert_dataio= forms.CharField(required=False)
	com_tidemark_net= forms.CharField(required=False)
	root_dataio= forms.CharField(required=False)
	
		

	org_hibernate_pf= forms.CharField(required=False)
	org_proferi_pf= forms.CharField(required=False)
	org_infinispan_pf= forms.CharField(required=False)
	org_gridgain_pf= forms.CharField(required=False)
	com_proferi_platform_gridmanager_gridgain_tcplite_pf= forms.CharField(required=False)
	com_proferi_platform_gridmanager_job_DumpJob_pf= forms.CharField(required=False)
	com_tidemark_pf= forms.CharField(required=False)
	root_pf= forms.CharField(required=False)
	
		

	org_hibernate_rsm= forms.CharField(required=False)
	org_proferi_rsm= forms.CharField(required=False)
	org_infinispan_rsm= forms.CharField(required=False)
	org_gridgain_rsm= forms.CharField(required=False)
	com_proferi_platform_gridmanager_gridgain_tcplite_rsm= forms.CharField(required=False)
	com_proferi_platform_gridmanager_job_DumpJob_rsm= forms.CharField(required=False)
	com_tidemark_rsm= forms.CharField(required=False)
	root_rsm= forms.CharField(required=False)
	
	
	
	org_hibernate_grid= forms.CharField(required=False)
	org_proferi_grid= forms.CharField(required=False)
	org_infinispan_grid= forms.CharField(required=False)
	org_gridgain_grid= forms.CharField(required=False)
	com_proferi_platform_gridmanager_gridgain_tcplite_grid= forms.CharField(required=False)
	com_proferi_platform_gridmanager_job_DumpJob_grid= forms.CharField(required=False)
	com_tidemark_grid= forms.CharField(required=False)
	root_grid= forms.CharField(required=False)
				
				
			
class LoggerForm(forms.Form):
	LOGGER_LEVELS = ( 
			( 'fatal', 'fatal' ),
			( 'error', 'error' ),
	        ( 'warn', 'warn' ),
	        ( 'info', 'info' ),
	        ( 'debug', 'debug' ),
	        ( 'trace', 'trace' ),
	)
	
	cluster_name = forms.CharField(label="Cluster", max_length=100)
	node_name = forms.ChoiceField(label="Node", required=True)
	component_name = forms.ChoiceField(label="Component", required=True)
	level = forms = forms.ChoiceField(choices=LOGGER_LEVELS)
	def __init__(self, *args, **kwargs):
			super(LoggerForm, self).__init__(*args, **kwargs)





class CommaSeparatedEmailField(Field):
	description = _(u"E-mail address(es)")

	def __init__(self, *args, **kwargs):
		self.token = kwargs.pop("token", ",")
		super(CommaSeparatedEmailField, self).__init__(*args, **kwargs)

	def to_python(self, value):
		if value in EMPTY_VALUES:
			return []

		value = [item.strip() for item in value.split(self.token) if item.strip()]

		return list(set(value))

	def clean(self, value):
		"""
		Check that the field contains one or more 'comma-separated' emails
		and normalizes the data to a list of the email strings.
		"""
		value = self.to_python(value)

		if value in EMPTY_VALUES and self.required:
			raise forms.ValidationError(_(u"This field is required."))

		for email in value:
			validate_email(email)

		return value


class LoggerEmailForm(forms.Form):
	emails = CommaSeparatedEmailField(widget=forms.TextInput(attrs={'size': 40}), initial="dgollas@tidemark.com")
	message = forms.CharField(required=False, widget=forms.Textarea(attrs={'rows': 3, 'cols': 40}), initial="Check out this log message")
	clusterhash = forms.CharField(widget=forms.HiddenInput())
	timestamp = forms.CharField(widget=forms.HiddenInput())
