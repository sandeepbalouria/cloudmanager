from django.db import models
from django.contrib.auth.models import User, Group
from django.utils.translation import ugettext_lazy as _
import jsonfield


class Cluster(models.Model):
	"""
	A single cluster.
	"""
	CLUSTER_TYPE_CHOICES = ( 
		( 'MN', 'MultiNode' ),
		( 'SN', 'SingleNode' ),
	)

	STATE_CHOICES = (
		( 'UP', 'Up' ),
		( 'DWN', 'Down' ),
		( 'MNT', 'Maintance' ),
		( 'OFF', 'Off' ),
		( 'PEN', 'pending' ),
	)

	tmid = models.CharField( _( "cluster id" ), max_length = 100 )
	type = models.CharField ( max_length = 2, choices = CLUSTER_TYPE_CHOICES )
	name = models.CharField(_("name"), max_length=50)
	state = models.CharField(max_length=3, choices=STATE_CHOICES)
	group = models.ManyToManyField(Group)
	attribute = jsonfield.JSONField()

	def __unicode__(self):
		return self.name

	class Meta:
		ordering = ['id']
		verbose_name = _("cluster")
		verbose_name_plural = _("clusters")

class Node(models.Model):

	"""
    A single node.
    """

	NODE_TYPE_CHOICES = (
		( 'GS', 'GridSlave' ),
		( 'WA', 'WebApp' ),
		( 'FG', 'Forge' ),
		( 'DN', 'DataNode' ),
		( 'SNP', 'Snap' ),
		( 'SN', 'SingleNode' ),
		( 'PF', 'ProcessFlow' ),
		( 'RM', 'ResultSetManager' ),
	)

	tmid = models.CharField (_("node id"), max_length=36)
	hostname = models.CharField(max_length=200, null=False, blank=True, default="")
	nodeType = models.CharField ( max_length = 3, choices = NODE_TYPE_CHOICES )
	maintenance = models.BooleanField (default=False)
	cluster = models.ForeignKey (Cluster)
	attribute = jsonfield.JSONField ()

	def __unicode__ (self):
		return self.tmid

	class Meta:
		ordering = ['id']
		verbose_name = _("node")
		verbose_name_plural = _("nodes")


