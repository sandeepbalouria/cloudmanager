__author__ = 'dgollas'
import os
import glob
import re
import tempfile
import json
import uuid

from optparse import make_option

import fabric.api
import fabric.state

from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import Group
from cloudmanager.models import Cluster, Node as djNode


def getFilesInDir(thePath, globPattern=r".*", recursive=False):
	result = []
	globString = os.path.join(thePath, globPattern)
	result = glob.glob(globString)

	if recursive:
		# get all subdirs and extend result with getting files in dir for those
		for root, dirs, files in os.walk(thePath):
			for name in dirs:
				result.extend(getFilesInDir(os.path.join(root, dirs), globPattern))

	return result


class Cloud(object):

	def __init__(self, name=None):
		self.name = name
		self.cluster_hash = None
		self.cluster_type = None # "single" | "multi"
		self.nodes = []
		self.attributes = {"services":["compiler_1",
									"dataio_1",
									"process-flow_1",
									"resultsetmanager_1",
									"gridslave_1",
									"gridslave_2",
									"tomcat_1",
									"zookeeper_1",
									"hdfsdatanode_1",
									"hdfsnamenode_1",
									"search_1",
									"nginx_1"],
							"cluster_version" : "1.11.0.7"
						   }

	def attributes_as_json(self):
		return json.dumps(self.attributes)





class Node(object):

	def __init__(self):
		self.nodeid = ""
		self.name = ""
		self.role = ""

	def display(self):
		result = " -- "
		result += self.name + "("+self.role+")"
		return result

	def get_node_type_code(self):
		#FROM THE MODEL:
		# NODE_TYPE_CHOICES = (
		# ( 'GS', 'GridSlave' ),
		# ( 'WA', 'WebApp' ),
		# ( 'FG', 'Forge' ),
		# ( 'DN', 'DataNode' ),
		# ( 'SNP', 'Snap' ),
		# ( 'SN', 'SingleNode' ),
		# ( 'PF', 'ProcessFlow' ),
		# ( 'RM', 'ResultSetManager' ),
		# )

		types = {
			"single":"SN",
			"forge":"FG",
			"data":"DN",
			"snap":"SNP",
			"web":"WA"
		}

		return types.get(self.role, "SN")






class Command(BaseCommand):

	help = "Reads cluster info from puppet server and creates CM2 clusters based on that"

	option_list = BaseCommand.option_list + (
		make_option('--puppet_url',
					default = "puppet-ubuntu.usw1.aws.tidemark.net",
					help = "The URL of the puppet server we will read from"),
		make_option('--cloud_filter',
					default = "",
					help = "A string representing a regex. If a cloud in the puppet server matches this, it will be processed. If left empty, all clouds will match"),
	make_option('--cloud_ignore',
				default="",
				help="A string representing a regex. If a cloud in the puppet server matches this, it will be IGNORED. If left empty, no clouds will be ignored")

	)

	def __init__(self):
		super(Command, self).__init__()
		self.cloud_filter = ""
		self.cloud_ignore = ""
		self.puppet_url = ""


	def handle(self, *args, **options):
		self.puppet_url = options.get("puppet_url")
		self.cloud_filter = options.get("cloud_filter")
		self.cloud_ignore = options.get("cloud_ignore")

		print "This is the puppet url "+self.puppet_url

		self.ingest_puppet_clouds()

	def ingest_puppet_clouds(self):

		puppet_url = "puppet-ubuntu.usw1.aws.tidemark.net"
		# todo: get this from command line

		sshKeys = []
		sshkeysdir = os.path.expanduser("~/.ssh/")
		if sshkeysdir.strip() != "":
			sshKeys = getFilesInDir(sshkeysdir, globPattern="*.pem", recursive=True)
		username = "ubuntu"
		fabricCmd = fabric.api.run
		if username == "root":
			fabricCmd = fabric.api.sudo
		host_string = username + "@" + self.puppet_url
		clouds = []
		# scan manifests directory and manifest files
		base_manifests_dirname = "/etc/puppet/manifests/"
		with fabric.api.settings(host_string=host_string, key_filename=sshKeys, disable_known_hosts=True):
			fabric.state.output['stdout'] = False
			print "Finding clouds in %s..." % (base_manifests_dirname)
			cmd = "ls -1 %s" % (base_manifests_dirname)
			output = fabric.api.run(cmd)
			tmpClouds = output.split()
			prog = re.compile(self.cloud_filter)
			ignore_prog = re.compile(self.cloud_ignore)
			for cloud in tmpClouds:
				if prog.match(cloud) is not None:
					if self.cloud_ignore != "":
						if ignore_prog.match(cloud) is None:
							clouds.append(Cloud(name=cloud))
					else:
						clouds.append(Cloud(name=cloud))
		cluster_hash_regex = r'''\$cluster_hash\ +=\ +['"]{1}(.+)['"]{1}'''
		cluster_hash_prog = re.compile(cluster_hash_regex)

		cluster_node_type_regex = r'''^[\s\S]*node\ +'(?P<hostname>.+\-(?P<role>(web|data|forge|single|snap)).*)\'+'''
		cluster_node_type_prog = re.compile(cluster_node_type_regex)

		#for each manifest
		with fabric.api.settings(host_string=host_string, key_filename=sshKeys, disable_known_hosts=True, warn_only=True):
			fabric.state.output['stdout'] = False
			for cloud in clouds:
				print "Finding info for %s" % (cloud.name)
				# check for existence of cluster_id, if not there, add it with comment.
				manifest_filename = os.path.join(base_manifests_dirname, cloud.name, "%s-site.pp" % (cloud.name))
				try:
					with tempfile.TemporaryFile() as fd:
						fabric.api.get(manifest_filename, fd)
						fd.seek(0)
						# manifest_contents = fd.read().split("\n")
						# print "Contents of %s: \n\n%s" %(manifest_filename, manifest_contents)
						for line in fd:
							cluster_hash_match = cluster_hash_prog.match(line)
							if cluster_hash_match:
								cloud.cluster_hash = cluster_hash_match.group(1)

							cluster_node_type_match = cluster_node_type_prog.match(line)

							if cluster_node_type_match:
								role = cluster_node_type_match.group('role')
								if role in ['web','data','forge']:
									cloud.cluster_type = "MN"
								elif role in ['single']:
									cloud.cluster_type = "SN"

								tmp_node = Node()
								tmp_node.role = role
								tmp_node.name = cluster_node_type_match.group('hostname')
								cloud.nodes.append(tmp_node)



							#print "Found cluster hash for %s: %s" % (cloud.name, cloud.cluster_hash)

				except Exception as e:
					print "Exception while looking for manifest %s: \n%s" % (manifest_filename, str(e))

			print "\n\nFound info for %i clouds:\n" % (len(clouds))

			group_name = "ops"
			opsgroup = None
			try:

				opsgroup = Group.objects.get(name=group_name)
			except ObjectDoesNotExist as e:
				print "%s group does not exists, creating" % (group_name)
				opsgroup = Group(name=group_name)
				opsgroup.save()

			for cloud in clouds:
				# check if this cluster is not already registered, if not, register it.
				print "\n*** PROCESSING %s (%s)" % (cloud.name, cloud.cluster_type)
				print "Nodes:"
				for node in cloud.nodes:
					print node.display()
				if cloud.cluster_hash and cloud.cluster_hash != "":
					print "%s: %s" % (cloud.name, cloud.cluster_hash)
					tmpClusters = Cluster.objects.all().filter(name = cloud.name)
					if len(tmpClusters):
						print "%s already registered with cluster_id=%s! skipping" %(cloud.name, tmpClusters[0].tmid)
					else:
						print "%s does not exist in django, creating..." % (cloud.name)
						tmpCluster = Cluster(tmid=cloud.cluster_hash,
											 type=cloud.cluster_type,
											 name=cloud.name,
											attribute=cloud.attributes,
											 state = "UP",
											)
						tmpCluster.save()
						tmpCluster.group.add(opsgroup)

						for node in cloud.nodes:
							print "creating node: %s" % (node.display())
							tmpNode = djNode(tmid=str(uuid.uuid4()),
											 hostname=node.name,
											 nodeType = node.get_node_type_code(),
											 maintenance = False,
											 cluster = tmpCluster
											 )
							tmpNode.save()



				else:
					print "%s: has no cluster_hash, skipping" % (cloud.name)

				#check for existence of cm2logs puppet module statements, adding if necessary.






if __name__ == "__main__":
	ingest_puppet_clouds()