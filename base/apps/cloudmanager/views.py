import logging
import os
import uuid

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.models import User
from django.core.mail import EmailMultiAlternatives
from django.shortcuts import redirect
from django.template import Template, Context
from django.template.loader import render_to_string
from django.views.generic.base import TemplateResponseMixin
from django.views.generic.detail import BaseDetailView
from django.views.generic.edit import FormView

from braces.views import LoginRequiredMixin, JSONResponseMixin
from .models import Cluster, Node
from saltlib.slib import ClusterData, SingleNode, MultiNode

from .forms import ClusterForm, TmVersionForm, JvmExtraOptions, loggerLevels, LoggerEmailForm


logger = logging.getLogger(__name__)


class BaseCMView( LoginRequiredMixin ):

	def dispatch( self, request, *args, **kwargs ):
		return super( BaseCMView, self ).dispatch( request, *args, **kwargs )

	def get_clusters_and_tenants( self, user ):
		tenants_list = []
		clusters_list = []

		groups = user.groups.all()
		for x in groups:
			for i in x.tenant_set.all():
				tenants_list.append( i )

			for y in x.cluster_set.all():
				clusters_list.append( y )

		tnc_counts = self.get_cluster_and_tenant_count( user )

		data = {
			'tenant_list':tenants_list,
			'tenant_cnt':tnc_counts['tenant_cnt'],
			'cluster_list':clusters_list,
			'cluster_cnt':tnc_counts['cluster_cnt']
		}

		return data

	def get_cluster_and_tenant_count( self, user ):
		tenant_cnt = 0
		clusters_cnt = 0

		groups = user.groups.all()
		for x in groups:
			tenant_cnt = +x.tenant_set.count()
			clusters_cnt = +x.cluster_set.count()

		data = {
			'tenant_cnt':tenant_cnt,
			'cluster_cnt':clusters_cnt
		}

		return data

	# TODO: rename method to "get_platform_version" as we get information about only one version. @ALX
	#
	def get_platform_versions(self):
		releases = os.listdir('/srv/salt/salt/tidemark/releases')
		snapshots = os.listdir('/srv/salt/salt/tidemark/snapshots')
		versions = {
			'releases': releases,
			'snapshots': snapshots
		}
		return versions


class Dashboard(BaseCMView, TemplateResponseMixin, BaseDetailView):
	"""
	Dashboard view.

	"""
	template_name = "cloudmanager/dashboard.html"
	def get(self, request, *args, **kwargs):
		# email = Send_EMail(to='lecole@tidemark.com', subject='This is a test email')
		# ctx = {'username':'lecole'}
		# email.text('cloudmanager/test_email.txt', ctx)
		# email.send(from_addr='mailer@tidemark.com')
		return self.render_to_response({})


class DeployerView( BaseCMView, FormView ):
	# FIXME: fix the docstring to provide more information. @ALX
	"""
	Deployer view.
	"""

	template_name = "cloudmanager/deployer.html"
	form_class = ClusterForm

	# FIXME: do we need that? @ALX
	def post( self, request, *args, **kwargs ):
		return super( DeployerView, self ).post( request, *args, **kwargs )
	
	def get_form_kwargs( self ):
		# FIXME: fix indentation @ALX
				# making initial data for the form:
				ver = self.get_platform_versions()
				versions = ver['releases'] + ver['snapshots']
				platform_version = [(version, version) for version in versions]
				kwargs = super ( DeployerView, self ).get_form_kwargs()
				kwargs['initial'] = {'platform_version':platform_version}
				return kwargs	

	def form_valid(self, form):
		services = [
		    "compiler_1",
		    "dataio_1",
		    "process-flow_1",
		    "resultsetmanager_1",
		    "gridslave_1", 
		    "tomcat_1",
		    "zookeeper_1",
		    "hdfsdatanode_1",
		    "hdfsnamenode_1",
		    "search_1",
		    "nginx_1"
		]
		
		cluster = Cluster.objects.create(
			tmid= uuid.uuid4(),
			name=form.cleaned_data['cluster_name'],
		    type=form.cleaned_data['cluster_type'],
		    attribute = {
		        'services': services,
		        'cluster_version': form.cleaned_data['platform_version']
	        },
		    
			state='PEN'                        
		)        
		# FIXME: fix the variable names. @ALX
		grp = self.request.user.groups.all()
		for x in grp:
			cluster.group.add(x)
		if form.cleaned_data['cluster_type'] == 'SN':
			s_node = Node.objects.create(
				tmid= uuid.uuid4(),
				nodeType= 'SN',
				cluster= cluster
			)

			sn = SingleNode(form.cleaned_data['cluster_name'],s_node.tmid,cluster.tmid)         
			sn.create_cluster(form.cleaned_data['platform_version'])


		elif form.cleaned_data['cluster_type'] == 'MN':
			web_node = Node.objects.create(
				tmid= uuid.uuid4(),
				nodeType= 'WA',
				cluster= cluster,                        
			)
			forge_node = Node.objects.create(
				tmid= uuid.uuid4(),
				nodeType= 'FG',
				cluster= cluster,                        
			)
			data_node = Node.objects.create(
				tmid= uuid.uuid4(),
				nodeType= 'DN',
				cluster= cluster,                        
			)             
			mn = MultiNode(form.cleaned_data['cluster_name'],web_node.tmid,forge_node.tmid,data_node.tmid,cluster.tmid)         
			mn.create_cluster(form.cleaned_data['platform_version'])         


		return redirect( 'cm_clusters' )


class ClustersView( BaseCMView, TemplateResponseMixin, BaseDetailView ):
	"""
	Clusters view.
	"""

	template_name = "cloudmanager/clusters.html"
	def get( self, request, *args, **kwargs ):
		cluster_tenants = self.get_clusters_and_tenants( request.user )
		return  self.render_to_response( cluster_tenants )


# FIXME: fix the class's name - class can't be a modal. @ALX
# FIXME: docstring missed. @ALX
class ModalDeployerView(BaseCMView, TemplateResponseMixin, BaseDetailView):
	template_name = "cloudmanager/modals/deployer.html"

	def get ( self, request, *args, **kwargs ):
		if not request.is_ajax():
			return redirect( '/' )
		clusterid = request.GET.get( 'clusterid' )
		versions = self.get_platform_versions()
		versions['clusterid'] = clusterid
		return self.render_to_response(versions)


# FIXME: fix the class's name - class can't be a modal. @ALX
class ModalDeployTm(BaseCMView, FormView,ClusterData):
	# FIXME: fix the docstring to provide more information. @ALX
	"""
	code deploy
	"""

	template_name = "cloudmanager/modals/deployer.html"
	form_class = TmVersionForm

	def post( self, request, *args, **kwargs ):
		return super( ModalDeployTm, self ).post( request, *args, **kwargs )
	
	def get_form_kwargs( self ):
		# FIXME: fix indentation. @ALX
			# making initial data for the form:
			ver = self.get_platform_versions()
			versions = ver['releases'] + ver['snapshots']
			platform_version = [(version, version) for version in versions]
			kwargs = super ( ModalDeployTm, self ).get_form_kwargs()
			kwargs['initial'] = {'platform_version':platform_version}
			return kwargs	
		
	def form_valid(self, form):
		cluster_id = form.cleaned_data['cluster_id']
		platform_version = form.cleaned_data['platform_version']
		self.platform_deploy(cluster_id,platform_version)
		cluster = Cluster.objects.get(tmid=cluster_id)
		cluster.attribute['cluster_version'] = form.cleaned_data['platform_version']
		cluster.save()
		cluster_name = cluster.name
		messages.success( 
		    self.request,
		    'Deploying {} on {} '.format(platform_version,cluster_name)
		)		
		return redirect( 'cm_clusters' )	


# FIXME: docstring missed. @ALX
# FIXME: class can't be modal. @ALX
# FIXME: class's name is not in PEP8. @ALX
class JvmExtraOptions_modal(BaseCMView, TemplateResponseMixin, BaseDetailView,ClusterData):
	template_name = "cloudmanager/modals/jvmExtraOptions_modal.html"

	def get ( self, request, *args, **kwargs ):
		if not request.is_ajax():
			return redirect( '/' )
		clusterid = request.GET.get( 'clusterid' )
		service = request.GET.get( 'service' )
		# FIXME: change the method's name in base class. @ALX
		jvmOptions = self.jvmExtraOptions_action(clusterid,service)
		jvmOptions['clusterid'] = clusterid
		jvmOptions['service'] = service
		return self.render_to_response(jvmOptions)


# FIXME: docstring missed. @ALX
# FIXME: class can't be modal. @ALX
# FIXME: class's name is not in PEP8. @ALX
class LoggerLevels_modal(BaseCMView, TemplateResponseMixin, BaseDetailView,ClusterData):
	template_name = "cloudmanager/modals/loggerLevels_modal.html"

	def get ( self, request, *args, **kwargs ):
		if not request.is_ajax():
			return redirect( '/' )
		clusterid = request.GET.get( 'clusterid' )
		service = request.GET.get( 'service' )
		loggerlevels = self.loggerLevels_action(clusterid,service)
		loggerlevels['clusterid'] = clusterid
		loggerlevels['service'] = service
		return self.render_to_response(loggerlevels)


# FIXME: class can't be modal. @ALX
# FIXME: class's name is not in PEP8. @ALX
class Modal_jvmoptions_tm(BaseCMView, FormView,ClusterData):
	# FIXME: fix the docstring to provide more information. @ALX
	"""
	code deploy
	"""

	template_name = "cloudmanager/modals/jvmExtraOptions_modal.html"
	form_class = JvmExtraOptions

	# FIXME: do we need that call? @ALX
	def post( self, request, *args, **kwargs ):
		return super( Modal_jvmoptions_tm, self ).post( request, *args, **kwargs )
	
	def form_valid(self, form):
		cluster_id = form.cleaned_data['cluster_id']
		service = form.cleaned_data['service']
		jvmoptions = form.cleaned_data['jvmExtraOptions']
		self.jvmExtraOptions_action(cluster_id,service,jvmoptions)
		cluster = Cluster.objects.get(tmid=cluster_id)
		cluster_name = cluster.name
		messages.success( 
		    self.request,
		    'Updated jvmExtra options for {} on {}. The new options are {} '.format(service,cluster_name, jvmoptions)
		)		
		return redirect( 'cm_clusters' )	


# FIXME: class's name is not in PEP8. @ALX
class Modal_loggerlevels_tm(BaseCMView, FormView,ClusterData):
	# FIXME: fix the docstring to provide more information. @ALX
	"""
	code deploy
	"""

	template_name = "cloudmanager/modals/loggerLevels_modal.html"
	form_class = loggerLevels

	# FIXME: do we need that call? @ALX
	def post( self, request, *args, **kwargs ):
		return super( Modal_loggerlevels_tm, self ).post( request, *args, **kwargs )
	
		
	def form_valid(self, form):
		loglevels = {}
		service = form.cleaned_data['service']
		cluster_id = form.cleaned_data['cluster_id']
		
		for k,v in form.cleaned_data.iteritems():
			# FIXME: in that case we can use single "if" condition without any "continue" statements
			if not k.endswith('_'+service):
				continue
			elif k == 'cluster_id' or k == 'service':
				continue
			else:
				loglevels[k] = str(v)
				
		self.loggerLevels_action(cluster_id,service,loglevels)
		cluster = Cluster.objects.get(tmid=cluster_id)
		cluster_name = cluster.name
		messages.success( 
		    self.request,
		    'Updated loggerlevels  for {} on {}. The updated levels are {} '.format(service,cluster_name, loglevels)
		)		
		return redirect( 'cm_clusters' )	



class RepositoriesView( BaseCMView, TemplateResponseMixin, BaseDetailView ):
	"""
	Repositories view.
	"""

	template_name = "cloudmanager/repositories.html"
	def get( self, request, *args, **kwargs ):
		return  self.render_to_response( '' )


class TestingView( BaseCMView, TemplateResponseMixin, BaseDetailView ):
	"""
	Testing view.
	"""

	template_name = "cloudmanager/testing.html"
	def get( self, request, *args, **kwargs ):
		return  self.render_to_response( '' )


class ReportsView( BaseCMView, TemplateResponseMixin, BaseDetailView ):
	"""
	Reports view.
	"""

	template_name = "cloudmanager/reports.html"
	def get( self, request, *args, **kwargs ):
		return  self.render_to_response( '' )


class BuildsView( BaseCMView, TemplateResponseMixin, BaseDetailView ):
	"""
	Builds view.
	"""

	template_name = "cloudmanager/builds.html"
	def get( self, request, *args, **kwargs ):
		return  self.render_to_response( '' )


class DataListNodesStatusView( BaseCMView, JSONResponseMixin, BaseDetailView ):
	"""
	List Nodes view.
	"""

	def get( self, request, *args, **kwargs ):
		nodes = salt_call( target = "*", function = "test.ping" )
		data = {
		    "nodes":nodes
		}
		return  self.render_to_response( data )


class DataNodesTopView( BaseCMView, JSONResponseMixin, BaseDetailView ):
	"""
	Top Nodes view.
	"""

	def get( self, request, *args, **kwargs ):

		nodes = salt_call( function = "ps.top" )
		data = {
		    "nodes":nodes
		}
		return  self.render_to_response( data )


class DataTnCView( BaseCMView, JSONResponseMixin, BaseDetailView ):
	"""
	Top Nodes view.
	"""

	def get( self, request, *args, **kwargs ):

		data = self.get_cluster_and_tenant_count( request.user )

		return  self.render_to_response( data )


# FIXME: class's name is not in PEP8. @ALX
class Send_EMail( object ):
	"""
	A wrapper around Django's EmailMultiAlternatives
	that renders txt and html templates.
	Example Usage:
	>>> email = Email(to='oz@example.com', subject='A great non-spammy email!')
	>>> ctx = {'username': 'Oz Katz'}
	>>> email.text('templates/email.txt', ctx)
	>>> email.html('templates/email.html', ctx)  # Optional
	>>> email.send()
	>>>
	"""
	def __init__( self, to, subject ):
		self.to = to
		self.subject = subject
		self._html = None
		self._text = None

	def _render( self, template, context ):
		return render_to_string( template, context )

	def html( self, template, context ):
		self._html = self._render( template, context )

	def dbhtml( self, template, context ):
		tpl = Template( template )
		self._html = tpl.render( Context( context ) )

	def text( self, template, context ):
		self._text = self._render( template, context )

	def send( self, from_addr = None, fail_silently = False ):
		if isinstance( self.to, basestring ):
			self.to = [self.to]
		if not from_addr:
			from_addr = getattr( settings, 'EMAIL_FROM_ADDR' )
		msg = EmailMultiAlternatives( 
			self.subject,
			self._text,
			from_addr,
			self.to
		)
		if self._html:
			msg.attach_alternative( self._html, 'text/html' )
		msg.send( fail_silently )
