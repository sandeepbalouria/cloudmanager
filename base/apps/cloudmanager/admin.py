from django.contrib import admin

from djcelery.models import TaskMeta

from cloudmanager.models import Cluster, Node


class TaskMetaAdmin(admin.ModelAdmin):
	readonly_fields = ('result',)


class ClusterAdmin(admin.ModelAdmin):
	list_display = ('id', 'name')


class NodeAdmin(admin.ModelAdmin):
	list_display = ('id', 'tmid')



admin.site.register(Cluster, ClusterAdmin)
admin.site.register(Node, NodeAdmin)
admin.site.register(TaskMeta, TaskMetaAdmin)
