from __future__ import absolute_import

import logging
import subprocess

from base.celery import app


_CLOUD_CLIENT = None


logger = logging.getLogger(__name__)


def __get_saltcloud_client():
	import salt.cloud
	global _CLOUD_CLIENT
	if not _CLOUD_CLIENT:
		_CLOUD_CLIENT = salt.cloud.CloudClient('/etc/salt/cloud')
	return _CLOUD_CLIENT


def cloud_client(cluster_map):
	sclient = __get_saltcloud_client()
	return sclient.map_run(cluster_map)


@app.task()
def provision_cluster(cluster_map):
	"""
	Description: create a cluster single node/multinode based on the map file

	:param cluster_map: the map file which defines a cluster
	:return:
	"""

	logger.debug("cluster map location " + cluster_map)
	try:
		retcode = subprocess.call(['salt-cloud', '-m', cluster_map, '-P', '-y'], shell=False)
		if retcode < 0:
			logger.debug(retcode)
		else:
			logger.debug(retcode)
	except OSError as e:
		logger.exception(e)
