from itertools import chain

from django import forms
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse
from django.utils.safestring import mark_safe

from base.apps.account.models import AllowedObjectPermission


class ContentTypeSelect(forms.Select):

	CHOICE_OUTPUT_TEMPLATE = 'var %(id)s_choice_urls = {%(choices)s};'
	CHOICE_LINK_TEMPLATE = "'%(choice)s': '%(choice_link)s',"
	TEMPLATE = """
	<script type='text/javascript'>
		(function($) {
			$(document).ready( function() {
				%(choice_output)s
				$('#%(fk_id)s').attr('href', %(id)s_choice_urls['%(choice)s'])
				$('#%(id)s').change(function (){
					$('#%(fk_id)s').attr('href', %(id)s_choice_urls[$(this).val()]);
				});
			});
		})(django.jQuery);
	</script>
	"""

	def __init__(self, lookup_id,  attrs=None, choices=()):
		self.lookup_id = lookup_id
		super(ContentTypeSelect, self).__init__(attrs, choices)

	def render(self, name, value, attrs=None, choices=()):
		output = super(ContentTypeSelect, self).render(name, value, attrs, choices)

		choices = chain(self.choices, choices)
		choices_links = ''
		for choice in choices:
			try:
				model = AllowedObjectPermission.get_model_class_by_object_type(choice[0])
				content_type = ContentType.objects.get_for_model(model)
			except (ValueError, ContentType.DoesNotExist):
				continue

			link = reverse('admin:%s_%s_changelist' % (content_type.app_label, content_type.model))
			choice_link = self.CHOICE_LINK_TEMPLATE % {
				'choice': choice[0],
				'choice_link': link,
			}
			choices_links = ''.join([choices_links, choice_link])

		select_id = attrs.get('id', '')
		choice_output = self.CHOICE_OUTPUT_TEMPLATE % {
			'id': select_id,
			'choices': choices_links
		}
		content_type_select = self.TEMPLATE % {
			'choice_output': choice_output,
			'id': select_id,
			'fk_id': self.lookup_id,
			'choice': value
		}
		output = ''.join([output, content_type_select])

		return mark_safe(u''.join(output))
