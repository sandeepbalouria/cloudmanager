from django.db import models, IntegrityError
from django.contrib.contenttypes.models import ContentType


class EmailAddressManager(models.Manager):

	def add_email(self, user, email, **kwargs):
		confirm = kwargs.pop("confirm", False)
		try:
			email_address = self.create(user=user, email=email, **kwargs)
		except IntegrityError:
			return None
		else:
			if confirm and not email_address.verified:
				email_address.send_confirmation()
			return email_address

	def get_primary(self, user):
		try:
			return self.get(user=user, primary=True)
		except self.model.DoesNotExist:
			return None

	def get_users_for(self, email):
		# this is a list rather than a generator because we probably want to
		# do a len() on it right away
		return [address.user for address in self.filter(verified=True, email=email)]


class EmailConfirmationManager(models.Manager):

	def delete_expired_confirmations(self):
		for confirmation in self.all():
			if confirmation.key_expired():
				confirmation.delete()


class AllowedObjectPermissionManager(models.Manager):

	def check_permissions(self, user, permission_items, check_all_permissions=True):

		"""
		Check the bunch of object-base action permissions for specific user
		:param user: specific user.
		:param dict permission_items: collection of actions and all object-base permissions that need to check.
		Example: {'backup': {'tenant': [100, 110], 'node': [200, 210]}}
		:param bool check_all_permissions: flag that define if function should check all permissions or
		break after first invalid

		:type user: django User ORM object

		:return: invalid permissions info
		:rtype: list of tuples. Example: [(user, 'backup', 'tenant', 100)]
		"""

		invalid_permissions = []

		for action, cloud_items in permission_items.iteritems():
			for cloud_item, cloud_item_ids in cloud_items.iteritems():
				is_allowed_model = self.model.get_model_class_by_object_type(cloud_item)
				if is_allowed_model:
					existing_cloud_item_ids = self.filter(
						user=user,
						action=action,
						object_type=cloud_item,
						object_id__in=cloud_item_ids
					).values_list(
						'object_id',
						flat=True
					)

					for item_id in (set(existing_cloud_item_ids) ^ set(cloud_item_ids)):
						invalid_permissions.append((user, action, cloud_item, item_id))

					if not check_all_permissions and invalid_permissions:
						return invalid_permissions

		return invalid_permissions

	def is_permissions_allowed(self, user, permission_items):
		return bool(self.check_permissions(user, permission_items, check_all_permissions=False))
