from django.contrib import admin

from base.apps.account.models import Account, SignupCode, AccountDeletion, EmailAddress, \
	AllowedObjectPermission
from base.apps.account.forms import AdminAllowedObjectPermissionForm


class SignupCodeAdmin(admin.ModelAdmin):

	list_display = ["code", "max_uses", "use_count", "expiry", "created"]
	search_fields = ["code", "email"]
	list_filter = ["created"]


class AllowedObjectAdmin(admin.ModelAdmin):
	form = AdminAllowedObjectPermissionForm
	filter_horizontal = ('user',)
	list_display = ('get_users', 'action', 'object_type', 'object_id', )

	def get_users(self, obj):
		return ', '.join([user.username for user in obj.user.all()])
	get_users.short_description = 'Users'


admin.site.register(Account)
admin.site.register(EmailAddress)
admin.site.register(SignupCode, SignupCodeAdmin)
admin.site.register(AccountDeletion, list_display=["email", "date_requested", "date_expunged"])
admin.site.register(AllowedObjectPermission, AllowedObjectAdmin)
