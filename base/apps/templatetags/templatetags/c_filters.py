from django import template
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.staticfiles import finders
from htools.funcs import epoch_to_datetime as ep_2_dt
from htools.funcs import time_sizeof_fmt
from htools.funcs import byte_sizeof_fmt as bytes_conv

import logging
logger = logging.getLogger(__name__)

register = template.Library()


@register.filter
def epoch_to_datetime(epoch, tz=None):
	dt = ep_2_dt(epoch, tz)
	if dt:
		return dt.strftime('%m/%d/%y %H:%M:%S')
	else:
		return ''


@register.filter
def sec_to_mhd(seconds):
	"""Converts given seconds count to minutes, hours, days	"""
	return time_sizeof_fmt(seconds)


@register.filter
def byte_sizeof_fmt(byte_cnt):
	"""Converts given bytes count to Ki, Mi etc"""
	return bytes_conv(byte_cnt)


@register.filter
def user_name(user_id):
	return User.objects.get(id=user_id).username


@register.filter
def logo_or_default(file_path):
	'''
	Checks that logo image exists in static storage. If it doesn't than default image returned.
	'''
	# if logo exists in static files than none empty string will be returned
	if file_path and finders.find(file_path,):
		return file_path
	new_filepath = settings.DEFAULT_TENANT_LOGO
	return new_filepath
