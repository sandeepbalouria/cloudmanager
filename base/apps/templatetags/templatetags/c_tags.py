from django import template
import logging

logger = logging.getLogger(__name__)
register = template.Library()


@register.inclusion_tag('templatetags/custom_tags/btn_paginator.html')
def paginate_ajax(page, display_page_count, url):
	'''
	Renders a pagination buttons group with a given count of displayed buttons.
	You should define a JS OnClick event handler for the "ajax-pgn" class to make the pagination (see the html template).

	:param page: current paginator's page
	:param display_page_count: count of buttons to display (except first and a last one)
	:param url: url to send an AJAX-request for the new page of data
	'''
	# using max to avoid negative indexes
	left_page = max(page.number - display_page_count / 2 - 1, 0)
	# if we are out of the list length we are moving our slicing borders
	if left_page + display_page_count > page.paginator.num_pages:
		left_page = max(page.paginator.num_pages - display_page_count, 0)
		page_range = page.paginator.page_range[left_page: page.paginator.num_pages]
	else:
		page_range = page.paginator.page_range[left_page: left_page + display_page_count]
	# a little magic:
	# setting the numbers of pages to quick jump to
	prev_jump = max(left_page - display_page_count / 2 + (1 if display_page_count % 2 == 0 else 0), 1)
	next_jump = min(page.paginator.num_pages, page_range[-1] + 1 + display_page_count / 2)
	return {'pages': page_range, 'cur_page': page, 'prev_jump': prev_jump, 'next_jump': next_jump, 'url': url}
