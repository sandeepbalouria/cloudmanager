import json
import logging
import operator

from django.contrib import messages
from django.contrib.auth.models import User
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse
from django.db.models import Q
from django.forms.forms import NON_FIELD_ERRORS
from django.forms.util import ErrorList
from django.http import HttpResponse
from django.shortcuts import redirect
from django.views.generic.base import TemplateResponseMixin, View
from django.views.generic.edit import FormView

from braces.views import JSONResponseMixin
from cloudmanager.views import BaseCMView
from htools.funcs import epoch_to_datetime, datetime_to_epoch

from .controllers import Migrate, Backup, GoldenState, Clone, LockManager
from .forms import BackupTenantForm, MigrateTenantForm, CloneTenantForm, RestoreTenantForm, DeleteTBForm
from .models import Cluster, Tenant, Task, Node, Subtask, SubtaskStep, TASK_STATE_CHOICES


logger = logging.getLogger(__name__)


class TenantsView(BaseCMView, TemplateResponseMixin, View):
	"""
	Renders tenants for particular user's permissions..
	"""

	template_name = "tenant/tenants.html"
	
	def get(self, request, *args, **kwargs):
		user_groups = User.objects.get(id=self.request.user.id).groups.all()
		tenants = Tenant.objects.filter(group__in=user_groups)
		return self.render_to_response({'tenants': tenants})


class TenantRestore(BaseCMView, FormView):
	"""
	Renders tenant restore form and launches Celerey restore task
	"""

	form_class = RestoreTenantForm
	template_name = "tenant/forms/tenant_restore.html"

	def get_form_kwargs(self):
		if not self.request.is_ajax():
			return redirect('/')

		user_groups = User.objects.get(id=self.request.user.id).groups.all()
		tenant_id = self.request.GET.get('tenant_id') or self.request.POST.get('tenant_id')

		tenant = Tenant.objects.get(id=tenant_id, group__in=user_groups,)
		source_clusters = [(cl.id, cl.name) for cl in Cluster.objects.filter(group__in=user_groups, node__nodeType__in=('DN', 'SN'), state='UP').distinct()]
		kwargs = super(TenantRestore, self).get_form_kwargs()
		kwargs['initial'] = {
			'tenant_id': tenant.id,
			'source_cluster': source_clusters,
		}

		class RawError(ErrorList):
			def __unicode__(self):
				return self.raw_errors()

			def raw_errors(self):
				if not self:
					return u''
				return ''.join([e for e in self])

		kwargs['error_class'] = RawError
		return kwargs

	def form_invalid(self, form):
		errors_dict = {}
		for error in form.errors:
			e = form.errors[error]
			errors_dict[error] = unicode(e)
		return HttpResponse(
			json.dumps({'errors': errors_dict}),
			content_type='application/json',
		)

	def form_valid(self, form):
		tenant_id = int(form.cleaned_data['tenant_id'])
		tenant = Tenant.objects.get(id=tenant_id)
		target_cluster_id = int(form.cleaned_data['source_cluster'])
		backup_id = int(form.cleaned_data['export_id'])
		user_id = self.request.user.id

		backup_controller = Backup(tenant_id, target_cluster_id, user_id, form.cleaned_data['comment'])
		# checking for tenant's lock
		if backup_controller.lock_exists(
			[backup_controller.get_data_node(target_cluster_id), ],
			tenant
		):
			form._errors['source_cluster'] = form.error_class(['Tenant is locked for the maintenance.', ])
			del form.cleaned_data['source_cluster']
			return self.form_invalid(form)

		restore_res = backup_controller.restore(backup_id)
		if restore_res['exit_code'] == 0:
			messages.success(
				self.request,
				"{}: tenant's restore operation has been started.".format(tenant.name,)
			)

			context = {'result': 'ok'}
			return HttpResponse(
				json.dumps(context),
				content_type='application/json',
			)
		else:
			form._errors[NON_FIELD_ERRORS] = form.error_class([restore_res['message'], ])
			return self.form_invalid(form)


class TenantDownloadGoldenState(BaseCMView, FormView):
	"""
	Renders tenant download GS form and launches Celery task
	"""

	form_class = RestoreTenantForm
	template_name = "tenant/forms/tenant_restore.html"

	def render_to_response(self, context, **response_kwargs):
		context['restore_type'] = 'golden_state'
		return super(TenantDownloadGoldenState, self).render_to_response(context, **response_kwargs)

	def get_form_kwargs(self):
		if not self.request.is_ajax():
			return redirect('/')
		
		user_groups = User.objects.get(id=self.request.user.id).groups.all()
		tenant_id = self.request.GET.get('tenant_id') or self.request.POST.get('tenant_id')

		tenant = Tenant.objects.get(id=tenant_id, group__in=user_groups,)

		source_clusters = [(cl.id, cl.name) for cl in Cluster.objects.filter(group__in=user_groups, node__nodeType__in=('DN', 'SN'), state='UP').distinct()]
		kwargs = super(TenantDownloadGoldenState, self).get_form_kwargs()
		kwargs['initial'] = {'tenant_id': tenant.id, 'source_cluster': source_clusters}

		class RawError(ErrorList):
			def __unicode__(self):
				return self.raw_errors()

			def raw_errors(self):
				if not self:
					return u''
				return ''.join([e for e in self])

		kwargs['error_class'] = RawError
		return kwargs

	def form_invalid(self, form):
		errors_dict = {}
		for error in form.errors:
			e = form.errors[error]
			errors_dict[error] = unicode(e)
		return HttpResponse(
			json.dumps({'errors': errors_dict}),
			content_type='application/json',
		)

	def form_valid(self, form):
		tenant_id = int(form.cleaned_data['tenant_id'])
		tenant = Tenant.objects.get(id=tenant_id)
		target_cluster_id = int(form.cleaned_data['source_cluster'])
		state_id = int(form.cleaned_data['export_id'])
		user_id = self.request.user.id

		golden_state_controller = GoldenState(
			tenant_id=tenant_id,
			source_cluster_id=target_cluster_id,
			user_id=user_id,
			comment=form.cleaned_data['comment'],
		)
		# checking for tenant's lock
		if golden_state_controller.lock_exists(
			[golden_state_controller.get_data_node(target_cluster_id), ],
			tenant
		):
			form._errors['source_cluster'] = form.error_class(['Tenant is locked for the maintenance.', ])
			del form.cleaned_data['source_cluster']
			return self.form_invalid(form)

		restore_res = golden_state_controller.restore(state_id)
		if restore_res['exit_code'] == 0:
			messages.success(
				self.request,
				"{}: tenant's restore operation has been started.".format(tenant.name,)
			)

			context = {'result': 'ok'}
			return HttpResponse(
				json.dumps(context),
				content_type='application/json',
			)
		else:
			form._errors[NON_FIELD_ERRORS] = form.error_class([restore_res['message'], ])
			return self.form_invalid(form)


class TenantClone(BaseCMView, FormView):
	"""
	Renders the clone form and launches the clone Celery tasks.
	"""

	form_class = CloneTenantForm
	template_name = "tenant/forms/tenant_cloning.html"

	def get_form_kwargs(self):
		if not self.request.is_ajax():
			return redirect('/')

		user_groups = User.objects.get(id=self.request.user.id).groups.all()
		tenant_id = self.request.GET.get('tenant_id') or self.request.POST.get('tenant_id')

		tenant = Tenant.objects.get(id=tenant_id, group__in=user_groups,)
		source_clusters = [(t.id, t.name) for t in Cluster.objects.filter(tenant=tenant, group__in=user_groups, node__nodeType__in=('DN', 'SN'), state='UP').distinct()]
		dest_clusters = [(t.id, t.name) for t in Cluster.objects.filter(group__in=user_groups, node__nodeType__in=('DN', 'SN'), state='UP').distinct()]

		kwargs = super(TenantClone, self).get_form_kwargs()
		kwargs['initial'] = {'tenant_id': tenant.id, 'source_cluster': source_clusters, 'destination_cluster': dest_clusters}

		class RawError(ErrorList):
			def __unicode__(self):
				return self.raw_errors()

			def raw_errors(self):
				if not self:
					return u''
				return ''.join([e for e in self])

		kwargs['error_class'] = RawError

		return kwargs

	def form_invalid(self, form):

		errors_dict = {}
		for error in form.errors:
			e = form.errors[error]
			errors_dict[error] = unicode(e)

		return HttpResponse(
			json.dumps({'errors': errors_dict}),
			content_type='application/json',
		)

	def form_valid(self, form):
		# Source's & destination's cluster rights are checked by initial parameters (see get_form_kwargs)
		user_obj = User.objects.get(id=self.request.user.id)
		user_groups = user_obj.groups.all()
		# getting Clusters for the checks
		clusters = Cluster.objects.filter(
			group__in=user_groups,
			tenant__id=int(form.cleaned_data['tenant_id']),
			tenant__group__in=user_groups,
			node__nodeType__in=('DN', 'SN')
		).distinct()
		tenant_clusters = [cl.id for cl in clusters]
		# checking for a proper link between tenant and source cloud
		if int(form.cleaned_data['source_cluster']) not in tenant_clusters:
			tenant_obj = Tenant.objects.get(id=int(form.cleaned_data['tenant_id']))
			# changing the source cloud choice list for the particular tenant:
			clusters = [("", "")] + [(cl.id, cl.name) for cl in clusters]
			form.fields['source_cluster'].choices = clusters
			# adding error message for the field and deleting field's cleaned data
			form._errors['source_cluster'] = form.error_class(['Wrong source cluster for %s tenant' % (tenant_obj.name,), ])
			del form.cleaned_data['source_cluster']
			return self.form_invalid(form)

		tenant = Tenant.objects.get(id=int(form.cleaned_data['tenant_id']))
		source_cluster = Cluster.objects.get(id=int(form.cleaned_data['source_cluster']))

		cloner = Clone(tenant.id, source_cluster.id, user_obj.id)
		# cheking for tenant's locks
		if cloner.lock_exists([
			cloner.get_data_node(source_cluster.id),
		]):
			form._errors['source_cluster'] = form.error_class(['Tenant is locked for the maintenance.', ])
			del form.cleaned_data['source_cluster']
			return self.form_invalid(form)

		res = cloner.clone(
			new_tenant_tmid=form.cleaned_data['new_tenant_id'],
			new_tenant_name=form.cleaned_data['new_tenant_name'],
			destination_cluster_id=form.cleaned_data['destination_cluster']
		)

		if res['exit_code'] == 0:
			messages.success(
				self.request,
				'Cloning process for {} tenant has been started'.format(tenant.name)
			)
			context = {'result': 'ok'}
			return HttpResponse(
				json.dumps(context),
				content_type='application/json',
			)
		else:
			form._errors[NON_FIELD_ERRORS] = form.error_class([res['message'], ])
			return self.form_invalid(form)


class TenantBackup(BaseCMView, FormView):
	"""
	Renders backup form and launches backup Celery tasks.
	"""

	form_class = BackupTenantForm
	template_name = "tenant/forms/tenant_backup.html"

	def get_form_kwargs(self):
		if not self.request.is_ajax():
			return redirect('/')
		user_groups = User.objects.get(id=self.request.user.id).groups.all()
		tenant_id = self.request.GET.get('tenant_id') or self.request.POST.get('tenant_id')

		tenant = Tenant.objects.get(id=tenant_id, group__in=user_groups,)
		source_clusters = [(cl.id, cl.name) for cl in Cluster.objects.filter(tenant=tenant, group__in=user_groups, node__nodeType__in=('DN', 'SN'), state='UP').distinct()]

		kwargs = super(TenantBackup, self).get_form_kwargs()
		kwargs['initial'] = {'tenant_id': tenant.id, 'source_cluster': source_clusters}

		class RawError(ErrorList):
			def __unicode__(self):
				return self.raw_errors()

			def raw_errors(self):
				if not self:
					return u''
				return ''.join([e for e in self])

		kwargs['error_class'] = RawError
		return kwargs

	def form_invalid(self, form):
		errors_dict = {}
		for error in form.errors:
			e = form.errors[error]
			errors_dict[error] = unicode(e)
		logger.error(errors_dict)
		return HttpResponse(
			json.dumps({'errors': errors_dict}),
			content_type='application/json',
		)

	def form_valid(self, form):
		tenant_id = int(form.cleaned_data['tenant_id'])
		source_cluster_id = int(form.cleaned_data['source_cluster'])
		user_id = self.request.user.id

		user_obj = User.objects.get(id=user_id)
		user_groups = user_obj.groups.all()
		# getting Clusters for the checks
		clusters = Cluster.objects.filter(
			group__in=user_groups,
			tenant__id=tenant_id,
			tenant__group__in=user_groups,
			node__nodeType__in=('DN', 'SN'),
			state='UP'
		).distinct()
		tenant_clusters = [cl.id for cl in clusters]
		tenant = Tenant.objects.get(id=tenant_id)
		# checking for a proper link between tenant and source cloud
		if source_cluster_id not in tenant_clusters:
			# changing the source cloud choice list for the particular tenant:
			clusters = [(cl.id, cl.name) for cl in clusters]
			form.fields['source_cluster'].choices = clusters
			# adding error message for the field and deleting field's cleaned data
			form._errors['source_cluster'] = form.error_class(['Wrong source cluster for %s tenant' % (tenant.name,), ])
			del form.cleaned_data['source_cluster']
			return self.form_invalid(form)

		backup_controller = Backup(tenant_id, source_cluster_id, user_id, form.cleaned_data['comment'])
		# checking for tenant's lock
		if backup_controller.lock_exists(
			[backup_controller.get_data_node(source_cluster_id), ],
			tenant
		):
			form._errors['source_cluster'] = form.error_class(['Tenant is locked for the maintenance.', ])
			del form.cleaned_data['source_cluster']
			return self.form_invalid(form)

		backup_controller.make()
		messages.success(
			self.request,
			"{}: tenant's backup operation has been started.".format(tenant.name,)
		)

		context = {'result': 'ok'}
		return HttpResponse(
			json.dumps(context),
			content_type='application/json',
		)


class TenantSaveGoldenState(BaseCMView, FormView):
	"""
	Renders saving golden state form and launches Celery task.
	"""

	form_class = BackupTenantForm
	template_name = "tenant/forms/tenant_backup.html"

	def render_to_response(self, context, **response_kwargs):
		context['backup_type'] = 'golden_state'
		return super(TenantSaveGoldenState, self).render_to_response(context, **response_kwargs)

	def get_form_kwargs(self):
		if not self.request.is_ajax():
			return redirect('/')

		user_groups = User.objects.get(id=self.request.user.id).groups.all()
		tenant_id = self.request.GET.get('tenant_id') or self.request.POST.get('tenant_id')

		tenant = Tenant.objects.get(id=tenant_id, group__in=user_groups,)
		source_clusters = [(cl.id, cl.name) for cl in Cluster.objects.filter(tenant=tenant, group__in=user_groups, node__nodeType__in=('DN', 'SN'), state='UP').distinct()]

		kwargs = super(TenantSaveGoldenState, self).get_form_kwargs()
		kwargs['initial'] = {'tenant_id': tenant.id, 'source_cluster': source_clusters}

		class RawError(ErrorList):
			def __unicode__(self):
				return self.raw_errors()

			def raw_errors(self):
				if not self:
					return u''
				return ''.join([e for e in self])

		kwargs['error_class'] = RawError
		return kwargs

	def form_invalid(self, form):
		errors_dict = {}
		for error in form.errors:
			e = form.errors[error]
			errors_dict[error] = unicode(e)
		logger.error(errors_dict)
		return HttpResponse(
			json.dumps({'errors': errors_dict}),
			content_type='application/json',
		)

	def form_valid(self, form):
		tenant_id = int(form.cleaned_data['tenant_id'])
		source_cluster_id = int(form.cleaned_data['source_cluster'])
		user_id = self.request.user.id

		user_obj = User.objects.get(id=user_id)
		user_groups = user_obj.groups.all()
		# getting Clusters for the checks
		clusters = Cluster.objects.filter(
			group__in=user_groups,
			tenant__id=tenant_id,
			tenant__group__in=user_groups,
			node__nodeType__in=('DN', 'SN'),
			state='UP'
		).distinct()
		tenant_clusters = [cl.id for cl in clusters]
		tenant = Tenant.objects.get(id=tenant_id)
		# checking for a proper link between tenant and source cloud
		if source_cluster_id not in tenant_clusters:
			# changing the source cloud choice list for the particular tenant:
			clusters = [(cl.id, cl.name) for cl in clusters]
			form.fields['source_cluster'].choices = clusters
			# adding error message for the field and deleting field's cleaned data
			form._errors['source_cluster'] = form.error_class(['Wrong source cluster for %s tenant' % (tenant.name,), ])
			del form.cleaned_data['source_cluster']
			return self.form_invalid(form)

		golden_state_controller = GoldenState(
			tenant_id=tenant_id,
			source_cluster_id=source_cluster_id,
			user_id=user_id,
			comment=form.cleaned_data['comment'],
		)
		# checking for tenant's lock
		if golden_state_controller.lock_exists(
			[golden_state_controller.get_data_node(source_cluster_id), ],
			tenant
		):
			form._errors['source_cluster'] = form.error_class(['Tenant is locked for the maintenance.', ])
			del form.cleaned_data['source_cluster']
			return self.form_invalid(form)

		golden_state_controller.save()
		messages.success(
			self.request,
			"{}: saving tenant's golden state...".format(tenant.name,)
		)

		context = {'result': 'ok'}
		return HttpResponse(
			json.dumps(context),
			content_type='application/json',
		)


class TenantMigration(BaseCMView, FormView):
	"""
	Renders migration form and launches migration Celery tasks.
	"""

	form_class = MigrateTenantForm
	template_name = "tenant/forms/tenant_migration.html"

	def get_form_kwargs(self):
		if not self.request.is_ajax():
			return redirect('/')
		user_groups = User.objects.get(id=self.request.user.id).groups.all()
		tenant_id = self.request.GET.get('tenant_id') or self.request.POST.get('tenant_id')

		tenant = Tenant.objects.get(id=tenant_id, group__in=user_groups,)
		source_clusters = [(cl.id, cl.name) for cl in Cluster.objects.filter(tenant=tenant, group__in=user_groups, node__nodeType__in=('DN', 'SN'), state='UP').distinct()]

		dest_clusters = [(cl.id, cl.name) for cl in Cluster.objects.filter(group__in=user_groups, node__nodeType__in=('DN', 'SN'), state='UP').distinct()]

		kwargs = super(TenantMigration, self).get_form_kwargs()
		kwargs['initial'] = {'tenant_id': tenant.id, 'source_cluster': source_clusters, 'destination_cluster': dest_clusters}

		class RawError(ErrorList):
			def __unicode__(self):
				return self.raw_errors()

			def raw_errors(self):
				if not self:
					return u''
				return ''.join([e for e in self])

		kwargs['error_class'] = RawError
		return kwargs

	def form_invalid(self, form):

		errors_dict = {}
		for error in form.errors:
			e = form.errors[error]
			errors_dict[error] = unicode(e)
		logger.error(errors_dict)
		return HttpResponse(
			json.dumps({'errors': errors_dict}),
			content_type='application/json',
		)

	def form_valid(self, form):
		# we don't have to check here the correctness of tenant, source and destination
		# cluster ids because it is checked automatically by form object
		# as we set the initial data for particular user with allowed data only

		tenant_id = int(form.cleaned_data['tenant_id'])
		source_cluster_id = int(form.cleaned_data['source_cluster'])
		destination_cluster_id = int(form.cleaned_data['destination_cluster'])
		user_id = self.request.user.id

		user_obj = User.objects.get(id=user_id)
		user_groups = user_obj.groups.all()
		# getting Clusters for the checks
		clusters = Cluster.objects.filter(
			group__in=user_groups,
			tenant__id=tenant_id,
			tenant__group__in=user_groups,
			node__nodeType__in=('DN', 'SN'),
			state='UP'
		).distinct()
		tenant_clusters = [cl.id for cl in clusters]
		tenant = Tenant.objects.get(id=tenant_id)
		# checking for a proper link between tenant and source cloud
		if source_cluster_id not in tenant_clusters:
			# changing the source cloud choice list for the particular tenant:
			clusters = [(cl.id, cl.name) for cl in clusters]
			form.fields['source_cluster'].choices = clusters
			# adding error message for the field and deleting field's cleaned data
			form._errors['source_cluster'] = form.error_class(['Wrong source cluster for %s tenant' % (tenant.name,), ])
			del form.cleaned_data['source_cluster']
			return self.form_invalid(form)

		migration_controller = Migrate(tenant_id, source_cluster_id, destination_cluster_id, user_id)
		# checking for tenant's lock
		if migration_controller.lock_exists(
			[migration_controller.get_data_node(source_cluster_id), ],
			tenant
		):
			form._errors['source_cluster'] = form.error_class(['Tenant is locked for the maintenance.', ])
			del form.cleaned_data['source_cluster']
			return self.form_invalid(form)

		if migration_controller.lock_exists(
			[migration_controller.get_data_node(destination_cluster_id), ],
			tenant
		):
			form._errors['destination_cluster'] = form.error_class(['Tenant is locked for the maintenance.', ])
			del form.cleaned_data['destination_cluster']
			return self.form_invalid(form)

		migration_type = int(form.cleaned_data['migration_type'])
		if migration_type == 1:
			restore_res = migration_controller.run_tenant()
			if restore_res['exit_code'] == 0:
				messages.success(
					self.request,
					'{}: full tenant migration has been started.'.format(tenant.name,)
				)
				context = {'result': 'ok'}
				return HttpResponse(
					json.dumps(context),
					content_type='application/json',
				)
			else:
				form._errors[NON_FIELD_ERRORS] = form.error_class([restore_res['message'], ])
				return self.form_invalid(form)

		elif migration_type == 2:
			restore_res = migration_controller.run_principals()
			if restore_res['exit_code'] == 0:
				messages.success(
					self.request,
					'{}: principals tenant migration has been started.'.format(tenant.name,)
				)
				context = {'result': 'ok'}
				return HttpResponse(
					json.dumps(context),
					content_type='application/json',
				)
			else:
				form._errors[NON_FIELD_ERRORS] = form.error_class([restore_res['message'], ])
				return self.form_invalid(form)

		else:
			form._errors['migration_type'] = form.error_class(['Wrong migration operation type (%s)' % (migration_type,), ])
			del form.cleaned_data['migration_type']
			return self.form_invalid(form)


class TenantDeleteGS(BaseCMView, FormView):
	"""
	Deletes information about particular tenant's golden state.
	Does not delete actual golden state from AWS S3.
	"""

	form_class = DeleteTBForm
	template_name = "tenant/forms/tenant_delete_gs.html"

	def get_form_kwargs(self):
		if not self.request.is_ajax():
			return redirect('/')
		user_groups = User.objects.get(id=self.request.user.id).groups.all()
		tenant_id = self.request.GET.get('tenant_id') or self.request.POST.get('tenant_id')
		export_id = self.request.GET.get('export_id') or self.request.POST.get('export_id')

		tenant = Tenant.objects.get(id=tenant_id, group__in=user_groups,)

		kwargs = super(TenantDeleteGS, self).get_form_kwargs()
		kwargs['initial'] = {'tenant_id': tenant.id, 'export_id': export_id}

		class RawError(ErrorList):
			def __unicode__(self):
				return self.raw_errors()

			def raw_errors(self):
				if not self:
					return u''
				return ''.join([e for e in self])

		kwargs['error_class'] = RawError
		return kwargs

	def render_to_response(self, context, **response_kwargs):
		# adding additional info about golden state
		# into template
		export_id = int(context['form'].initial['export_id'])
		tenant_id = int(context['form'].initial['tenant_id'])
		gs_controller = GoldenState(
			tenant_id,
		)
		states = gs_controller.list()
		state = None
		for item in states:
			if item['export_id'] == export_id and item['tenant_id'] == tenant_id:
				state = item
				break

		if state:
			context['state_info'] = state

		return super(TenantDeleteGS, self).render_to_response(context, **response_kwargs)

	def form_invalid(self, form):
		errors_dict = {}
		for error in form.errors:
			e = form.errors[error]
			errors_dict[error] = unicode(e)
		return HttpResponse(
			json.dumps({'errors': errors_dict}),
			content_type='application/json',
		)

	def form_valid(self, form):
		if not self.request.is_ajax():
			return redirect('/')
		# we don't have to check here the correctness of tenant, source and destination
		# cluster ids because it is checked automatically by form object
		# as we set the initial data for particular user with allowed data only

		tenant_id = int(form.cleaned_data['tenant_id'])
		export_id = int(form.cleaned_data['export_id'])

		gs_controller = GoldenState(
			tenant_id,
		)
		res = gs_controller.delete(export_id)
		if res['exit_code'] == 0:
			messages.success(
				self.request,
				'Golden state has been deleted'
			)
			context = {'result': 'ok'}
			return HttpResponse(
				json.dumps(context),
				content_type='application/json',
			)
		else:
			form._errors[NON_FIELD_ERRORS] = form.error_class([res['message'], ])
			return self.form_invalid(form)


class ClustersList (BaseCMView, JSONResponseMixin, View):
	"""
	Returns JSON cluster's list (via AJAX) available for specific Tenant and current user's permissions.
	"""

	def get(self, request, *args, **kwargs):
		if not request.is_ajax():
			return redirect('/')

		tenant_id = request.GET.get('tenant_id')

		if tenant_id:
			user_obj = User.objects.get(id=request.user.id)
			user_groups = user_obj.groups.all()

			clusters = Cluster.objects.filter(
				group__in=user_groups,
				tenant__id=tenant_id,
				tenant__group__in=user_groups,
				node__nodeType__in=('DN', 'SN'),
				state='UP'
			).distinct()

			return self.render_to_response(
				[
					{
						'id': cl.id,
						'tmid': cl.tmid,
						'name': cl.name,
						'type': cl.type,
						'state': cl.state,
						'version': cl.attribute['cluster_version']
					} for cl in clusters
				]
			)

		else:
			return self.render_to_response('')


class DataTenantStatus(BaseCMView, JSONResponseMixin, View):
	"""
	Returns tenant's status on particular cluster's data node via AJAX.
	"""

	def get(self, request, *args, **kwargs):
		if not request.is_ajax():
			return redirect('/')

		response = {'tenant_status': 'Unknown'}
		tenant_id = request.GET.get('tenant_id')
		cluster_id = request.GET.get('cluster_id')
		cluster = Cluster.objects.get(id=int(cluster_id))
		if tenant_id and cluster_id:
			user_obj = User.objects.get(id=request.user.id)
			user_groups = user_obj.groups.all()
			try:
				node = Node.objects.get(
					nodeType='DN' if cluster.type == 'MN' else 'SN',
					cluster=cluster,
					cluster__group__in=user_groups,
				)
				tenant = Tenant.objects.get(
					id=tenant_id,
					group__in=user_groups
				)
				if LockManager.lock_exists(node, tenant):
					response['tenant_status'] = 'Locked'
				else:
					response['tenant_status'] = 'Free'

			except Node.DoesNotExist:
				logger.error("Data node not found for cluster_id = {} while getting tenant's status information. User_id  = {} (from the request)".format(cluster_id, request.user.id))
			except Tenant.DoesNotExist:
				logger.error("Tenant (tenant_id = {}) not found while getting tenant's status information for user_id  = {} (from the request)".format(tenant_id, request.user.id))

			return self.render_to_response(response)
		else:
			return self.render_to_response(response)


class DataBaseTaskInfo (object):
	'''
	Base class for getting information about tasks' status.
	'''

	def get_tasks_set(self, tenant_id, cluster_id, in_progress_only=False):
		'''
		Returns information about tasks.

		:param tenant_id: tenant id
		:param cluster_id: cluster id
		:param in_progress_only: flag to return an information only about the tasks that "in progress"
		'''

		tasks = []

		if tenant_id and cluster_id:
			cluster = Cluster.objects.get(id=cluster_id)
			user_obj = User.objects.get(id=self.request.user.id)
			user_groups = user_obj.groups.all()
			try:
				node = Node.objects.get(
					nodeType='DN' if cluster.type == 'MN' else 'SN',
					cluster=cluster,
					cluster__group__in=user_groups,
				)
				tenant = Tenant.objects.get(
					id=tenant_id,
					group__in=user_groups
				)

				filter_list = [Q(subtask__tenant=tenant), Q(subtask__node=node)]
				if in_progress_only:
					# getting "in progress" or has ended 5 minutes ago tasks only
					filter_list.append((Q(end_dt=None) | Q(end_dt__gte=datetime_to_epoch(shift=60 * 5, make_mix=False))))

				if Task.objects.filter(reduce(operator.and_, filter_list)).exists():
					tasks = Task.objects.filter(reduce(operator.and_, filter_list)).all()
#
			except Node.DoesNotExist:
				logger.error("Data node not found for cluster_id = {} while getting tenant's status information. User_id  = {} (from the request)".format(cluster_id, self.request.user.id))
			except Tenant.DoesNotExist:
				logger.error("Tenant (tenant_id = {}) not found while getting tenant's status information for user_id  = {} (from the request)".format(tenant_id, self.request.user.id))

		return tasks

	def get_json_tasks_info(self, tenant_id, cluster_id, in_progress_only=False):
		'''
		Returns an information about tasks in JSON format.

		:param tenant_id: tenant id
		:param cluster_id: cluster id
		:param in_progress_only: flag to return an information only about the tasks that "in progress"
		'''
		class _Task (object):
			def __init__(self, id, status, description, owner, start_time, end_time):
				self.id = id
				self.status = status
				self.description = description
				self.owner = owner
				self.start_time = start_time
				self.end_time = end_time
				self.subtasks = []

			def add_subtask(self, subtask):
				self.subtasks.append(subtask)

		class _Subtask (object):
			def __init__(self, jid, status, description, start_time, end_time):
				self.jid = jid
				self.status = status
				self.description = description
				self.start_time = start_time
				self.end_time = end_time

		task_container = []
		tasks = self.get_tasks_set(tenant_id, cluster_id, in_progress_only)

		for tenant_task in tasks:
			task_statuses = dict(TASK_STATE_CHOICES)
			subtask_statuses = dict(SubtaskStep.STEP_STATUS_CHOICES)
			task = _Task(
				id=tenant_task.id,
				status=task_statuses[tenant_task.status],
				description=tenant_task.description,
				owner=User.objects.get(id=tenant_task.user_id).username,
				start_time=epoch_to_datetime(tenant_task.start_dt, dt_zone='America/Los_Angeles').strftime('%m/%d/%y %H:%M:%S'),
				end_time=epoch_to_datetime(tenant_task.end_dt, dt_zone='America/Los_Angeles').strftime('%m/%d/%y %H:%M:%S') if tenant_task.end_dt else ''
			)
			for tenant_subtask in tenant_task.subtask_set.all():
				subtask = _Subtask(
					jid=tenant_subtask.job_id,
					status=subtask_statuses[tenant_subtask.status],
					description=tenant_subtask.description,
					start_time=epoch_to_datetime(tenant_subtask.start_dt, dt_zone='America/Los_Angeles').strftime('%m/%d/%y %H:%M:%S'),
					end_time=epoch_to_datetime(tenant_subtask.end_dt, dt_zone='America/Los_Angeles').strftime('%m/%d/%y %H:%M:%S') if tenant_subtask.end_dt else ''
				)
				task.add_subtask(subtask)

			task_container.append(task)

		json_tasks = [
			{
				'id': task.id,
				'status': task.status,
				'start_time': task.start_time,
				'end_time': task.end_time if task.end_time else '',
				'subtasks': [
					{
						'jid': st.jid,
						'status': st.status,
						'start_time': st.start_time,
						'end_time': st.end_time if st.end_time else '',
					} for st in task.subtasks
				]
			} for task in task_container
		]
		return json_tasks


class DataTenantTasksInfo(BaseCMView, TemplateResponseMixin, View, DataBaseTaskInfo):
	"""
	Returns information about tenant's tasks via AJAX.
	"""
	template_name = "tenant/tenant_task_info.html"

	def get(self, request, *args, **kwargs):

		if not request.is_ajax():
			return redirect('/')

		tenant_id = request.GET.get('tenant_id')
		cluster_id = request.GET.get('cluster_id')
		page = request.GET.get('page', 1)
		tasks = self.get_tasks_set(tenant_id, cluster_id)
		# Show 10 tasks per page:
		paginator = Paginator(tasks, 10)
		try:
			tasks = paginator.page(page)
		except PageNotAnInteger:
			# If page is not an integer, deliver first page.
			tasks = paginator.page(1)
		except EmptyPage:
			# If page is out of range (e.g. 9999), deliver last page of results.
			tasks = paginator.page(paginator.num_pages)

		return self.render_to_response({'tasks': tasks, 'tenant_id': tenant_id, 'cluster_id': cluster_id})


class DataTenantBackups(BaseCMView, TemplateResponseMixin, View):
	"""
	Returns information obout tenant's backups via AJAX.
	"""

	template_name = "tenant/tenant_backup_info.html"

	def get(self, request, *args, **kwargs):

		if not request.is_ajax():
			return redirect('/')

		tenant_id = request.GET.get('tenant_id')
		cluster_id = request.GET.get('cluster_id')
		page = request.GET.get('page', 1)

		backup_controller = Backup(tenant_id, cluster_id, self.request.user.id)
		backup_meta = backup_controller.list()

		paginator = Paginator(backup_meta, 10)
		backup_meta = paginator.page(page)

		return self.render_to_response({'backups': backup_meta, 'tenant_id': tenant_id, 'cluster_id': cluster_id})


class DataTenantGS(BaseCMView, JSONResponseMixin, View):
	"""
	Returns information about all tenant's golden states via AJAX.
	"""

	def get(self, request, *args, **kwargs):
		if not request.is_ajax():
			return redirect('/')

		tenant_id = request.GET.get('tenant_id')

		gs_controller = GoldenState(
			tenant_id,
		)
		gs_meta = gs_controller.list()
		data = []
		download_url = unicode(reverse('tenant_download_gs'))
		delete_url = unicode(reverse('tenant_delete_gs'))
		for state in gs_meta:
			# adding user name to response
			state['user_name'] = User.objects.get(id=state['user_id']).username
			data.append({
				'attributes': state,
				'actions': {
					'download': {'url': download_url, },
					'delete': {'url': delete_url, }
				}

			})
		json_response = {'data': data}
		return self.render_to_response(json_response)


class DataUpdateTask(BaseCMView, JSONResponseMixin, View, DataBaseTaskInfo):
	'''
	Returns updated information about the tasks statuses via AJAX.
	Returns information about the tasks that are "in progress".
	'''

	def get(self, request, *args, **kwargs):
		if not request.is_ajax():
			return redirect('/')

		tenant_id = request.GET.get('tenant_id')
		cluster_id = request.GET.get('cluster_id')
		# updating only tasks that are "in progress"
		tasks = self.get_json_tasks_info(tenant_id, cluster_id, in_progress_only=True)

		return self.render_to_response({'tasks': tasks})


class SubtaskDetails(BaseCMView, TemplateResponseMixin, View):
	'''
	Returns information about subtask's (Celery task) steps via AJAX.
	'''

	template_name = "tenant/modals/subtask_details.html"

	def get(self, request, *args, **kwargs):
		if not request.is_ajax():
			return redirect('/')

		celery_jid = request.GET.get('jid')
		subtask = None
		if celery_jid:
			try:
				subtask = Subtask.objects.get(job_id=celery_jid)
			except Subtask.DoesNotExist:
				logger.error("Subtask not found for clelery_jid = {} while getting subtask's steps information. User_id  = {} (from the request)".format(celery_jid, self.request.user.id))
		return self.render_to_response({'subtask': subtask})


class TenantDashboard(BaseCMView, TemplateResponseMixin, View):
	'''
	Renders tenant's dashboard with all information about tenant.
	'''

	template_name = "tenant/tenant_dashboard.html"

	def get(self, request, *args, **kwargs):
		tenant_id = kwargs['tenant_id']

		user_obj = User.objects.get(id=self.request.user.id)
		user_groups = user_obj.groups.all()
		tenant = None
		clusters = None

		try:
			tenant = Tenant.objects.get(
				id=tenant_id,
				group__in=user_groups
			)
		except Tenant.DoesNotExist:
				logger.error("Tenant (tenant_id = {}) not found while getting tenant's status information for user_id  = {} (from the request)".format(tenant_id, self.request.user.id))

		try:
			clusters = Cluster.objects.filter(
				group__in=user_groups,
				tenant__id=tenant_id,
				tenant__group__in=user_groups,
				node__nodeType__in=('DN', 'SN'),
				state='UP'
			).distinct()
		except Tenant.DoesNotExist:
				logger.error("Cluster(s) not found for tenant_id = {} and user_id  = {} (from the request)".format(tenant_id, self.request.user.id))

		return self.render_to_response({'tenant': tenant, 'clusters': clusters})
