from django.contrib.auth.models import User, Group
from django.db import models
from django.utils.translation import ugettext_lazy as _
import jsonfield

from cloudmanager.models import Cluster, Node


STARTED = 'ST'
SUCCEEDED = 'SU'
FAILED = 'FA'
CANCELED = 'CA'
TASK_STATE_CHOICES = (
	(STARTED, 'Started'),
	(SUCCEEDED, 'Succeeded'),
	(FAILED, 'Failed'),
	(CANCELED, 'Canceled'),
)


class Tenant(models.Model):
	"""
	A single tenant.
	"""

	tmid = models.IntegerField(_("tenant id"))
	name = models.CharField(_("name"), max_length=50)
	group = models.ManyToManyField(Group)
	cluster = models.ManyToManyField(Cluster)
	attribute = jsonfield.JSONField()

	def __unicode__(self):
		return self.name

	class Meta:
		ordering = ['name']
		verbose_name = _("tenant")
		verbose_name_plural = _("tenants")
		db_table = "cloudmanager_tenant"


class Task(models.Model):
	"""
	A tenant task.
	Task - Django view task.
	"""
	description = models.TextField(null=True)
	user_id = models.IntegerField(_("user_id"))
	start_dt = models.BigIntegerField()
	end_dt = models.BigIntegerField(null=True)
	status = models.CharField(
		max_length=2,
		choices=TASK_STATE_CHOICES,
		default=STARTED
	)

	class Meta:
		ordering = ['-start_dt', '-id']
		verbose_name = _("tenant_task")
		verbose_name_plural = _("tenant_tasks")
		db_table = "cloudmanager_task"


class Subtask(models.Model):
	"""
	A tenant subtask.
	Subtask is a Celery task. We can have multiple subtasks inside one task - DjangoView.
	"""
	description = models.TextField(null=True)
	start_dt = models.BigIntegerField(null=True)
	end_dt = models.BigIntegerField(null=True)
	job_id = models.CharField(_("job_id"), max_length=50, null=True)
	status = models.CharField(
		max_length=2,
		choices=TASK_STATE_CHOICES,
		default=STARTED
	)
	error_message = models.TextField(null=True)
	task = models.ForeignKey(Task)
	tenant = models.ManyToManyField(Tenant)
	node = models.ManyToManyField(Node)

	class Meta:
		ordering = ['start_dt', 'id']
		verbose_name = _("tenant_subtask")
		verbose_name_plural = _("tenant_subtasks")
		db_table = "cloudmanager_subtask"


class SubtaskStep(models.Model):
	"""
	Subtask steps class: defines an information about the subtask execution process.
	Subtaskstep is a Celery internal execution step(for example: tenant_backup is a 
	substep of migration Celery task. 
	"""
	PENDING = 'PN'
	STARTED = 'ST'
	SUCCEEDED = 'SU'
	FAILED = 'FA'
	SKIPPED = 'SK'
	STEP_STATUS_CHOICES = (
		(PENDING, 'Pending'),
		(STARTED, 'Started'),
		(SUCCEEDED, 'Succeeded'),
		(FAILED, 'Failed'),
		(SKIPPED, 'Skipped'),
	)

	step_identifier = models.CharField(max_length=100, null=True)
	step_num = models.SmallIntegerField()
	start_dt = models.BigIntegerField(null=True)
	end_dt = models.BigIntegerField(null=True)
	status = models.CharField(
		max_length=2,
		choices=STEP_STATUS_CHOICES,
		default=PENDING
	)
	result = models.TextField(null=True)
	comment = models.TextField()
	subtask = models.ForeignKey(Subtask)

	class Meta:
		ordering = ['step_num']
		verbose_name = _("subtask_step")
		verbose_name_plural = _("subtask_steps")
		db_table = "cloudmanager_subtaskstep"


class TenantLock(models.Model):
	"""
	A tenant lock.
	"""
	task = models.ForeignKey(Task, null=True)

	class Meta:
		verbose_name = _("lock")
		verbose_name_plural = _("locks")
		db_table = "cloudmanager_tenantlock"
