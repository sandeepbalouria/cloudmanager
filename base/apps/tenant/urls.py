from django.conf.urls import patterns, url

from htools.decorators import staff_only

from .views import (
	TenantBackup, TenantsView, TenantMigration, DataTenantStatus, DataTenantBackups, DataTenantTasksInfo,
	DataUpdateTask, SubtaskDetails, TenantClone, TenantDashboard, TenantRestore, TenantSaveGoldenState, DataTenantGS,
	TenantDownloadGoldenState, TenantDeleteGS, ClustersList
)


urlpatterns = patterns(
	'',
	url(r'^clusters/$', staff_only(ClustersList.as_view()), name="clusters_ajax"),

	url(r'^list/$', staff_only(TenantsView.as_view()), name="cm_tenants"),
	url(r'^(?P<tenant_id>\d+)/$', staff_only(TenantDashboard.as_view()), name="tenant_dashboard"),

	url(r'^migration/$', staff_only(TenantMigration.as_view()), name="cm_migration"),
	url(r'^clone/$', staff_only(TenantClone.as_view()), name="cm_clone"),

	url(r'^status_data/$', staff_only(DataTenantStatus.as_view()), name="data_tenant_status"),
	url(r'^tasks_data/$', staff_only(DataTenantTasksInfo.as_view()), name="data_tenant_tasks"),
	url(r'^tasks_data_update/$', staff_only(DataUpdateTask.as_view()), name="data_tenant_tasks_update"),


	url(r'^modal/subtask_data/$', staff_only(SubtaskDetails.as_view()), name="modal_tenant_subtask"),
	url(r'^backup-data/$', staff_only(DataTenantBackups.as_view()), name="tenant_backups"),

	url(r'^backup/$', staff_only(TenantBackup.as_view()), name="cm_tenant_backup"),

	url(r'^restore/$', staff_only(TenantRestore.as_view()), name="cm_tenant_restore"),

	url(r'^golden_state/save/$', staff_only(TenantSaveGoldenState.as_view()), name="tenant_save_gs"),
	url(r'^golden_state/download/$', staff_only(TenantDownloadGoldenState.as_view()), name="tenant_download_gs"),
	url(r'^golden_state/list/$', staff_only(DataTenantGS.as_view()), name="tenant_list_gs"),
	url(r'^golden_state/delete/$', staff_only(TenantDeleteGS.as_view()), name="tenant_delete_gs"),

)
