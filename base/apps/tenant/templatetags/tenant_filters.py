import logging

from django import template

from ..models import TASK_STATE_CHOICES


logger = logging.getLogger(__name__)

register = template.Library()


@register.filter
def task_status(short_status):
	return dict(TASK_STATE_CHOICES)[short_status]
