from __future__ import absolute_import

import itertools
import logging

from celery import Task
from django.conf import settings
import boto3

from base.celery import app
from dynamizer import dict_to_dynamo
from htools.funcs import datetime_to_epoch


_CLOUD_CLIENT = None

logger = logging.getLogger(__name__)


def use_sqlite(position=1):
	'''
	Adds SQLite DB connection as a parameter to a given position.
	:param position: 0-based position number in args
	'''

	def wrapped(fun):
		def wrapper(*args, **kwargs):
			import sqlite3
			# FIXME: remove hardcoded connection parameters
			conn = sqlite3.connect('../cloudmanager_dev.db')
			l_args = list(args)
			l_args.insert(position, conn)
			try:
				res = fun(*l_args, **kwargs)
				conn.commit()
				conn.close()
			except Exception as e:
				logger.exception(e)
				conn.close()
				raise

			return res
		return wrapper

	return wrapped


def use_pgsql(position=1):
	'''
	Adds Postgres DB connection as a parameter to a given position.
	:param position:0-based position number in args
	'''

	def wrapped(fun):
		def wrapper(*args, **kwargs):
			import psycopg2
			# FIXME: remove hardcoded connection parameters
			conn = psycopg2.connect(
				host=settings.DATABASES['default']['HOST'],
				port=settings.DATABASES['default']['PORT'],
				database=settings.DATABASES['default']['NAME'],
				user=settings.DATABASES['default']['USER'],
				password=settings.DATABASES['default']['PASSWORD'],
			)
			l_args = list(args)
			l_args.insert(position, conn)
			try:
				res = fun(*l_args, **kwargs)
				conn.commit()
				conn.close()
			except Exception as e:
				logger.exception(e)
				# conn.rollback ()
				conn.close()
				raise

			return res
		return wrapper

	return wrapped


class AbstractTask(Task):
	'''
	Base class for Celery tasks.
	Provides base methods to work with Salt tasks.
	Keep in mind that Celery task is a _subtask_ for Django task.
	'''
	abstract = True

	def __init__(self, *args, **kwargs):
		super(AbstractTask, self).__init__(*args, **kwargs)
		# SALT jobs that are in_process
		# list of dicts {job_id, minion_name}
		self.jobs_in_process = []

	@use_pgsql()
	def publish_task(self, conn, task_descr):
		'''
		Creates Celery Task object.

		:param conn: DB connection, sets by decorator
		:param task_descr: task's description, format:
			{
				'subtask_id': subtask_id,
				'sub_task_description': 'subtask's description',
				'sub_task_steps': [
					"step(1) short description",
					"step(n-1) short description",
					"step(n) short description",
					"step(n+1) short description",
				],
			}
		'''
		if not isinstance(task_descr, dict):
			raise Exception('Bad task description')
		elif 'subtask_id' not in task_descr:
			raise Exception("SUBTASK_ID key not found inside the task's description")
		elif 'sub_task_description' not in task_descr:
			raise Exception("SUB_TASK_DESCRIPTION key not found inside the task's description")
		elif 'sub_task_steps' not in task_descr:
			raise Exception("SUB_TASK_STEPS key not found inside the task's description")

		# FIXME: rewrite SQL with SQLAlchemy. @ALX

		# Updating the subtask record that connects Django task and current celery task (subtask)
		cur = conn.cursor()
		cur.execute(
			'''
			UPDATE cloudmanager_subtask
			SET description = %s,
				job_id = %s,
				start_dt = %s,
				status = %s
			WHERE id = %s
			''',
			(task_descr['sub_task_description'], self.request.id, datetime_to_epoch(make_mix=False), 'ST', task_descr['subtask_id'])
		)

		# Creating an information about subtask steps
		if not isinstance(task_descr['sub_task_steps'], list):
			task_descr['sub_task_steps'] = [task_descr['sub_task_steps'], ]

		step_counter = itertools.count(1)
		# Each subtask step is in the pending state now:
		cur.executemany(
			'''
			INSERT INTO cloudmanager_subtaskstep (subtask_id, step_num,  status, comment)
			VALUES (%s, %s, %s, %s)''',
			(
				[(task_descr['subtask_id'], step_counter.next(), 'PN', step) for step in task_descr['sub_task_steps']]
			)
		)
		cur.close()

	def nodes_live_check(self, nodes):
		'''
		Checks that Nodes we want to operate with are live.

		:param nodes: nodes' ids (names)
		'''
		logger.info('Checking for live minions...')
		import salt.config
		import salt.runner

		if not isinstance(nodes, list):
			nodes = [nodes, ]
		opts = salt.config.master_config('/etc/salt/master')
		runner = salt.runner.RunnerClient(opts)
		for attempt in range(11):
			logger.info('Checking for live minions. Attempt # %s.', attempt)
			clusters_up = runner.cmd('manage.up', [])
			if len(set(nodes).difference(set(clusters_up))) == 0:
				logger.info('Checking for live minions done.')
				return True
			clusters_down = ','.join([str(cluster) for cluster in (set(nodes).difference(set(clusters_up)))])
			msg = 'Attempt # {}. These nodes are not reachable at this moment: {}'.format(attempt, clusters_down)
			logger.error(msg)
		return False

	@use_pgsql()
	def salt_cmd_async(self, conn, minion_name, cmd, cmd_args, step_num=None):
		'''
		Runs Salt command (task) asynchronously, updates information task's step status.

		:param conn: DB connection, sets by decorator
		:param minion_name: Salt minion's id (name)
		:param cmd: command to launch
		:param cmd_args: command arguments
		:param step_num: number of subtask's step if this command belongs to task as step.
		'''

		logger.debug('Applying the salt task async.')
		import salt.client
		local = salt.client.LocalClient()
		jid = local.cmd_async(minion_name, cmd, cmd_args)
		self.jobs_in_process.append({'jid': jid, 'minion_name': minion_name, 'start_timestamp': datetime_to_epoch(make_mix=False)})

		if step_num:
			cur = conn.cursor()
			# checking for valid step number
			cur.execute(
				'''
				SELECT MAX (step_num)
				FROM cloudmanager_subtaskstep ss
				JOIN cloudmanager_subtask ts ON ss.subtask_id = ts.id
				WHERE ts.job_id = %s
				''',
				(self.request.id,)
			)
			msn = cur.fetchone()[0]
			if step_num > msn:
				cur.close()
				raise Exception('Wrong step number {} for job_id = {}'.format(step_num, self.request.id))
			# updating the step status, setting 'STARTED' status with datetime:
			cur.execute(
				'''
				UPDATE cloudmanager_subtaskstep
				SET status = 'ST', start_dt = %s, step_identifier = %s
				WHERE step_num = %s
				AND subtask_id = (SELECT id FROM cloudmanager_subtask WHERE job_id = %s)
				''',
				(datetime_to_epoch(make_mix=False), jid, step_num, self.request.id,)
			)
			conn.commit()
			# if there are any steps with number less that current, than we assume that they are skipped:
			cur.execute(
				'''
				UPDATE cloudmanager_subtaskstep
				SET status = 'SK'
				WHERE status = 'PN'
				AND step_num < %s
				AND subtask_id = (SELECT id FROM cloudmanager_subtask WHERE job_id = %s)
				''',
				(step_num, self.request.id,)
			)
			cur.close()
		logger.debug('Salt task applied. Salt JID = %s', jid)
		return jid

	@use_pgsql()
	def get_salt_result(self, conn, salt_jid, minion_name):
		'''
		Gets the Salt job (task) result by Salt job_id.
		:param conn: DB connection, sets by decorator
		:param salt_jid: Salt job id
		:param minion_name: Salt minion's id (name)
		:returns: salt's task execution result
		'''
		logger.error('Looking for result of salt JID=%s', salt_jid)

		from time import sleep
		import salt.config
		import salt.runner

		ret = None
		# we will wait result for 30 minutes
		result_timeout_sec = 30 * 60

		opts = salt.config.master_config('/etc/salt/master')
		runner = salt.runner.RunnerClient(opts)

		while not ret:
			ret = runner.cmd('jobs.lookup_jid', [salt_jid, ])
			# getting the start timestamp for this Salt job
			job_start_timestamp = None
			for job in self.jobs_in_process:
				if job['jid'] == salt_jid:
					job_start_timestamp = job['start_timestamp']
					break
			# if we are out of result_timeout_sec interval:
			if not ret and datetime_to_epoch(make_mix=False) - job_start_timestamp > result_timeout_sec:
				if self.nodes_live_check(minion_name):
					opts = salt.config.master_config('/etc/salt/master')
					runner = salt.runner.RunnerClient(opts)
					active_jobs = runner.cmd('jobs.active', [])
					# If Salt task is not active any more - we lost it.
					if salt_jid not in active_jobs:
						ret = {
							minion_name: {
								'message': 'Can not locate jid [{}] in active Salt jobs'.format(salt_jid),
								'result': 'job missed',
								'exit_code': 1
							}
						}
					else:
						logger.info("Salt task {} has timeout, but it is still active. Continue to wait the task's result".format(salt_jid))
				else:
					ret = {
						minion_name: {
							'message': 'Salt Minion {} has gone away'.format(minion_name),
							'result': 'minion missed',
							'exit_code': 1
						}
					}

			sleep(10)

		result = ret[minion_name]
		cur = conn.cursor()
		if result['exit_code'] == 0:
			status = 'SU'
			res = result['result']
		else:
			status = 'FA'
			res = result['message']
		# Updating the subtask's step status:
		cur.execute(
			'''
			UPDATE cloudmanager_subtaskstep
			SET end_dt = %s, status = %s, result = %s
			WHERE step_identifier = %s
			''',
			(datetime_to_epoch(make_mix=False), status, str(res), salt_jid)
		)
		cur.close()

		self.jobs_in_process.remove({'jid': salt_jid, 'minion_name': minion_name, 'start_timestamp': job_start_timestamp})
		if result['exit_code'] != 0:
			conn.commit()
			raise Exception('Salt task {} failed. Error message: {}'.format(salt_jid, res))

		return result

	def on_failure(self, exc, task_id, args, kwargs, einfo):
		'''
		Standard Celery error handler for task.
		'''

		logger.error('Task failed. There are %s Salt-jobs needs to be ended', len(self.jobs_in_process))
		# get_salt_result method will remove the list elements, so that we do slicing for iteration:
		for job in self.jobs_in_process[:]:
			logger.error('Waiting the job %s to be done on minion %s.', job['jid'], job['minion_name'])
			# trying to complete all tasks
			try:
				self.get_salt_result(job['jid'], job['minion_name'])
				logger.error('Job %s is done on minion %s.', job['jid'], job['minion_name'])
			except Exception as e:
				logger.exception('Salt task has raised exception while it was trying to be done. %s', str(e))


@app.task(bind=True)
def check_locks(self):
	'''
	Checks tenant's locks on clusters.
	'''

	logger.info('Checking tenant locks.')
	from celery.result import AsyncResult
	import psycopg2
	# FIXME: remove hardcoded connection parameters. @ALX
	conn = psycopg2.connect(host="localhost", port=5432, database="cloudmanager", user="postgres", password="123")
	cur = conn.cursor()
	# getting all Celery job ids that are "in progress" - Started:
	cur.execute(
		"""
		SELECT st.job_id
		FROM cloudmanager_task t
		JOIN cloudmanager_subtask st ON t.id = st.task_id
		WHERE st.status='ST'
		AND st.start_dt IS NOT NULL
		""")
	tasks = cur.fetchall()
	for task in tasks:
		# Celery job id
		jid = task[0]
		# Celery AsyncResult
		job = AsyncResult(jid)
		if job.ready():
			if job.failed():
				cur.execute(
					"UPDATE cloudmanager_subtask SET status ='FA', end_dt = %s, error_message = %s WHERE job_id = %s ",
					(datetime_to_epoch(make_mix=False), str(job.info), jid,)
				)
				logger.error('Subtask failed. Subtask (Celery) id = %s. Error message: %s', jid, job.info)
			else:
				cur.execute(
					"UPDATE cloudmanager_subtask SET status ='SU', end_dt = %s WHERE job_id = %s",
					(datetime_to_epoch(make_mix=False), jid,)
				)
			# if celery task is ready, but some steps (subtasks) are still in pending mode,
			# then we expect that they were skipped:
			cur.execute(
				'''
				UPDATE cloudmanager_subtaskstep
				SET status = 'SK'
				WHERE status = 'PN'
				AND subtask_id = (SELECT id FROM cloudmanager_subtask WHERE job_id = %s)
				''',
				(jid,)
			)
	conn.commit()
	# setting the task's status based on subtask's result:
	cur.execute(
		"""
		UPDATE cloudmanager_task t
		SET status = CASE
			WHEN (
				EXISTS (SELECT 1 FROM cloudmanager_subtask sub WHERE t.id = sub.task_id AND sub.status = 'FA')
			) THEN 'FA'
			WHEN (
				EXISTS (SELECT 1 FROM cloudmanager_subtask sub WHERE t.id = sub.task_id AND sub.status = 'CA')
			) THEN 'CA'
			WHEN (
				(SELECT COUNT (1) FROM cloudmanager_subtask sub WHERE t.id = sub.task_id) =
				(SELECT COUNT (1) FROM cloudmanager_subtask sub WHERE t.id = sub.task_id AND sub.status = 'SU' )
			) THEN 'SU'
			ELSE 'ST'
		END
		WHERE status = 'ST'
		AND NOT EXISTS (
			SELECT 1
			FROM cloudmanager_subtask st
			JOIN cloudmanager_subtaskstep sts ON st.id = sts.subtask_id
			WHERE t.id = st.task_id
			AND sts.status = 'ST'
		)
		""",)
	conn.commit()
	# if task has end_dt not null than it is done:
	cur.execute(
		"""
		UPDATE cloudmanager_task
		SET end_dt = %s
		WHERE status != 'ST'
		AND end_dt IS NULL""",
		(datetime_to_epoch(make_mix=False),)
	)
	conn.commit()
	# if all tasks are done than we can free the tenant's lock
	# TODO: do we need the last EXISTS SELECT? @ALX
	cur.execute('''
		DELETE FROM cloudmanager_tenantlock
		WHERE task_id IN (
			SELECT t.id
			FROM cloudmanager_task t
			WHERE t.end_dt IS NOT NULL
			AND NOT EXISTS (
				SELECT 1
				FROM cloudmanager_subtask st
				JOIN cloudmanager_subtaskstep sts ON st.id = sts.subtask_id
				WHERE t.id = st.task_id
				AND sts.status = 'ST'
			)
		)
		''')
	conn.commit()
	cur.close()
	conn.close()


@app.task(bind=True, base=AbstractTask)
def tenant_migrate(self, subtask_id, src_cluster_tmid, src_cluster_node, dst_cluster_tmid, dst_cluster_node, cluster_version, tenant_name):
	'''
	Migrates tenant between the clusters.

	:param subtask_id: subtask_id (global Task -> Subtask.id)
	:param src_cluster_tmid: source cluster tmid
	:param src_cluster_node: source SALT data node (name)
	:param dst_cluster_tmid: destination cluster tmid
	:param dst_cluster_node: destination SALT data node (name)
	:param cluster_version: source cluster version
	:param tenant_name: tenant name
	'''

	self.publish_task(
		{
			'subtask_id': subtask_id,
			'sub_task_description': 'Full tenant migration',
			'sub_task_steps': [
				"export tenant to S3 from the source cluster",
				"export tenant to S3 from the destination cluster (if exists)",
				"export tenant's principals to S3 from the destination cluster (if exists)",
				"drop tenant on the destination cluster (if exists)",
				"import tenant from S3 to the destination cluster",
				"drop tenant's principals data on the destination cluster",
				"import tenant's principals data from S3 to the destination cluster",
			],
		}
	)

	if not self.nodes_live_check([src_cluster_node, dst_cluster_node, ]):
		raise Exception('Some nodes are not reachable at this moment. Migration has been canceled.')

	# check if the tenant exists on the target machine
	jid = self.salt_cmd_async(dst_cluster_node, 'tmnode.tenant_exists', [tenant_name, ])
	job_res = self.get_salt_result(jid, dst_cluster_node)

	if job_res['exit_code'] == 0:
		end_tenant_exists = job_res['result']
	else:
		msg = 'Cannot get the information from the destination cluster ({})'.format(job_res['message'])
		raise Exception(msg)

	source_export_job = self.salt_cmd_async(src_cluster_node, 'tmnode.e_tenant_migrate', [tenant_name, ], step_num=1)
	if end_tenant_exists:
		dest_export_job = self.salt_cmd_async(dst_cluster_node, 'tmnode.e_tenant_migrate', [tenant_name, ], step_num=2)
		dest_principals_job = self.salt_cmd_async(dst_cluster_node, 'tmnode.e_tenant_migrate', [tenant_name, 'ePrincipals'], step_num=3)

	source_export_result = self.get_salt_result(source_export_job, src_cluster_node)

	if end_tenant_exists:
		_dest_export_result = self.get_salt_result(dest_export_job, dst_cluster_node)
		dest_principals_result = self.get_salt_result(dest_principals_job, dst_cluster_node)

	if end_tenant_exists:
		# dropping the tenant
		jid = self.salt_cmd_async(dst_cluster_node, 'tmnode.tenant_drop', [tenant_name, ], step_num=4)
		job_res = self.get_salt_result(jid, dst_cluster_node)

	# restoring the tenant
	jid = self.salt_cmd_async(dst_cluster_node, 'tmnode.i_tenant_migrate', [cluster_version, tenant_name, src_cluster_tmid, source_export_result['result']['export_id']], step_num=5)
	job_res = self.get_salt_result(jid, dst_cluster_node)

	if end_tenant_exists:
		logger.info('Destination tenant exists')
		# dropping the principals data
		jid = self.salt_cmd_async(dst_cluster_node, 'tmnode.tenant_drop', [tenant_name, 'ersrPrincipals'], step_num=6)
		job_res = self.get_salt_result(jid, dst_cluster_node)

		# restoring the original Principals data
		jid = self.salt_cmd_async(dst_cluster_node, 'tmnode.i_tenant_migrate', [cluster_version, tenant_name, dst_cluster_tmid, dest_principals_result['result']['export_id']], step_num=7)
		job_res = self.get_salt_result(jid, dst_cluster_node)
	else:
		logger.info('Destination tenant does not exist')
		logger.debug("Connecting %s tenant to the %s cluster", tenant_name, dst_cluster_node)
		import psycopg2
		conn = psycopg2.connect(host="localhost", port=5432, database="cloudmanager", user="postgres", password="123")
		t_cur = conn.cursor()

		logger.debug('Getting the tenant with "%s" name', tenant_name)
		t_cur.execute(
			"SELECT id FROM cloudmanager_tenant WHERE name = %s",
			(tenant_name,)
		)
		tenant_id = t_cur.fetchone()[0]

		logger.debug('Getting cluster with %s id', dst_cluster_tmid)
		t_cur.execute(
			"SELECT id FROM cloudmanager_cluster WHERE tmid = %s",
			(dst_cluster_tmid,)
		)
		cluster_id = t_cur.fetchone()[0]

		logger.info('Inserting connection information')
		t_cur.execute(
			"INSERT INTO cloudmanager_tenant_cluster (tenant_id, cluster_id) VALUES (%s, %s)",
			(tenant_id, cluster_id)
		)
		t_cur.close()
		conn.commit()
		conn.close()


@app.task(bind=True, base=AbstractTask)
def tenant_clone(self, subtask_id, src_cluster_tmid, dst_cluster_tmid, src_cluster_node, dst_cluster_node, cluster_version, old_tenant_name, new_tenant_tmid, new_tenant_name):
	'''
	Clones the tenant.

	:param subtask_id: subtask_id (global Task -> Subtask.id)
	:param src_cluster_tmid: source cluster tmid
	:param src_cluster_node: source cluster data node name
	:param dst_cluster_tmid: destination cluster tmid
	:param dst_cluster_node: destination cluster data node name
	:param cluster_version: source cluster version
	:param old_tenant_name: old tenant name
	:param new_tenant_tmid: new tenant tmid
	:param new_tenant_name: new tenant name
	'''

	self.publish_task(
		{
			'subtask_id': subtask_id,
			'sub_task_description': 'Cloning the tenant',
			'sub_task_steps': [
				"export tenant to S3 from the source cluster",
				"modify and import tenant data",
			],
		}
	)

	if not self.nodes_live_check([src_cluster_node, dst_cluster_node, ]):
		raise Exception('Some nodes are not reachable at this moment. Cloning has been canceled.')

	source_export_job = self.salt_cmd_async(src_cluster_node, 'tmnode.pre_tenant_clone', [old_tenant_name, ], step_num=1)
	source_export_result = self.get_salt_result(source_export_job, src_cluster_node)
	if source_export_result['exit_code'] == 0:
		source_export_id = source_export_result['result']['export_id']
		clone_job = self.salt_cmd_async(dst_cluster_node, 'tmnode.tenant_clone', [cluster_version, old_tenant_name, src_cluster_tmid, source_export_id, new_tenant_tmid, new_tenant_name], step_num=2)
		dest_clone_result = self.get_salt_result(clone_job, dst_cluster_node)
		if dest_clone_result['exit_code'] != 0:
			msg = 'Error occurred while cloning tenant data on RDS (pgsql) or on the destination node (hdfs)  ({})'.format(dest_clone_result['message'])
			raise Exception(msg)
	else:
		msg = 'Error occurred while exporting tenant data from source node  ({})'.format(source_export_result['message'])
		raise Exception(msg)


@app.task(bind=True, base=AbstractTask)
def tenant_backup(self, subtask_id, src_cluster_id, src_cluster_tmid, src_cluster_node, cluster_version, tenant_name, tenant_id, user_id, comment):

	'''
	Tenant backup task.

	:param subtask_id: subtask_id (global Task -> Subtask.id)
	:param src_cluster_id: cluster id (not Tidemark id)
	:param src_cluster_tmid: source cluster tmid
	:param src_cluster_node: source cluster data node name
	:param cluster_version: source cluster version
	:param tenant_name: tenant name
	:param tenant_id: tenant id (not Tidemark id)
	:param user_id: user id that has launched the task
	:param comment: user comment for the backup task

	Puts into AWS DynamoDb backup information in next format:
	{
		u'comment': u'User comment for backup',
		u'user_id': 1,
		u'cluster_version': u'1.5.0.2',
		u'tenant_name': u'reddyice',
		u'cluster_id': 2,
		u'tenant_id': 2,
		u'export_id': 1426730386131013,
		u'meta': {
			u'storage_info': [
				{
					u'start_timestamp': 1426730386486039,
					u'exported_data': u'eDeepSleep, eAppModel, eR1, eBuckets, eToggles, eMetamodel',
					u'filename': u'pgsql.tar.gz',
					u'storage_type': u'pgsql',
					u'filesize': 12749938,
					u's3_path': u'TmBackup/1.5.0.2/reddyice/9068355b-7795-4192-bf17-5e299fc23d5a/1426730386131013',
					u'end_timestamp': 1426730414743497
				},
				{
					u'start_timestamp': 1426730414917450,
					u'exported_data': u'eHdfs',
					u'filename': u'hdfs.tar.gz',
					u'storage_type': u'hdfs',
					u'filesize': 14050008,
					u's3_path': u'TmBackup/1.5.0.2/reddyice/9068355b-7795-4192-bf17-5e299fc23d5a/1426730386131013',
					u'end_timestamp': 1426730422628513
				}
			],
			u's3_path': u'TmBackup/1.5.0.2/reddyice/9068355b-7795-4192-bf17-5e299fc23d5a/1426730386131013'
		},
	}
	'''
	logger.error('Tenant backup started')
	self.publish_task(
		{
			'subtask_id': subtask_id,
			'sub_task_description': 'Backup the tenant',
			'sub_task_steps': [
				"export tenant to S3 from the source cluster",
			],
		}
	)

	if not self.nodes_live_check([src_cluster_node, ]):
		raise Exception('Some nodes are not reachable at this moment. Creating backup has been canceled.')

	source_export_job = self.salt_cmd_async(src_cluster_node, 'tmnode.tenant_backup', [tenant_name, ], step_num=1)
	source_export_result = self.get_salt_result(source_export_job, src_cluster_node)
	if source_export_result['exit_code'] == 0:
		# put backup metadata data to the DynamoDb
		logger.error('Connecting to AWS DynamoDb...')

		aws_session = boto3.session.Session(
			aws_access_key_id=settings.S3_AWS_ACCESS_KEY_ID,
			aws_secret_access_key=settings.S3_AWS_SECRET_ACCESS_KEY,
			region_name='us-west-1'
		)
		dynamo_db = aws_session.client('dynamodb')

		logger.error('Writing metadata to the AWS DynamoDb...')
		data = {
			'export_id': source_export_result['result']['export_id'],
			'user_id': user_id,
			'tenant_id': tenant_id,
			'tenant_name': tenant_name or None,
			'cluster_version': cluster_version or None,
			'cluster_id': src_cluster_id,
			'meta': source_export_result['result']['meta'] or None,
			'comment': comment or None,
		}
		dynamized_item = dict_to_dynamo(data)
		dynamo_db.put_item(TableName='cm2-tenant-backup', Item=dynamized_item)
		logger.error('Backup metadata has been written to the AWS DynamoDb...')
	else:
		msg = 'Error occurred while exporting tenant data from source node  ({})'.format(source_export_result['message'])
		logger.error(msg)
		raise Exception(msg)


@app.task(bind=True, base=AbstractTask)
def save_golden_state(self, subtask_id, src_cluster_node, cluster_version, tenant_name, tenant_id, user_id, comment):

	'''
	Tenant backup task.

	:param subtask_id: subtask_id (global Task -> Subtask.id)
	:param src_cluster_id: cluster id (not Tidemark id)
	:param src_cluster_tmid: source cluster tmid
	:param src_cluster_node: source cluster data node name
	:param cluster_version: source cluster version
	:param tenant_name: tenant name
	:param tenant_id: tenant id (not Tidemark id)
	:param user_id: user id that has launched the task
	:param comment: user comment for the backup task

	Puts into AWS DynamoDb backup information in next format:
	{
		u'comment': u'User comment for backup',
		u'user_id': 1,
		u'cluster_version': u'1.5.0.2',
		u'tenant_name': u'reddyice',
		u'cluster_id': 2,
		u'tenant_id': 2,
		u'export_id': 1426730386131013,
		u'meta': {
			u'storage_info': [
				{
					u'start_timestamp': 1426730386486039,
					u'exported_data': u'eDeepSleep, eAppModel, eR1, eBuckets, eToggles, eMetamodel',
					u'filename': u'pgsql.tar.gz',
					u'storage_type': u'pgsql',
					u'filesize': 12749938,
					u's3_path': u'TmBackup/1.5.0.2/reddyice/9068355b-7795-4192-bf17-5e299fc23d5a/1426730386131013',
					u'end_timestamp': 1426730414743497
				},
				{
					u'start_timestamp': 1426730414917450,
					u'exported_data': u'eHdfs',
					u'filename': u'hdfs.tar.gz',
					u'storage_type': u'hdfs',
					u'filesize': 14050008,
					u's3_path': u'TmBackup/1.5.0.2/reddyice/9068355b-7795-4192-bf17-5e299fc23d5a/1426730386131013',
					u'end_timestamp': 1426730422628513
				}
			],
			u's3_path': u'TmBackup/1.5.0.2/reddyice/9068355b-7795-4192-bf17-5e299fc23d5a/1426730386131013'
		},
	}
	'''
	logger.error('Tenant backup started')
	self.publish_task(
		{
			'subtask_id': subtask_id,
			'sub_task_description': 'Save tenant\'s golden state',
			'sub_task_steps': [
				"export tenant to S3 from the source cluster",
			],
		}
	)

	if not self.nodes_live_check([src_cluster_node, ]):
		raise Exception('Some nodes are not reachable at this moment. Saving golden state has been canceled.')

	source_export_job = self.salt_cmd_async(src_cluster_node, 'tmnode.set_gold_tenant', [tenant_name, ], step_num=1)
	source_export_result = self.get_salt_result(source_export_job, src_cluster_node)
	if source_export_result['exit_code'] == 0:
		# put golden state metadata data to the DynamoDb
		logger.error('Connecting to AWS DynamoDb...')

		aws_session = boto3.session.Session(
			aws_access_key_id=settings.S3_AWS_ACCESS_KEY_ID,
			aws_secret_access_key=settings.S3_AWS_SECRET_ACCESS_KEY,
			region_name='us-west-1'
		)
		dynamo_db = aws_session.client('dynamodb')

		logger.error('Writing metadata to the AWS DynamoDb...')
		data = {
			'export_id': source_export_result['result']['export_id'],
			'user_id': user_id,
			'tenant_id': tenant_id,
			'cluster_version': cluster_version or None,
			'meta': source_export_result['result']['meta'] or None,
			'comment': comment or None,
		}
		dynamized_item = dict_to_dynamo(data)
		dynamo_db.put_item(TableName='cm2-tenant-golden-state', Item=dynamized_item)
		logger.error('Golden state metadata has been written to the AWS DynamoDb...')
	else:
		msg = 'Error occurred while exporting tenant data from source node  ({})'.format(source_export_result['message'])
		logger.error(msg)
		raise Exception(msg)


@app.task(bind=True, base=AbstractTask)
def principals_migrate(self, subtask_id, src_cluster_tmid, src_cluster_node, dst_cluster_node, cluster_version, tenant_name):
	'''
	Migrates only the principal's data between the clusters.

	:param subtask_id: subtask_id (global Task -> Subtask.id)
	:param src_cluster_tmid: source cluster's tmid
	:param src_cluster_node: data node's name on source cluster
	:param dst_cluster_node: destination cluster's tmid
	:param cluster_version:  destination cluster's version
	:param tenant_name: tenant's name
	'''

	self.publish_task(
		{
			'subtask_id': subtask_id,
			'sub_task_description': "Tenant's principals migration",
			'sub_task_steps': [
				"export tenant's principals data to S3 from the source cluster",
				"drop tenant's principals data on the destination cluster",
				"import tenant's principal data from S3 to the destination cluster",
			],
		}
	)

	if not self.nodes_live_check([src_cluster_node, dst_cluster_node, ]):
		raise Exception('Some nodes are not reachable at this moment. Principals migration has canceled.')

	# check if the tenant exists on the target machine
	jid = self.salt_cmd_async(dst_cluster_node, 'tmnode.tenant_exists', [tenant_name, ])
	job_res = self.get_salt_result(jid, dst_cluster_node)

	if job_res['exit_code'] == 0:
		if not job_res['result']:
			raise Exception('Destination tenant does not exist.')
	else:
		raise Exception('Cannot get the information from the destination cluster ({})'.format(job_res['message']))

	source_export_job = self.salt_cmd_async(src_cluster_node, 'tmnode.e_tenant_migrate', [tenant_name, 'ePrincipals'], step_num=1)
	dest_drop_job = self.salt_cmd_async(dst_cluster_node, 'tmnode.tenant_drop', [tenant_name, 'ersrPrincipals'], step_num=2)

	source_export_result = self.get_salt_result(source_export_job, src_cluster_node)
	dest_drop_result = self.get_salt_result(dest_drop_job, dst_cluster_node)

	if source_export_result['exit_code'] == 0 and dest_drop_result['exit_code'] == 0:
		jid = self.salt_cmd_async(dst_cluster_node, 'tmnode.i_tenant_migrate', [cluster_version, tenant_name, src_cluster_tmid, source_export_result['result']['export_id']], step_num=3)
		job_res = self.get_salt_result(jid, dst_cluster_node)
	else:
		raise Exception(source_export_result['message'])


@app.task(bind=True, base=AbstractTask)
def tenant_restore(self, subtask_id, target_cluster_tmid, target_cluster_node, orig_cluster_tmid, orig_cluster_version, tenant_name, backup_id):
	'''
	Restores tenant from backup with given backup data.

	:param subtask_id: subtask_id (global Task -> Subtask.id)
	:param target_cluster_node: data node's name on source cluster
	:param orig_cluster_tmid: original cluster tmid (backup source)
	:param orig_cluster_version:  destination cluster's version
	:param tenant_name: tenant's name
	:param backup_id: export id on AWS S3 bucket
	'''

	self.publish_task(
		{
			'subtask_id': subtask_id,
			'sub_task_description': "Tenant restore",
			'sub_task_steps': [
				"restore tenant's data",
			],
		}
	)

	if not self.nodes_live_check([target_cluster_node, ]):
		raise Exception('Target node is not reachable at this moment. Restore is canceled.')

	# check if the tenant exists on the target machine
	jid = self.salt_cmd_async(target_cluster_node, 'tmnode.tenant_exists', [tenant_name, ])
	job_res = self.get_salt_result(jid, target_cluster_node)

	tenant_exists = job_res['result']
	if tenant_exists:
		# dropping the tenant
		jid = self.salt_cmd_async(target_cluster_node, 'tmnode.tenant_drop', [tenant_name, ])
		job_res = self.get_salt_result(jid, target_cluster_node)

	jid = self.salt_cmd_async(target_cluster_node, 'tmnode.tenant_restore', [orig_cluster_version, tenant_name, orig_cluster_tmid, backup_id], step_num=1)
	job_res = self.get_salt_result(jid, target_cluster_node)


@app.task(bind=True, base=AbstractTask)
def download_golden_state(self, subtask_id, target_cluster_tmid, target_cluster_node, orig_cluster_tmid, orig_cluster_version, tenant_name, backup_id):
	'''
	Restores tenant from backup with given backup data.

	:param subtask_id: subtask_id (global Task -> Subtask.id)
	:param target_cluster_node: data node's name on source cluster
	:param orig_cluster_tmid: original cluster tmid (backup source)
	:param orig_cluster_version:  destination cluster's version
	:param tenant_name: tenant's name
	:param backup_id: export id on AWS S3 bucket
	'''

	self.publish_task(
		{
			'subtask_id': subtask_id,
			'sub_task_description': "Download and apply golden state",
			'sub_task_steps': [
				"download and apply golden state",
			],
		}
	)

	if not self.nodes_live_check([target_cluster_node, ]):
		raise Exception('Target node is not reachable at this moment. Restore is canceled.')

	# check if the tenant exists on the target machine
	jid = self.salt_cmd_async(target_cluster_node, 'tmnode.tenant_exists', [tenant_name, ])
	job_res = self.get_salt_result(jid, target_cluster_node)

	tenant_exists = job_res['result']
	if tenant_exists:
		# dropping the tenant
		jid = self.salt_cmd_async(target_cluster_node, 'tmnode.tenant_drop', [tenant_name, ])
		job_res = self.get_salt_result(jid, target_cluster_node)
	jid = self.salt_cmd_async(target_cluster_node, 'tmnode.get_gold_tenant', [orig_cluster_version, tenant_name, orig_cluster_tmid, backup_id], step_num=1)
	job_res = self.get_salt_result(jid, target_cluster_node)
