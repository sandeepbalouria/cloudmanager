from .models import TenantLock, Task, Subtask, Node
from cloudmanager.models import Cluster
from htools.funcs import datetime_to_epoch


class ObjectExplorer(object):

	@staticmethod
	def get_data_node(cluster):
		'''
		Returns cluster's node with 'data' type
		:param cluster: cluster's id or cluster object, where data node is located
		'''
		if isinstance(cluster, int):
			cluster = Cluster.objects.get(id=cluster)
		elif not isinstance(cluster, Cluster):
			raise Exception("Wrong [cluster] parameter type (should be either int or Cluster), but {} got.".format(type(cluster)))

		if cluster.type == 'MN':
			node_type = 'DN'
		else:
			node_type = 'SN'
		node = Node.objects.get(nodeType=node_type, cluster=cluster)
		return node


class LockManager(object):

	@staticmethod
	def create_lock():
		'''
		Creates lock for Django task.
		'''
		lock = TenantLock()
		lock.save()
		return lock

	@staticmethod
	def lock_exists(nodes=None, tenants=None):
		'''
		Checks for lock on clusters / tenant.
		Returns Lock object if lock found, else - None

		:param cluster_nodes: nodes objects
		:param tenants: tenant objects
		'''
		if not isinstance(nodes, list):
			nodes = [nodes, ]

		if not isinstance(tenants, list):
			tenants = [tenants, ]

		if TenantLock.objects.filter(task__subtask__tenant__in=tenants, task__subtask__node__in=nodes).exists():
			return TenantLock.objects.filter(task__subtask__tenant__in=tenants, task__subtask__node__in=nodes).distinct()

	@staticmethod
	def lock_task(lock, task):
		'''
		Attach given lock object to the given task object.

		:param task: task object
		:param lock: lock object
		'''
		task.tenantlock_set.add(lock)
		task.save()
		return task


class CeleryMix (object):

	@staticmethod
	def create_task(user_id, description):
		'''
		Creates Task and lock for this task.
		Returns Task object.
		'''
		tenant_task = Task(
			description=description,
			user_id=user_id,  # self.request.user.id,
			start_dt=datetime_to_epoch()
		)

		tenant_task.save()
		return tenant_task

	@staticmethod
	def create_subtask(task, tenants, nodes):
		'''
		Creates and returns a subtask for current Task.

		:param tenants: tenants objects
		:param nodes: node objects

		'''
		if not isinstance(tenants, list):
			tenants = [tenants, ]

		if not isinstance(nodes, list):
			nodes = [nodes, ]

		subtask = Subtask(
			task=task,  # self.django_tenant_task,
		)
		subtask.save()
		subtask.tenant.add(*tenants)
		subtask.node.add(*nodes)
		subtask.save()

		return subtask
