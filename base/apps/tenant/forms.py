from django import forms
from django.core.exceptions import ValidationError


class BaseTenantForm(forms.Form):
	"""
	Base form class for tenant operation.
	"""

	tenant_id = forms.IntegerField(label='Tenant')
	source_cluster = forms.ChoiceField(label='Source cluster')
	comment = forms.CharField(label='comment', required=False)

	def __init__(self, *args, **kwargs):
		super(BaseTenantForm, self).__init__(*args, **kwargs)
		if (
			self.initial.get('tenant_id', None) and
			self.initial.get('source_cluster', None)):
				self.fields['tenant_id'].value = self.initial['tenant_id']
				self.fields['source_cluster'].choices = self.initial['source_cluster']


class TenantDestinationForm(BaseTenantForm):
	"""
	Extends BaseTenantForm by adding the "destination_cluster" field for
	migration and cloning operations.
	"""

	destination_cluster = forms.ChoiceField(label='Destination cluster')

	def __init__(self, *args, **kwargs):
		super(TenantDestinationForm, self).__init__(*args, **kwargs)
		if self.initial.get('destination_cluster', None):
			self.fields['destination_cluster'].choices = self.initial['destination_cluster']


class BackupTenantForm(BaseTenantForm):
	"""
	Form for tenant's backups
	"""
	pass


class RestoreTenantForm(BaseTenantForm):
	"""
	Form for tenant's restore operation
	"""
	export_id = forms.CharField(label='export_id', required=False)


class MigrateTenantForm(TenantDestinationForm):
	"""
	Tenant migration form
	"""

	MIGRATION_TYPE = (
		(1, 'full tenant migration'),
		(2, 'principals migration'),
	)

	migration_type = forms.ChoiceField(label='Migration type', choices=MIGRATION_TYPE)

	def clean_destination_cluster(self):

		if 'source_cluster' in self.cleaned_data and self.cleaned_data['destination_cluster'] == self.cleaned_data['source_cluster']:
			raise ValidationError('The source cluster cannot be the same as destination cluster')

		return self.cleaned_data['destination_cluster']


class CloneTenantForm (TenantDestinationForm):
	"""
	Tenant clone form.
	"""

	new_tenant_name = forms.CharField(label="New tenant's name", max_length=256)
	new_tenant_id = forms.IntegerField(label="New tenant's id")


class DeleteTBForm(forms.Form):
	"""
	Delete tenant's backup form
	"""
	tenant_id = forms.IntegerField(label='Tenant')
	export_id = forms.IntegerField(label='Backup')
