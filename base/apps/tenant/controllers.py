import datetime
import logging

from django.conf import settings
import boto3

from cloudmanager.models import Cluster
from dynamizer.dynamizer import dynamo_to_dict
from htools.funcs import datetime_to_epoch, clear_timestamp

from .models import Tenant
from .pkg_core import CeleryMix, LockManager, ObjectExplorer
from tenant.tasks import tenant_migrate, principals_migrate, tenant_backup, tenant_restore, save_golden_state, download_golden_state, tenant_clone


logger = logging.getLogger("cloudmanager")


class Backup(LockManager, CeleryMix, ObjectExplorer):
	"""
	Represents common tenant's backup operations (do backup, list backups etc)
	"""

	def __init__(self, tenant_id, source_cluster_id, user_id, comment=None):

		self.tenant = Tenant.objects.get(id=tenant_id)
		self.user_id = user_id

		self.source_cluster = Cluster.objects.get(id=source_cluster_id)
		self.source_cluster_tmid = self.source_cluster.tmid
		self.source_cluster_version = self.source_cluster.attribute['cluster_version']
		self.source_node = self.get_data_node(self.source_cluster)
		self.salt_source_node = '{}-{}-{}'.format(self.source_cluster.name, self.source_node.tmid, self.source_node.nodeType.lower())
		self.comment = comment

	def make(self):
		"""
		Runs Tenant Backup Celery task.
		"""
		task = self.lock_task(
			self.create_lock(),
			self.create_task(self.user_id, 'Tenant backup')
		)
		subtask = self.create_subtask(task, self.tenant, [self.source_node, ])
		task = tenant_backup.delay(
			subtask_id=subtask.id,
			src_cluster_id=self.source_cluster.id,
			src_cluster_tmid=self.source_cluster_tmid,
			src_cluster_node=self.salt_source_node,
			cluster_version=self.source_cluster_version,
			tenant_name=self.tenant.name,
			tenant_id=self.tenant.id,
			user_id=self.user_id,
			comment=self.comment
		)

	def list(self):
		"""
		Returns list of backups based on meta information from AWS DynamoDb.
		Result format is list of dicts:
		[
			{
				u'cluster_id': 2,
				u'export_id': 1427144056002086,
				u'tenant_id': 2,
				u'tenant_name': u'Demo',
				u'cluster_version': u'1.5.0.2',
				u'comment': u'Backup comment text',
				u'start_timestamp': 1427144056,
				u'end_timestamp': 1427144091,
				u'duration': 35,
				u'summary_duration': 35,
				u'summary_filesize': 26798453,
				u'user_id': 1
				u'meta': {
					u's3_path': u'TmBackup/1.5.0.2/Demo/9068355b-7795-4192-bf17-5e299fc23d5a/1427144056002086',
					u'storage_info': [{
										u'storage_type': u'pgsql',
										u'exported_data': u'Buckets, Toggles, DeepSleep, AppModel, Metamodel, R1',
										u'filename': u'pgsql.tar.gz',
										u'filesize': 12749940,
										u'start_timestamp': 1427144056,
										u'end_timestamp': 1427144084,
										u'duration': 28,
										u's3_path': u'TmBackup/1.5.0.2/Demo/9068355b-7795-4192-bf17-5e299fc23d5a/1427144056002086',
									},
									{
										u'storage_type': u'hdfs',
										u'exported_data': u'Hdfs',
										u'filename': u'hdfs.tar.gz',
										u'filesize': 14048513,
										u'start_timestamp': 1427144084,
										u'end_timestamp': 1427144091,
										u'duration': 7,
										u's3_path': u'TmBackup/1.5.0.2/Demo/9068355b-7795-4192-bf17-5e299fc23d5a/1427144056002086',
									}]
				},
			},
		...
		]
		"""

		tenant_id = self.tenant.id
		cluster_id = self.source_cluster.id

		aws_session = boto3.session.Session(
			aws_access_key_id=settings.S3_AWS_ACCESS_KEY_ID,
			aws_secret_access_key=settings.S3_AWS_SECRET_ACCESS_KEY,
			region_name='us-west-1'
		)
		dynamo_db = aws_session.client('dynamodb')
		month_ago = datetime.datetime.utcnow() - datetime.timedelta(days=31)
		timestamp_month_ago = datetime_to_epoch(month_ago)
		result_set = dynamo_db.query(
			TableName='cm2-tenant-backup',
			KeyConditions={
				'tenant_id': {
					'AttributeValueList': [{'N': str(tenant_id)}, ],
					'ComparisonOperator': 'EQ'
				},
				'export_id': {
					'AttributeValueList': [{'N': str(timestamp_month_ago)}, ],
					'ComparisonOperator': 'GE'
				},
			},
			ExpressionAttributeValues={":cluster_id": {"N": str(cluster_id)}},
			FilterExpression='cluster_id = :cluster_id',
			ScanIndexForward=False,  # for order in reverse mode
		)

		backup_meta = []
		for result in result_set['Items']:
			formatted_result = dynamo_to_dict(result)
			# adding some additional meta
			# calcing duration times
			formatted_result[u'start_timestamp'] = []
			formatted_result[u'end_timestamp'] = []
			formatted_result[u'summary_duration'] = 0
			formatted_result[u'summary_filesize'] = 0

			for storage in formatted_result['meta']['storage_info']:
				formatted_result['start_timestamp'].append(storage['start_timestamp'])
				formatted_result['end_timestamp'].append(storage['end_timestamp'])
				storage[u'duration'] = (storage['end_timestamp'] - storage['start_timestamp'])
				formatted_result['summary_duration'] += storage['duration']
				formatted_result['summary_filesize'] += storage['filesize']

			formatted_result['start_timestamp'] = min(formatted_result['start_timestamp'])
			formatted_result['end_timestamp'] = max(formatted_result['end_timestamp'])
			formatted_result[u'duration'] = (formatted_result['end_timestamp'] - formatted_result['start_timestamp'])
			backup_meta.append(formatted_result)

		return backup_meta

	def restore(self, backup_id):
		res = {'exit_code': 0, 'message': ''}

		aws_session = boto3.session.Session(
			aws_access_key_id=settings.S3_AWS_ACCESS_KEY_ID,
			aws_secret_access_key=settings.S3_AWS_SECRET_ACCESS_KEY,
			region_name='us-west-1'
		)
		dynamo_db = aws_session.client('dynamodb')
		result_set = dynamo_db.query(
			TableName='cm2-tenant-backup',
			KeyConditions={
				'tenant_id': {
					'AttributeValueList': [{'N': str(self.tenant.id)}, ],
					'ComparisonOperator': 'EQ'
				},
				'export_id': {
					'AttributeValueList': [{'N': str(backup_id)}, ],
					'ComparisonOperator': 'EQ'
				},
			},
		)
		if int(result_set["Count"]) == 1:
			formatted_result = dynamo_to_dict(result_set['Items'][0])
			# checking backup and target clusters versions match
			if formatted_result['cluster_version'] != self.source_cluster.attribute['cluster_version']:
				res = {'exit_code': 1, 'message': 'Backup version and destination cluster version do not match'}
				return res

			task = self.lock_task(
				self.create_lock(),
				self.create_task(self.user_id, 'Tenant restore')
			)
			orig_cluster = Cluster.objects.get(id=formatted_result['cluster_id'])
			subtask = self.create_subtask(task, self.tenant, [self.source_node, ])

			if Tenant.objects.filter(
				cluster__id=self.source_cluster.id,
				id=self.tenant.id
			).count() == 0:
				self.tenant.cluster.add(self.source_cluster)
				self.tenant.save()

			task = tenant_restore.delay(
				subtask_id=subtask.id,
				target_cluster_tmid=self.source_cluster_tmid,
				target_cluster_node=self.salt_source_node,
				orig_cluster_tmid=orig_cluster.tmid,
				orig_cluster_version=formatted_result['cluster_version'],
				tenant_name=self.tenant.name,
				backup_id=backup_id
			)
			return res
		else:
			res = {'exit_code': 1, 'message': '{} backups were found with {} id.'.format(result_set["Count"], backup_id)}
			return res


class GoldenState(LockManager, CeleryMix, ObjectExplorer):
	"""
	Represents common tenant's "golden state" operations (save, restore)
	"""

	def __init__(self, tenant_id, user_id=None, source_cluster_id=None, comment=None):

		self.tenant = Tenant.objects.get(id=tenant_id)
		self.user_id = user_id

		self.source_cluster = Cluster.objects.get(id=source_cluster_id) if source_cluster_id else None
		self.source_cluster_tmid = self.source_cluster.tmid if source_cluster_id else None
		self.source_cluster_version = self.source_cluster.attribute['cluster_version'] if source_cluster_id else None
		self.source_node = self.get_data_node(self.source_cluster) if source_cluster_id else None
		self.salt_source_node = '{}-{}-{}'.format(self.source_cluster.name, self.source_node.tmid, self.source_node.nodeType.lower()) if source_cluster_id else None
		self.comment = comment

	def save(self):
		"""
		Runs Celery task for saving tenant's golden state
		"""
		task = self.lock_task(
			self.create_lock(),
			self.create_task(self.user_id, 'Save tenant\'s golden state')
		)
		subtask = self.create_subtask(task, self.tenant, [self.source_node, ])
		task = save_golden_state.delay(
			subtask_id=subtask.id,
			src_cluster_node=self.salt_source_node,
			cluster_version=self.source_cluster_version,
			tenant_name=self.tenant.name,
			tenant_id=self.tenant.id,
			user_id=self.user_id,
			comment=self.comment
		)

	def list(self):
		""" Returns information about tenant's golden states in JSON format:
		[
			{
				u'cluster_version': u'1.11.0.2',
				u'comment': u'1.11 golden state',
				u'utc_timestamp': 1427414430L,
				u'export_id': 1427414430218296,
				u'meta': {
					u's3_path': u'TmGoldenState/tenanat_name/1.5.0.2/1427414430218296',
					u'storage_info': [
						{
							u'end_timestamp': 1427414458,
							u'exported_data': u'R1, Toggles, DeepSleep, AppModel, Buckets, Metamodel',
							u'filename': u'pgsql.tar.gz',
							u'filesize': 12749909,
							u's3_path': u'TmGoldenState/tenanat_name/1.5.0.2/1427414430218296',
							u'start_timestamp': 1427414430,
							u'storage_type': u'pgsql'
						},
						{
							u'end_timestamp': 1427414466,
							u'exported_data': u'Hdfs',
							u'filename': u'hdfs.tar.gz',
							u'filesize': 14045438,
							u's3_path': u'TmGoldenState/tenanat_name/1.5.0.2/1427414430218296',
							u'start_timestamp': 1427414458,
							u'storage_type': u'hdfs'
						}
					]
				},
				u'tenant_id': 2,
				u'user_id': 1
			},
			...
		]"""
		aws_session = boto3.session.Session(
			aws_access_key_id=settings.S3_AWS_ACCESS_KEY_ID,
			aws_secret_access_key=settings.S3_AWS_SECRET_ACCESS_KEY,
			region_name='us-west-1'
		)
		dynamo_db = aws_session.client('dynamodb')
		result_set = dynamo_db.query(
			TableName='cm2-tenant-golden-state',
			KeyConditions={
				'tenant_id': {
					'AttributeValueList': [{'N': str(self.tenant.id)}, ],
					'ComparisonOperator': 'EQ'
				},
			},
		)
		gs_ajax = []

		for result in result_set['Items']:
			formatted_result = dynamo_to_dict(result)
			formatted_result[u'utc_timestamp'] = clear_timestamp(formatted_result['export_id'])
			gs_ajax.append(formatted_result)

		return gs_ajax

	def restore(self, state_id):
		res = {'exit_code': 0, 'message': ''}

		aws_session = boto3.session.Session(
			aws_access_key_id=settings.S3_AWS_ACCESS_KEY_ID,
			aws_secret_access_key=settings.S3_AWS_SECRET_ACCESS_KEY,
			region_name='us-west-1'
		)
		dynamo_db = aws_session.client('dynamodb')
		result_set = dynamo_db.query(
			TableName='cm2-tenant-golden-state',
			KeyConditions={
				'tenant_id': {
					'AttributeValueList': [{'N': str(self.tenant.id)}, ],
					'ComparisonOperator': 'EQ'
				},
				'export_id': {
					'AttributeValueList': [{'N': str(state_id)}, ],
					'ComparisonOperator': 'EQ'
				},
			},
			IndexName='export_id-index',
		)
		if int(result_set["Count"]) == 1:
			formatted_result = dynamo_to_dict(result_set['Items'][0])
			# checking backup and target clusters versions match
			if formatted_result['cluster_version'] != self.source_cluster.attribute['cluster_version']:
				res = {'exit_code': 1, 'message': 'Backup version and destination cluster version do not match'}
				return res

			task = self.lock_task(
				self.create_lock(),
				self.create_task(self.user_id, "Download tenant's golden state")
			)
			orig_cluster = None  # Cluster.objects.get(id=formatted_result['cluster_id'])
			subtask = self.create_subtask(task, self.tenant, [self.source_node, ])

			if Tenant.objects.filter(
				cluster__id=self.source_cluster.id,
				id=self.tenant.id
			).count() == 0:
				self.tenant.cluster.add(self.source_cluster)
				self.tenant.save()

			task = download_golden_state.delay(
				subtask_id=subtask.id,
				target_cluster_tmid=self.source_cluster_tmid,
				target_cluster_node=self.salt_source_node,
				orig_cluster_tmid=orig_cluster,  # .tmid,
				orig_cluster_version=formatted_result['cluster_version'],
				tenant_name=self.tenant.name,
				backup_id=state_id
			)
			return res
		else:
			res = {'exit_code': 1, 'message': '{} backups were found with {} id.'.format(result_set["Count"], state_id)}
			return res

	def delete(self, state_id):
		'''Deletes golden state meta information from AWS DynamoDb'''

		res = {'exit_code': 0, 'message': ''}

		aws_session = boto3.session.Session(
			aws_access_key_id=settings.S3_AWS_ACCESS_KEY_ID,
			aws_secret_access_key=settings.S3_AWS_SECRET_ACCESS_KEY,
			region_name='us-west-1'
		)
		dynamo_db = aws_session.client('dynamodb')
		result_set = dynamo_db.query(
			TableName='cm2-tenant-golden-state',
			KeyConditions={
				'tenant_id': {
					'AttributeValueList': [{'N': str(self.tenant.id)}, ],
					'ComparisonOperator': 'EQ'
				},
				'export_id': {
					'AttributeValueList': [{'N': str(state_id)}, ],
					'ComparisonOperator': 'EQ'
				},
			},
			IndexName='export_id-index',
		)
		if int(result_set["Count"]) == 1:
			formatted_result = dynamo_to_dict(result_set['Items'][0])
			dynamo_db.delete_item(
				TableName='cm2-tenant-golden-state',
				Key={
					'tenant_id': {'N': str(formatted_result['tenant_id'])},
					'cluster_version': {'S': str(formatted_result['cluster_version'])}
				}
			)
		else:
			res['exit_code'] = 1
			res['message'] = 'Unable to locate the golden state.'
		return res


class Migrate(LockManager, CeleryMix, ObjectExplorer):

	def __init__(self, tenant_id, source_cluster_id, destination_cluster_id, user_id):

		self.tenant = Tenant.objects.get(id=tenant_id)
		self.user_id = user_id

		self.source_cluster = Cluster.objects.get(id=source_cluster_id)
		self.source_cluster_tmid = self.source_cluster.tmid
		self.source_cluster_version = self.source_cluster.attribute['cluster_version']
		self.source_node = self.get_data_node(self.source_cluster)
		self.salt_source_node = '{}-{}-{}'.format(self.source_cluster.name, self.source_node.tmid, self.source_node.nodeType.lower())

		self.destination_cluster = Cluster.objects.get(id=destination_cluster_id)
		self.destination_cluster_tmid = self.destination_cluster.tmid
		self.destination_node = self.get_data_node(self.destination_cluster)
		self.destination_node_tmid = self.destination_node.tmid
		self.salt_destination_node = '{}-{}-{}'.format(self.destination_cluster.name, self.destination_node.tmid, self.destination_node.nodeType.lower())

	def run_tenant(self):
		res = {'exit_code': 0, 'message': ''}
		if self.source_cluster.attribute['cluster_version'] != self.destination_cluster.attribute['cluster_version']:
			res = {'exit_code': 1, 'message': 'Cluster versions do not match'}
			return res
		# ------------- FULL TENANT's MIGRATION -------------
		task = self.lock_task(
			self.create_lock(),
			self.create_task(self.user_id, 'Full tenant migration')
		)
		subtask = self.create_subtask(task, self.tenant, [self.source_node, self.destination_node])

		# launching asynchronously celery task
		tenant_migrate.delay(
			subtask_id=subtask.id,
			src_cluster_tmid=self.source_cluster_tmid,
			src_cluster_node=self.salt_source_node,
			dst_cluster_tmid=self.destination_cluster_tmid,
			dst_cluster_node=self.salt_destination_node,
			cluster_version=self.source_cluster_version,
			tenant_name=self.tenant.name
		)
		return res

	def run_principals(self):
		res = {'exit_code': 0, 'message': ''}
		if self.source_cluster.attribute['cluster_version'] != self.destination_cluster.attribute['cluster_version']:
			res = {'exit_code': 1, 'message': 'Cluster versions do not match'}
			return res
		# ------------- PRINCIPALS MIGRATION -------------
		task = self.lock_task(
			self.create_lock(),
			self.create_task(self.user_id, "Tenant's principals migration")
		)
		subtask = self.create_subtask(task, self.tenant, [self.source_node, self.destination_node])

		task = principals_migrate.delay(
			subtask_id=subtask.id,
			src_cluster_tmid=self.source_cluster_tmid,
			src_cluster_node=self.salt_source_node,
			dst_cluster_node=self.salt_destination_node,
			cluster_version=self.source_cluster_version,
			tenant_name=self.tenant.name
		)
		return res


class Clone(LockManager, CeleryMix, ObjectExplorer):
	"""
	Clones the tenant.
	"""

	def __init__(self, tenant_id, source_cluster_id, user_id):
		self.tenant = Tenant.objects.get(id=tenant_id)
		self.source_cluster = Cluster.objects.get(id=source_cluster_id)
		self.user_id = user_id

	def clone(self, new_tenant_tmid, new_tenant_name, destination_cluster_id):
		
		source_node = self.get_data_node(self.source_cluster)
		salt_source_node = '{}-{}-{}'.format(self.source_cluster.name, source_node.tmid, source_node.nodeType.lower())

		destination_cluster = Cluster.objects.get(id=destination_cluster_id)
		destination_node = self.get_data_node(destination_cluster)
		salt_destination_node = '{}-{}-{}'.format(destination_cluster.name, destination_node.tmid, destination_node.nodeType.lower())

		existing_tenants = Tenant.objects.filter(name=new_tenant_name).count()
		if existing_tenants > 0:
			return {'exit_code': 1, 'message': 'Tenant with "{}" name already exists'.format(new_tenant_name,)}

		existing_tenants = Tenant.objects.filter(tmid=new_tenant_tmid).count()
		if existing_tenants > 0:
			return {'exit_code': 1, 'message': 'Tenant with "{}" id already exists'.format(new_tenant_tmid,)}

		# creating a Tenant object inside the Django Backend Db
		cloned_tenant = Tenant(
			tmid=new_tenant_tmid,
			name=new_tenant_name,
			attribute=self.tenant.attribute
		)
		cloned_tenant.save()

		# adding new cloned tenant to the destination cluster:
		cloned_tenant.cluster.add(destination_node.cluster)
		cloned_tenant.save()

		# adding groups to the cloned tenant
		cloned_tenant.group.add(*self.tenant.group.all())
		cloned_tenant.save()

		task = self.lock_task(
			self.create_lock(),
			self.create_task(self.user_id, 'Cloning the tenant')
		)
		subtask = self.create_subtask(task, self.tenant, [source_node, destination_node])

		tenant_clone.delay(
			subtask_id=subtask.id,
			src_cluster_tmid=self.source_cluster.tmid,
			dst_cluster_tmid=destination_cluster.tmid,
			src_cluster_node=salt_source_node,
			dst_cluster_node=salt_destination_node,
			cluster_version=self.source_cluster.attribute['cluster_version'],
			old_tenant_name=self.tenant.name,
			new_tenant_tmid=new_tenant_tmid,
			new_tenant_name=new_tenant_name
		)

		return {'exit_code': 0, 'message': ''}
