from django.contrib import admin
from tenant.models import Tenant


class TenantAdmin(admin.ModelAdmin):
	list_display = ('id', 'name')


admin.site.register(Tenant, TenantAdmin)
