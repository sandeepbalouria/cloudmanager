from django.conf import settings
from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView, RedirectView
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
                       #url(r"^$", TemplateView.as_view(template_name="homepage.html"), name="home"),
                       url(r"^$", RedirectView.as_view(url='/accounts/login/'), name='home'),
                       (r'^admin/', include(admin.site.urls)),
                       (r'^accounts/', include('account.urls')),
                       (r'^api/', include('api.urls')),
                       (r'^cm/', include('cloudmanager.urls')),
                       (r'^cm/tenant/', include('tenant.urls')),
					   (r'^cm/logger/', include('logger.urls')),
                       
                       )
if settings.SERVE_MEDIA:
    urlpatterns += staticfiles_urlpatterns()
