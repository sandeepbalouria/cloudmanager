# -*- coding: utf-8 -*-
"""This is a patch that makes interaction with Boto3 DynamoDb easer when you deal with dicts.

This patch should be used for Boto3 but depends on Boto.

Patch removes type definitions from DynamoDb result items and adds type definitions
before adding item into DynamoDb.

* 	use "dict_to_dynamo" to convert Python dict object to DynamoDb JSON object 
	before insertion into DynamoDb;
* 	use "dynamo_to_dict" to convert DynamoDb JSON object to Python dict 
	after retrieving result from DynamoDb.
"""

from boto.dynamodb.types import LossyFloatDynamizer as Dynamizer, get_dynamodb_type


def _get_dynamodb_type(self, attr):
	if isinstance(attr, dict):
		return 'M'
	if isinstance(attr, list):
		return 'L'
	return get_dynamodb_type(attr)


def _encode_m(self, attr):
	result = {}
	for k, v in attr.items():
		result[k] = self.encode(v)
	return result


def _decode_m(self, attr):
	result = {}
	for k, v in attr.items():
		result[k] = self.decode(v)
	return result


def _encode_l(self, attr):
	return [self.encode(v) for v in attr]


def _decode_l(self, attr):
	return [self.decode(v) for v in attr]


Dynamizer._get_dynamodb_type = _get_dynamodb_type
Dynamizer._encode_m = _encode_m
Dynamizer._decode_m = _decode_m
Dynamizer._encode_l = _encode_l
Dynamizer._decode_l = _decode_l


def dict_to_dynamo(item):
	"""
	Converts dict object into DynamoDb JSON format - prepares dict object
	for insertion into DynamoDb.

	:param item: dictionary object
	:output JSON object
	"""

	dyn = Dynamizer()
	result = dyn.encode(item)
	return result["M"]


def dynamo_to_dict(item):
	"""
	Converts DynamoDb JSON object into Python dict object.

	:param item: DynamoDb JSON object
	:output Python dict
	"""

	if isinstance(item, dict):
		dict_item = item

		if len(item) > 1:
			dict_item = {"M": item}
		else:
			if "M" not in dict:
				dict_item = {"M": item}
	else:
		# convert it to a dict with M in front
		dict_item = {"M": item}

	dyn = Dynamizer()
	return dyn.decode(dict_item)
