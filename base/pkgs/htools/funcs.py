import datetime
import random
import time

from pytz import timezone


def clear_timestamp(timestamp_to_clear):
	# if we have timestamp that is mixed with random integer
	# by datetime_to_epoch function
	if len(str(timestamp_to_clear)) > 10:
		return long(str(timestamp_to_clear)[:10])
	return long(timestamp_to_clear)


def epoch_to_datetime(value, dt_zone=None):
	"""
	Converts integer timestamp to datetime object.

	:param value: integer timestamp
	:param dt_zone: time zone to convert to
	"""

	if value:
		ts = clear_timestamp(value)

		d = datetime.datetime.fromtimestamp(ts)
		if dt_zone:
			tz = timezone(dt_zone)
			return tz.fromutc(d.replace(tzinfo=tz))
		return d


def datetime_to_epoch(dt=None, shift=None, make_mix=True):
	if not dt:
		dt = datetime.datetime.utcnow()

	if shift:
		dt = dt - datetime.timedelta(seconds=shift)
	# time.mktime is not thread safety
	try:
		r_now = time.mktime(dt.timetuple())
	except:
		time.sleep(0.10)
		r_now = time.mktime(dt.timetuple())

	if make_mix:
		r = random.randrange(999999)
		return long(r_now * 1000000 + r)
	return long(r_now)


def byte_sizeof_fmt(num, suffix='B'):

	"""
	Returns the file size. Converts bytes to higher notation is applicable
	"""

	for unit in ['', 'Ki', 'Mi', 'Gi', 'Ti', 'Pi', 'Ei', 'Zi']:
		if abs(num) < 1024.0:
			return "%3.1f%s%s" % (num, unit, suffix)
		num /= 1024.0
	return "%.1f%s%s" % (num, 'Yi', suffix)


def time_sizeof_fmt(num_sec):

	"""
	Returns the time length. Converts seconds to higher notation.
	"""

	for unit in ['sec', 'min', 'hr', 'day(s)']:
		if abs(num_sec) < 60.0 or unit == 'day(s)':
			return "%3.1f %s" % (num_sec, unit)
		if unit == 'hr':
			num_sec /= 24.0
		else:
			num_sec /= 60.0
	return "%.1f %s" % (num_sec, unit)
