from functools import wraps

from django.shortcuts import redirect


def staff_only(view_func):
	"""
	Decorator for views that checks that the user is logged in and is a staff
	member, redirecting to the home page if necessary.
	"""
	@wraps(view_func)
	def _checklogin(request, *args, **kwargs):
		if request.user.is_active and request.user.is_staff:
			return view_func(request, *args, **kwargs)
		# redirecting to the home page
		return redirect('/')
	return _checklogin
