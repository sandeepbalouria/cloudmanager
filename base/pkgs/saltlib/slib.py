import os
import yaml
import time
import copy
from cloudmanager.tasks import provision_cluster


_FUNCTION=None
_SALT_CALLER=None
_CLOUD_CLIENT=None

jvmExtraOptions = {
    'jvmExtraOptions_compiler': '-XX:+UseG1GC -XX:MaxPermSize=512m -Xmx1G -Djava.net.preferIPv4Stack=true',
    'jvmExtraOptions_dataio': '-XX:+UseG1GC -XX:MaxPermSize=512m -Xmx1G -Djava.net.preferIPv4Stack=true',
    'jvmExtraOptions_pf': '-XX:+UseG1GC -XX:MaxPermSize=512m -Xmx1G -Djava.net.preferIPv4Stack=true',
    'jvmExtraOptions_rsm': '-XX:+UseG1GC -XX:MaxPermSize=512m -Xmx1G -Djava.net.preferIPv4Stack=true',
    'jvmExtraOptions_grid': '-XX:+UseG1GC -XX:MaxPermSize=512m -Xmx1G -Djava.net.preferIPv4Stack=true -XX:+UseLargePages -DGRIDGAIN_HOME=/opt/proferi/gridgain -DGRIDGAIN_QUIET=false',
    'jvmExtraOptions_tomcat_forge': '-XX:+UseG1GC -XX:MaxPermSize=512m -Xmx1G -Djava.net.preferIPv4Stack=true',
    'jvmExtraOptions_tomcat_web': '-XX:+UseG1GC -XX:MaxPermSize=512m -Xmx1G -Djava.net.preferIPv4Stack=true',
    'jvmExtraOptions_tomcat': '-XX:+UseG1GC -XX:MaxPermSize=512m -Xmx1G -Djava.net.preferIPv4Stack=true',
    
    'loggerLevels_compiler' : { 
        'org_hibernate_compiler': 'WARN',
        'org_proferi_compiler': 'Warn',
        'org_infinispan_compiler': 'WARN',
        'org_gridgain_compiler': 'WARN',
        'com_proferi_platform_gridmanager_gridgain_tcplite_compiler':'WARN',
        'com_proferi_platform_gridmanager_job_DumpJob_compiler': 'WARN',
        'com_tidemark_compiler': 'WARN',
        'com_hazelcast_compiler': 'WARN',
        'root_compiler': 'WARN' 
        },
    
    
    'loggerLevels_dataio' : {
        'org_hibernate_dataio': 'WARN',
        'org_proferi_dataio': 'Warn',
        'org_infinispan_dataio': 'WARN',
        'org_gridgain_dataio': 'WARN',
        'com_proferi_platform_gridmanager_gridgain_tcplite_dataio':'WARN',
        'com_proferi_platform_gridmanager_job_DumpJob_dataio': 'WARN',
        'com_tidemark_dataio': 'WARN',
        'com_hazelcast_dataio': 'WARN',
        'org_linkedin_norbert_dataio': 'WARN',
        'root_dataio': 'WARN'
        },
    
    'loggerLevels_pf' : {
        'org_hibernate_pf': 'WARN',
        'org_proferi_pf': 'Warn',
        'org_infinispan_pf': 'WARN',
        'org_gridgain_pf': 'WARN',
        'com_proferi_platform_gridmanager_gridgain_tcplite_pf':'WARN',
        'com_proferi_platform_gridmanager_job_DumpJob_pf': 'WARN',
        'com_tidemark_pf': 'WARN',
        'root_pf': 'WARN'
        },
    
    'loggerLevels_rsm' : {   
        'org_hibernate_rsm': 'WARN',
        'org_proferi_rsm': 'Warn',
        'org_infinispan_rsm': 'WARN',
        'org_gridgain_rsm': 'WARN',
        'com_proferi_platform_gridmanager_gridgain_tcplite_rsm':'WARN',
        'com_proferi_platform_gridmanager_job_DumpJob_rsm': 'WARN',
        'com_tidemark_rsm': 'WARN',
        'root_rsm': 'WARN'
        },
    
    'loggerLevels_grid' : {
        'org_hibernate_grid': 'WARN',
        'org_proferi_grid': 'Warn',
        'org_infinispan_grid': 'WARN',
        'org_gridgain_grid': 'WARN',
        'com_proferi_platform_gridmanager_gridgain_tcplite_grid':'WARN',
        'com_proferi_platform_gridmanager_job_DumpJob_grid': 'WARN',
        'com_tidemark_grid': 'WARN',
        'root_grid': 'WARN'
        }
}


class TmSaltBase(object):
	def __get_salt_client(self):
		import salt.config
		import salt.loader	
		global _FUNCTION 
		if not _FUNCTION:
			_SALT_CALLER = salt.client.Caller()
			opts = salt.config.minion_config('/etc/salt/minion') 
			_FUNCTION = salt.loader.minion_mods(opts)
		return _FUNCTION

	def salt_call(self,target=None, function=None):
		sclient = self.__get_salt_client()
		return sclient['publish.publish'](target, function) 

	def __get_salt_local_client(self):
		import salt.client
		_SALT_LOCAL_CLIENT = salt.client.LocalClient()
		return _SALT_LOCAL_CLIENT

	def __get_salt_runner_client(self):
		import salt.runner
		opts = salt.config.master_config('/etc/salt/master')
		runner = salt.runner.RunnerClient(opts)
		return runner

	def runner_client(self,func,arg):
		rclient = self.__get_salt_runner_client()
		return rclient.cmd(func,arg)

	def local_client(self, target, function, fun_arg=(),expr_f='glob', fun_kwarg=None):
		sclient = self.__get_salt_local_client()
		return sclient.cmd(target, function, arg=fun_arg, expr_form=expr_f, kwarg=fun_kwarg)

	def local_client_async(self, target, function, fun_arg=(),expr_f='glob', fun_kwarg=None):
		sclient = self.__get_salt_local_client()
		return sclient.cmd_async(target, function, arg=fun_arg, expr_form=expr_f, kwarg=fun_kwarg)    


	def __get_saltcloud_client(self):
		import salt.cloud        
		global _CLOUD_CLIENT    
		if not _CLOUD_CLIENT:
			_CLOUD_CLIENT = salt.cloud.CloudClient('/etc/salt/cloud')
		return _CLOUD_CLIENT

	def cloud_client(self, cluster_map):
		sclient = self.__get_saltcloud_client()
		#sclient.map_run(cluster_map,hard="False")
		sclient.map_run(cluster_map)
		os._exit(0)


class SingleNode(TmSaltBase):
	__cluster_map = {}

	def __init__(self,cluster_name,sn_tmid,cluster_tmid):
		self.__cluster_name = cluster_name
		self.__cluster_id = cluster_tmid
		self.__node_id = sn_tmid

	def create_cluster(self,tm_version):        
		local_jvm = copy.deepcopy(jvmExtraOptions)
		grains = {
		    'clusterType': 'SingleNode',
		    'cluster_id': str(self.__cluster_id),
		    'role': 'singlenode',
		    'operational_environment': str(self.__cluster_name),
		    'sn_minion': str(self.__cluster_name)+"-"+str(self.__node_id) +'-sn',
		    'data_node': True,
		    'firstrun': 'True',
		    'tm_version': str(tm_version),
		    'cm2_url': "http://cm2.usw1.aws.tidemark.net"
		    }
		grains.update(local_jvm)
		
		self.__cluster_map = {
			'singlenode': {
				str(self.__cluster_name)+"-"+ str(self.__node_id) +'-sn': {
				    'minion':{
				        'environment': 'nxtgen',
				        },                    
				    'grains': grains
				}            
			}        
		}
		with open('/etc/salt/map_files/' + self.__cluster_name +"-"+ str(self.__cluster_id), 'w') as mapfile:
			mapfile.write(yaml.dump(self.__cluster_map))

		provision_cluster.delay('/etc/salt/map_files/' + self.__cluster_name +"-"+ str(self.__cluster_id))


class MultiNode(TmSaltBase):
	__cluster_map = {}

	def __init__(self,cluster_name,web_tmid,forge_tmid,data_tmid,cluster_tmid):
		self.__cluster_name = cluster_name
		self.__cluster_id = cluster_tmid
		self.__web_id = web_tmid
		self.__forge_id = forge_tmid
		self.__data_id = data_tmid

	def create_cluster(self,tm_version):        
		local_jvm = copy.deepcopy(jvmExtraOptions)
		grains = {
		    'clusterType': 'MultiNode',
		    'cluster_id': str(self.__cluster_id),
		    'operational_environment': str(self.__cluster_name),
		    'firstrun': 'True',
		    'web_minion': str(self.__cluster_name)+"-"+str(self.__web_id)+'-wn',
		    'forge_minion': str(self.__cluster_name)+"-"+str(self.__forge_id)+'-fn',
		    'data_minion': str(self.__cluster_name)+"-"+str(self.__data_id)+'-dn',
		    'tm_version': str(tm_version),
		    'cm2_url': "http://cm2.usw1.aws.tidemark.net"
		}
		grains.update(local_jvm)
		web_grains = {'role':'web'}
		web_grains.update(grains)
		forge_grains = {'role':'forge'}
		forge_grains.update(grains)
		data_grains = {'role':'data'}
		data_grains.update(grains)
		self.__cluster_map = {
			'web': [{
				str(self.__cluster_name)+ "-" + str(self.__web_id)+'-wn': {
				    'minion':{
				        'environment': 'nxtgen',
				        },
		            'role': 'web',
				    'grains': web_grains
				}    
				}],
			'forge': [{
				str(self.__cluster_name)+ "-" + str(self.__forge_id)+'-fn': {
				    'minion':{
				        'environment': 'nxtgen',
				        },
		            'role': 'forge',
				    'grains': forge_grains
				    }
				     
				}],
			'data': [{
				str(self.__cluster_name)+"-"+str(self.__data_id)+'-dn': {
				    'minion':{
				        'environment': 'nxtgen',
				        },
		            'role': 'data',
				    'grains': data_grains
				    }                    
				                
			}]                    
		}
		with open('/etc/salt/map_files/' + self.__cluster_name +"-"+ str(self.__cluster_id), 'w') as mapfile:
			mapfile.write(yaml.dump(self.__cluster_map))

		provision_cluster.delay('/etc/salt/map_files/' + self.__cluster_name +"-"+ str(self.__cluster_id))


class ClusterData(TmSaltBase):



	def get_Cluster_Service_Status(self,cluster_id,service=None):

		status = {}
		if service:
			jid = self.local_client_async("cluster_id:" +str(cluster_id), 'tidemark.status',fun_arg=[service],expr_f='grain')
			service_status = self.runner_client('jobs.lookup_jid', [jid])
			for key in service_status:
				for k,v in service_status[key].iteritems():
					status[k] = v				
			return status                        
		else:
			jid = self.local_client_async("cluster_id:" +str(cluster_id), 'tidemark.status',expr_f='grain')
			time.sleep(1)
			service_status = self.runner_client('jobs.lookup_jid', [jid])
			for key in service_status:
				for k,v in service_status[key].iteritems():
					status[k] = v
			return status

	   
	def get_System_Service_Status(self,cluster_id,service=None):

		status = {}
		if service:
			jid = self.local_client_async("cluster_id:" +str(cluster_id), 'tidemark.SystemServiceAction',fun_arg=[service],expr_f='grain')
			service_status = self.runner_client('jobs.lookup_jid', [jid])			
			for key in service_status:
				for k,v in service_status[key].iteritems():
					status[k] = v				
				return status                        
		else:
			jid = self.local_client_async("cluster_id:" +str(cluster_id), 'tidemark.SystemServiceAction',expr_f='grain')
			service_status = self.runner_client('jobs.lookup_jid', [jid])			
			for key in service_status:
				for k,v in service_status[key].iteritems():
					status[k] = v				
			return status         

	def get_Firstrun_Status(self,cluster_id):

		status = {'ready':'no'}
		jid = self.local_client_async("cluster_id:" +str(cluster_id), 'grains.item',fun_arg=['firstrun'],expr_f='grain')
		firstrun = self.runner_client('jobs.lookup_jid', [jid])
		if firstrun:
			for key in firstrun:
				if firstrun[key]['firstrun'] == 'NotAnyMore':
					status['ready'] = 'yes'

				else:
					status['ready'] = 'no' 
					break
		return status

	def cluster_Service_Action(self,cluster_id,service,action):
		status = {}
		if service == 'all':
			self.local_client_async("cluster_id:" +str(cluster_id), 'tidemark.'+str(action),expr_f='grain')
			time.sleep(15)
			status = self.get_Cluster_Service_Status(cluster_id)
			return status
		else:
			self.local_client_async("cluster_id:" +str(cluster_id), 'tidemark.'+str(action),fun_arg=[service],expr_f='grain')			
			status = self.get_Cluster_Service_Status(cluster_id,service=service)
		return status

	def system_Service_Action(self,cluster_id,service,action):
		status = {}
		if service == 'all':
			service_status = self.local_client("cluster_id:" +str(cluster_id), 'tidemark.SystemServiceAction',fun_arg=['all',action],expr_f='grain')
		else:
			self.local_client_async("cluster_id:" +str(cluster_id), 'tidemark.SystemServiceAction',fun_arg=[service,action],expr_f='grain')
			time.sleep(10)
			service_status = self.get_System_Service_Status(cluster_id,service=service)
			status['status'] = service_status[str(service)]
		return status
	
	def platform_deploy(self,cluster_id,tm_version):
		'''
		sends deploy command async and sets a grain deloy_jid on the cluster
		'''
		deploy_jid = self.local_client_async("cluster_id:" +str(cluster_id), 'tidemark.deploy', fun_arg=[tm_version],expr_f='grain')
		self.local_client_async("cluster_id:" +str(cluster_id), 'grains.setval', fun_arg=['deploy_jid', deploy_jid],expr_f='grain')        
	
	def jvmExtraOptions_action(self,cluster_id,service,jvmoptions=None):
		jvmExtraOption = {}
		if jvmoptions:
			jid = self.local_client_async("cluster_id:" +str(cluster_id), 'grains.setval',fun_arg=['jvmExtraOptions_'+str(service),str(jvmoptions)],expr_f='grain')
			time.sleep(1)
			self.local_client_async("cluster_id:" +str(cluster_id), 'state.highstate',expr_f='grain')			
		else:
			jid = self.local_client_async("cluster_id:" +str(cluster_id), 'grains.item',fun_arg=['jvmExtraOptions_'+str(service)],expr_f='grain')
			time.sleep(4)
			jvmOptions = self.runner_client('jobs.lookup_jid', [jid])
			for key in jvmOptions:
				for k,v in jvmOptions[key].iteritems():
					jvmExtraOption['options'] = v			
			return jvmExtraOption
	
	def loggerLevels_action(self,cluster_id,service,loggerLevels=None):
			loggerL = {}
			if loggerLevels:
				jid = self.local_client_async("cluster_id:" +str(cluster_id), 'grains.setval',fun_arg=['loggerLevels_'+str(service),dict(loggerLevels)],expr_f='grain')
				time.sleep(1)
				self.local_client_async("cluster_id:" +str(cluster_id), 'state.highstate',expr_f='grain')						
			else:
				jid = self.local_client_async("cluster_id:" +str(cluster_id), 'grains.item',fun_arg=['loggerLevels_'+str(service)],expr_f='grain')
				time.sleep(4)
				levels = self.runner_client('jobs.lookup_jid', [jid])
				for key in levels:
					for k,v in levels[key].iteritems():
						loggerL['levels'] = v			
				return loggerL	

