# Django settings for base project.
import os, sys
from os.path import join
import djcelery
djcelery.setup_loader()

SITE_NAME = 'ww'
BROKER_URL = 'amqp://guest:guest@localhost:5672//'
CELERY_CHORD_PROPAGATES = True
# django result backend
# CELERY_RESULT_BACKEND = 'djcelery.backends.database:DatabaseBackend'

# redis result backend
CELERY_RESULT_BACKEND = 'redis://localhost:6379'

# rabbitMQ message result backend
# CELERY_RESULT_BACKEND = 'amqp'
# CELERY_TASK_RESULT_EXPIRES = 18000  # 5 hours.

# mongodb result backend
# CELERY_RESULT_BACKEND = 'mongodb://localhost:27017/'
# CELERY_MONGODB_BACKEND_SETTINGS = {
#     'database': 'mydb',
#     'taskmeta_collection': 'my_taskmeta_collection',
# }

# CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'


from datetime import timedelta
CELERYBEAT_SCHEDULE = {
    # Executes every 10 sec
    'check-tenant-lock': {
        'task': 'tenant.tasks.check_locks',
        'schedule': timedelta(seconds=10),
    },
}


DEBUG = True
TEMPLATE_DEBUG = DEBUG

PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
BASE_DIR = os.path.abspath(os.path.dirname(__file__)) + os.sep

sys.path.insert(0, join(PROJECT_ROOT, "apps"))
sys.path.insert(0, join(PROJECT_ROOT, "pkgs"))

AUTH_PROFILE_MODULE = 'profiles.Profile'

ADMINS = (
     ('Lecole Cole', 'lcole@tidemark.com'),
     ('Daniel Gollas', 'dgollas@tidemark.com'),
)

MANAGERS = ADMINS

isProduction = False
isDevelopment = False
isProduction = os.path.isfile("/home/ubuntu/web/cm2/cm2_prod")
isDevelopment = os.path.isfile("/home/ubuntu/web/cm2/cm2_dev")


if isProduction:
    print isProduction
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'cloudmanager_prod',
            'USER': 'fmHeyfuph976XhrT',
            'PASSWORD': 'vnrrL5jzXAuVXDsx',
            'HOST': 'cloud-manager-db.cxvtt4ihxnts.us-west-1.rds.amazonaws.com',
            'PORT': '5432',
        }
    }
elif isDevelopment:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'cloudmanager',
            'USER': '2cNWpwfxn4JY4yRE',
            'PASSWORD': 'aJ_6gHaq9@#CC9d8',
            'HOST': 'cloud-manager-db.cxvtt4ihxnts.us-west-1.rds.amazonaws.com',
            'PORT': '5432',
        }
    }

else:
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql_psycopg2',
            'NAME': 'cloudmanager',
            'USER': 'postgres',
            'PASSWORD': '123',
            'HOST': '127.0.0.1',
            'PORT': '5432',
        }
    }

#workaround for debugtoolbar  path setting
try:
    if sys.argv[1] == 'runserver' or sys.argv[1] == 'runserver_plus':
        DEBUG_TOOLBAR_PATCH_SETTINGS = DEBUG
    else:
        DEBUG_TOOLBAR_PATCH_SETTINGS = False
except IndexError:
        DEBUG_TOOLBAR_PATCH_SETTINGS = False


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# In a Windows environment this must be set to your system time zone.
TIME_ZONE = 'UTC'  # 'America/Los_Angeles'

# Hosts/domain names that are valid for this site.
# "*" matches anything, ".example.com" matches example.com and all subdomains
ALLOWED_HOSTS = ["*"]

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

# tells Django to serve media through the staticfiles app.
SERVE_MEDIA = DEBUG

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale.
USE_L10N = True

# If you set this to False, Django will not use timezone-aware datetimes.
USE_TZ = True

# Absolute filesystem path to the directory that will hold user-uploaded files.
# Example: "/home/media/media.lawrence.com/media/"
MEDIA_ROOT = os.path.join(PROJECT_ROOT, "site_media", "media")

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash.
# Examples: "http://media.lawrence.com/media/", "http://example.com/media/"
MEDIA_URL = "/site_media/media/"

# Absolute path to the directory static files should be collected to.
# Don't put anything in this directory yourself; store your static files
# in apps' "static/" subdirectories and in STATICFILES_DIRS.
# Example: "/home/media/media.lawrence.com/static/"

STATIC_ROOT = os.path.join(PROJECT_ROOT, "static")

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = "/site_media/static/"

# Additional locations of static files
STATICFILES_DIRS = (
    os.path.join(PROJECT_ROOT, "site_media", "static"),
)

# List of finder classes that know how to find static files in
# various locations.
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder'
)


# Make this unique, and don't share it with anybody.
SECRET_KEY = 'l-q&amp;=v5vog5@oupk2ytp5prqbpdy$p3c3l*vzggohyq-ymt84*'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader'
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth', 
    'django.core.context_processors.debug', 
    'django.core.context_processors.i18n', 
    'django.core.context_processors.media', 
    'django.core.context_processors.static', 
    'django.core.context_processors.tz', 
    'django.contrib.messages.context_processors.messages',
    'django.core.context_processors.request'
)

INTERNAL_IPS = ('127.0.0.1',)

MIDDLEWARE_CLASSES = (
    'htmlmin.middleware.HtmlMinifyMiddleware',    
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',

    # Uncomment the next line for simple clickjacking protection:
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

# For Debug Tool Bar
DEBUG_TOOLBAR_CONFIG = {
    'INTERCEPT_REDIRECTS' : False
}

HTML_MINIFY = False

ROOT_URLCONF = 'base.urls'

# Python dotted path to the WSGI application used by Django's runserver.
WSGI_APPLICATION = 'base.wsgi.application'

TEMPLATE_DIRS = (
    os.path.join(PROJECT_ROOT, "templates"),
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

CRISPY_TEMPLATE_PACK = 'bootstrap3'
CRISPY_FAIL_SILENTLY = not DEBUG

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.humanize',
    
    'compressor',
    'debug_toolbar',
    'django_forms_bootstrap',
    'timezones',
    'announcements',
    'pagination',
    'djcelery',
	'rest_framework',
    #Project
    'api',
    'account',
    'cloudmanager',
    'tenant',
    'templatetags',
	'south'
)

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
	'formatters': {
	        'default': {
	            'format': '[%(asctime)s] %(levelname)s %(name)s (line #%(lineno)d) : %(message)s',
	            'datefmt':'%d/%b/%Y %H:%M:%S'
	        },
	    },
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        },
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'console':{
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'default',
			'filters': ['require_debug_true']
        },
		'file_debug': {
			'level': 'DEBUG',
			'class': 'logging.FileHandler',
			'formatter': 'default',
			'filename': '/var/log/django/debug.log',
			
		},
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins', 'file_debug'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'cloudmanager': {
			'level': 'DEBUG',
            'handlers': ['file_debug', 'console', ],
			'propagate': True,
        },
        'api': {
            'handlers': ['console'],
            'level': 'DEBUG',

        }
    }
}


EMAIL_BACKEND = 'django_ses.SESBackend'

DEFAULT_FROM_EMAIL = 'mailer@tidemark.com'
AWS_SES_REGION_NAME = "us-west-2"
AWS_SES_REGION_ENDPOINT = 'email.us-west-2.amazonaws.com'
SERVER_EMAIL = 'mailer@tidemark.com'

ACCOUNT_USER_DISPLAY = lambda user: user.email
ACCOUNT_EMAIL_UNIQUE = True
ACCOUNT_EMAIL_CONFIRMATION_REQUIRED = True
ACCOUNT_USER_DISPLAY = lambda user: user.email

AUTHENTICATION_BACKENDS = (
    'tokenapi.backends.TokenBackend',
    'account.auth_backends.EmailAuthenticationBackend',
    )

ACCOUNT_LOGIN_REDIRECT_URL = 'cm_dashboard'


#Creds for CloudManagerSendEmail

AWS_ACCESS_KEY = 'AKIAJ3AWQPJYGNMIWXSA'
AWS_SECRET_ACCESS_KEY = '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'

AWS_ACCESS_KEY_ID = 'AKIAJ3AWQPJYGNMIWXSA'


#AWS_ACCESS_KEY = 'AKIAIHZR7J3ZQCNFU5CQ'
#AWS_SECRET_ACCESS_KEY = 'AgWVfKXcmFSmuJmnskpDZMMuZNV7+xwHV+HLLIx8wWkU'

#AWS_ACCESS_KEY_ID = 'AKIAIHZR7J3ZQCNFU5CQ'


# AWS S3 settings for backup, migration and golden state
S3_AWS_ACCESS_KEY_ID = "AKIAIFDYI4TZ4JERPBOA"
S3_AWS_SECRET_ACCESS_KEY = "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"

AWS_S3_BUCKET = 'migration-dev'

AWS_S3_BACKUP = 'TmBackup'
AWS_S3_MIGRATION = 'TmMigrate'
AWS_S3_GOLDEN_STATE = 'TmGoldenState'

########TM_ LOGGER########
#TODO: SWITCH THESE OUT TO A LIMITED IAM's KEYS!
LOGGER_AWS_ACCESS_KEY_ID = "AKIAITW7OQCAQMBBLHXA"
LOGGER_AWS_SECRET_ACCESS_KEY = "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"
LOGGER_AWS_REGION = "us-west-2"

LOGGER_MAX_TRIES = 50
LOGGER_MAX_CAPACITY_UNITS_PER_REQUEST = 16*10
LOGGER_MAX_REQUESTED_LIMIT = 100
LOGGER_MAX_UNFILTERED_ENTRIES = 100
LOGGER_TABLE_NAME = "tm_log_02"
LOGGER_SNIPPET_TABLE_NAME = "tm_log_snippets_01"
LOGGER_SNIPPET_EXPIRATION_TIME = 60 * 24 * 15 #expiration time in seconds
#########################

DEFAULT_TENANT_LOGO='img/landing_brand-development_tidemark.jpg'

