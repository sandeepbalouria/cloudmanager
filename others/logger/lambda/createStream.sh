#!/bin/sh

STREAMNAME=$1
SHARDCOUNT=$2

echo "Creating stream "$STREAMNAME" with "$SHARDCOUNT" shards"
aws kinesis create-stream --stream-name $STREAMNAME --shard-count $SHARDCOUNT