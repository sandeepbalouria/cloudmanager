#!/bin/sh

STREAMNAME=$1
TABLENAME=$2
WAITTIME=10s
echo "Deleting stream "$STREAMNAME" and table "$TABLENAME
#delete stream
sh deleteStream.sh $STREAMNAME
#delete table
sh deleteLogTable.sh $TABLENAME
#sleep 10 seconds
echo "Waiting "$WAITTIME
sleep $WAITTIME
echo "Creating stream "$STREAMNAME" and table "$TABLENAME
#create stream
sh createStream.sh $STREAMNAME 1
#create table
sh createLogTable.sh $TABLENAME

