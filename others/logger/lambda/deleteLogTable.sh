#!/bin/sh

TABLENAME=$1
echo "Deleting table "$TABLENAME
aws dynamodb delete-table --table-name $TABLENAME