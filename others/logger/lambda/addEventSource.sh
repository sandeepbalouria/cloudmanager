#!/bin/sh
echo "HELLO"

STREAMNAME=$1
LAMBDANAME=$2
BATCHSIZE=$3

echo "Linking stream->function "$STREAMNAME"-->"$LAMBDANAME" batch "$BATCHSIZE
aws lambda add-event-source --role arn:aws:iam::338535913256:role/tm_logger_lambda_invocation_role --event-source arn:aws:kinesis:us-west-2:338535913256:stream/$STREAMNAME --batch-size $BATCHSIZE --parameters InitialPositionInStream=LATEST --function-name $LAMBDANAME
echo "Done"



#aws lambda update-function-configuration --function-name tmLoggerToDynamo --handler tm_logger_handler