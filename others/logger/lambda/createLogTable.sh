#!/bin/sh
TABLENAME=$1
echo "Creating table "$TABLENAME
aws dynamodb create-table --table-name $TABLENAME --cli-input-json file://tm_logger_dynamodb.json