/*
 SAMPLE DATA
 {
 "Records": [
 {
 "kinesis": {
 "partitionKey": "partitionKey-3",
 "kinesisSchemaVersion": "1.0",
 "data": " ew0KICAgICJhbGwiOiAidGhpcyBpcyB0aGUgbWVzc2FnZSIsDQogICAgIl9ob3N0IjogImRldm9wczAxLXNpbmdsZS51c3cxLmF3cy50aWRlbWFyay5uZXQiLA0KICAgICJfdGltZSI6ICIyMDE1LTAxLTA4IDIwOjEyOjQwICswMDAwIiwNCiAgICAiX2NvbXAiOiAiY29tcGlsZXJfMV9tYWluIiwNCiAgICAibGV2ZWwiOiAiaW5mbyIsDQogICAgIl9jbHVzdGVyaGFzaCI6ICJiNGIwZjE5Yi0yZTVhLTRmN2UtYjczNy1lOWMwYWE3YzAyZmFfY29tcGlsZXJfMV9tYWluIiwNCiAgICAidGltZSI6ICIyMDE1LTAxLTA4VDIwOiAxMjogNDBaIiwNCiAgICAidGFnIjogImtpbmVzaXMuY29tcGlsZXJfMV9tYWluIg0KfQ==",


 "sequenceNumber": "49545115243490985018280067714973144582180062593244200961"
 },
 "eventSource": "aws:kinesis",
 "eventID": "shardId-000000000000:49545115243490985018280067714973144582180062593244200961",
 "invokeIdentityArn": "arn:aws:iam::059493405231:role/testLEBRole",
 "eventVersion": "1.0",
 "eventName": "aws:kinesis:record",
 "eventSourceARN": "arn:aws:kinesis:us-east-1:35667example:stream/examplestream",
 "awsRegion": "us-east-1"
 }
 ]
 }


 THIS IS THE CONTENT OF THE DATA FIELD
 {
    "all": "this is the message",
    "_host": "devops01-single.usw1.aws.tidemark.net",
    "_time": "2015-01-08 20:12:40 +0000",
    "_comp": "compiler_1_main",
    "level": "info",
    "_clusterhash": "b4b0f19b-2e5a-4f7e-b737-e9c0aa7c02fa_compiler_1_main",
    "time": "2015-01-08T20: 12: 40Z",
    "tag": "kinesis.compiler_1_main"
}
 */


//https://www.npmjs.com/package/dynamo-client
var dynamo = require('dynamo-client');
var region = "us-west-2";
var accessKeyId='AKIAITW7OQCAQMBBLHXA';
var secretAccessKey = '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';
var ddb = dynamo.createClient(
                                region,
                                {
                                    accessKeyId: accessKeyId ,
                                    secretAccessKey : secretAccessKey
                                }
                             );

//Used to convert js objects into dynamodb formatted attribute maps
//https://github.com/kayomarz/dynamodb-data-types
var attrWrapper = require('dynamodb-data-types').AttributeValue;

var moment = require("moment");
//var TABLENAME = "tm_test_01";
var TABLENAME = "tm_log_02";

var processedHosts = {};
var batches = [];
var currentBatch = [];
var totalCallbacks = 0;
var highCallbacks = 0;
var totalConsumedCapacity = {};
var _globalContext = null;
var oldestInRun = {"data": null, "timestamp": "99999999999999999999999999999999999"};
var newestInRun = {"data": null, "timestamp": ""};

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}

function levelToNumber(level){
    /*
    * ERROR 0
    * WARN 1
    * INFO 2
    * DEBUG 3
    * TRACE 4
    * */

    var levelToNumberMap;
    levelToNumberMap = {
        "error": 0,
        "err": 0,

        "warn": 1,
        "warning": 1,

        "info": 2,
        "information": 2,

        "debug": 3,

        "trace": 4
    };

    var sanitizedLevel=level.trim().toLowerCase();
    if(sanitizedLevel in levelToNumberMap){
        return levelToNumberMap[sanitizedLevel];
    }
    else{
        return 5;
    }

}

function randomizeTimestamp(pTime) {
    //Timestamps can be in epoch (123456.1232456) or some string format
    //console.log(pTime);
    pTime = pTime.replace(",","."); //sometimes time comes with a , separating the milliseconds

    //sometimes the time comes in this format 24/Apr/2015:20:22:12 +0000, get rid of the first colon if this happens.
    colons = pTime.match(/(:)/g);
    if(colons.length > 2){
        //console.log("replacing on "+pTime);
        pTime = pTime.replace(/:/," ");
    }

    var tsEpoch = null

    if (!isNumber(tsEpoch)) {
        tsEpoch = moment(pTime).format("x");
    }
    else {
        tsEpoch = String(tsEpoch)
    }

    //multiply by 1,000,000 and add 6 random digits to the millisecond epoch.
    //we store a very long long so Dynamo won't work with floats but long ints.
    //var theRandom = Math.floor(Math.random()*1000000)
    var theRandom =(""+Math.random()).substring(2,8)
    var finalTimestamp = (tsEpoch)+String(theRandom)
    //console.log(finalTimestamp);
    return finalTimestamp;
}

function prepareRequestItems(tableName, items){
    var requestItems = {}

    var putRequests = []

    for(var i=0; i<items.length; i++){
      var tmpPutRequest = {"PutRequest": {"Item": {} } }
        var theItem = items[i];
        //console.log("PreWrapped:\n:"+JSON.stringify(theItem));
        var dynamizedItem = attrWrapper.wrap(theItem);
        //console.log("PostWrapped:\n:"+JSON.stringify(dynamizedItem));

        tmpPutRequest.PutRequest.Item = dynamizedItem;
        putRequests.push(tmpPutRequest);
    }


    requestItems[tableName] = putRequests;
    return requestItems;
}

function batchWriteEntries(client, requestItems, cb, batchInfo){

    //delay will make msDelay grow exponentially, but on the first run (delay==0) it should be executed without delay.

    var msDelay = 0;
    if(batchInfo.delay > -1){
        msDelay = Math.pow(2, batchInfo.delay) * 1000;
    }

    //console.log("Batch id: "+String(batchInfo.id)+" delayed "+String(batchInfo.delay)+" ("+String(msDelay)+"ms)");
    totalCallbacks += 1;
    highCallbacks += 1;

    var requestData = {
        "RequestItems": requestItems,
        "ReturnConsumedCapacity": "TOTAL",
        "ReturnItemCollectionMetrics": "NONE"
    }

    //console.log("Final requestData:\n:"+JSON.stringify(requestData));
    setTimeout(
        function(){
                ddb.request("BatchWriteItem", requestData, function(err, data){
                    //wrap our own callback in the ddb callback to send our own data for next run.
                    cb(err, data, batchInfo);
                });
            },
            msDelay
    );

}

function kinesisRecordToDynamoRecord(theRecord){

    var result = null;

    // Kinesis data is base64 encoded so decode here
    var encodedPayload = theRecord.kinesis.data;
    var payload = new Buffer(encodedPayload, 'base64').toString('ascii');
    //data may be twice b64 encoded (fluent-kinesis plugin bug), try to parse json, or decode and try to parse again.
    var parsedData = null
    try {
        parsedData = JSON.parse(payload);
    }
    catch (e) {
        //console.log("JSON parse failed, trying to decode again")
        payload = new Buffer(payload, 'base64').toString('ascii');
        parsedData = JSON.parse(payload);
    }

    var _clusterhash = parsedData["_clusterhash"] || "NO_CLUSTERHASH";

    if (_clusterhash == "NO_CLUSTERHASH") {
        console.log("no clusterhash, skipping entry");
        return null
    }

    var _host = parsedData["_host"] || "NO_HOST_IN_RECORD";
    var _level = levelToNumber( parsedData["level"] || "NO_LEVEL" );
    var _comp = parsedData["_comp"] || "NO_COMPONENT";
    var _time = parsedData["_time"];
    var _payload = parsedData || "no payload";

    //remove redundant fields from _payload
    delete _payload["_host"];
    delete _payload["_level"];
    delete _payload["_comp"];
    //delete _payload["_time"];
    delete _payload["_clusterhash"];
    delete _payload["time"];

    var finalTimestamp = null

    try {
        finalTimestamp = randomizeTimestamp(_time);

    if(finalTimestamp.indexOf("Invalid") > -1){
        console.log("{'INVALID_RECORD': {'reason': 'invalid timestamp', 'data': "+JSON.stringify(parsedData));
        return null
    }

    result = {
        clusterhash: _clusterhash,
        host: _host,
        timestamp: finalTimestamp,
        level: _level,
        component: _comp,
        payload: _payload
    };

        //keep track of oldes and newest records for debugging purposes in the logs.
        if(result.timestamp < oldestInRun.timestamp){
            oldestInRun = result;
        }
        if(result.timestamp > newestInRun.timestamp){
            newestInRun = result;
        }
    }
    catch(err) {
      console.log("Ignoring invalid record due to exception: ")
      console.log(err.message);
      console.log(parsedData);
        return null;
    }
    return result;
}

function accountForLogs(record){
    var _clusterhash_host = record.clusterhash+"__"+record.host;
    if (processedHosts.hasOwnProperty(_clusterhash_host)) {
        processedHosts[_clusterhash_host] = processedHosts[_clusterhash_host] + 1;
    }
    else {
        processedHosts[_clusterhash_host] = 1;
    }
}

function size_dict(d){c=0; for (i in d) ++c; return c;}

function secondsToTimecode(sec_num) {
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = hours+':'+minutes+':'+seconds;
    return time;
}

var batchWriteCallback = function (err, data, batchInfo) {
    if (err) {
        console.log(err, err.stack);
    } else {
        //TODO: CHECK FOR UNPROCESSED ITEMS

        var unprocessedItems = data.UnprocessedItems;
        var unprocessedLength = (unprocessedItems[TABLENAME] || []).length;
        if(unprocessedLength > 0){
            batchInfo.delay += 1;
            console.log("Batch id:"+String(batchInfo.id)+": Reprocessing "+String(unprocessedLength)+" items with delay "+String(batchInfo.delay)+" --- "+JSON.stringify(data));
            batchWriteEntries(ddb, unprocessedItems, batchWriteCallback, batchInfo);
        }

        var consumedCapacity = data.ConsumedCapacity
        var tmpCapacity = null;
        for(var i=0; i<consumedCapacity.length; i++){
            tmpCapacity = consumedCapacity[i];
            if(totalConsumedCapacity.hasOwnProperty(tmpCapacity.TableName)){
                totalConsumedCapacity[tmpCapacity.TableName]+=tmpCapacity.CapacityUnits;
            }
            else{
                totalConsumedCapacity[tmpCapacity.TableName]=tmpCapacity.CapacityUnits;
            }
        }
        //console.log("Successful batch write!: "+JSON.stringify(data.CapacityUnits));
        //console.log("data!: "+JSON.stringify(data));
        //console.log("Unprocessed Items: "+JSON.stringify(unprocessedItems));
        //console.log("Unprocessed Items Length: "+String(unprocessedLength));
    }

    totalCallbacks = totalCallbacks - 1;

    if (totalCallbacks <= 0) {
        //console.log("totalCallbacks left: "+totalCallbacks.toString()+"/"+highCallbacks.toString());

        var newest_timestamp_in_s = newestInRun.timestamp.substring(0, 10);
        var newest_backlog_delta = moment().unix()-parseInt(newest_timestamp_in_s);

        var oldest_timestamp_in_s = oldestInRun.timestamp.substring(0, 10);
        var oldest_backlog_delta = moment().unix()-parseInt(oldest_timestamp_in_s);

        var deltas ={};
        deltas["NEWEST_BACKLOG_DELTA"]=secondsToTimecode(newest_backlog_delta);
        deltas["OLDEST_BACKLOG_DELTA"]=secondsToTimecode(oldest_backlog_delta);
        final_results = {"BATCHES_STATUS":"COMPLETED","TOTALS":{}, "PROCESSED_HOSTS":{}};
        final_results.TOTALS["batch_count"] = highCallbacks.toString();
        final_results.TOTALS["consumed_capacity"] = JSON.stringify(totalConsumedCapacity);
        final_results.PROCESSED_HOSTS=processedHosts
        final_results["OLDEST_RECORD"]=oldestInRun;
        final_results["NEWEST_RECORD"]=newestInRun;
        final_results["NEWEST_BACKLOG_DELTA"]=secondsToTimecode(newest_backlog_delta);
        final_results["OLDEST_BACKLOG_DELTA"]=secondsToTimecode(oldest_backlog_delta);
        final_results["BACKLOG_NOW"]=moment().unix();
        console.log(final_results)
        console.log(deltas)
        //console.log("TOTAL BATCHES PROCESSED: " + highCallbacks.toString()+ "(Consumed capacity: "+JSON.stringify(totalConsumedCapacity)+")");
        //console.log(processedHosts);
        _globalContext.done(null, "We are done");
    }
}

exports.tm_logger_handler = function (event, context) {

    _globalContext = context
    console.log("PROCESSING " + event.Records.length.toString() + " RECORD(S)");

    var currentBatchSize = 0;
    var ITEMS_PER_BATCH = 25;

    oldestInRun = {"data": null, "timestamp": "99999999999999999999999999999999999"};
    newestInRun = {"data": null, "timestamp": ""};

    //we can write a maximum of 16mb per batch write
    var MAX_BATCH_SIZE = 16 * 1024 * 1024
    batches = [];
    currentBatch = [];
    totalCallbacks = 0;
    highCallbacks = 0;
    processedHosts = {};

    for (i = 0; i < event.Records.length; ++i) {


        var record = kinesisRecordToDynamoRecord(event.Records[i])
        if(record == null){
            continue;
        }
        accountForLogs(record)

        var newRecordSize = (JSON.stringify(record)).length

        //If current batch of records is within size, keep adding...
        if (currentBatchSize + newRecordSize < MAX_BATCH_SIZE && currentBatch.length < ITEMS_PER_BATCH) {
            currentBatch.push(record)
            currentBatchSize += newRecordSize;
        }
        else {
            //...otherwise, start a new batch and push the previous one
            batches.push(currentBatch);
            currentBatchSize = newRecordSize;
            currentBatch = [];
            currentBatch.push(record);
        }
    }

    if(currentBatch.length > 0)
    {
        //push the last batch in before shipping them
        batches.push(currentBatch);
    }

    for(i=0; i< batches.length; i++){
        //Write each batch at a time. Once all callbacks have been called,
        // the lambda will notify the context it finished and the invocation will be done.
        var requestItems = prepareRequestItems(TABLENAME,batches[i])
        batchWriteEntries(ddb, requestItems, batchWriteCallback, {"id":i, "delay":-1});
    }

};


