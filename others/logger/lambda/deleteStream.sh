#!/bin/sh

STREAMNAME=$1
SHARDCOUNT=$2

echo "Deleting stream "$STREAMNAME
aws kinesis delete-stream --stream-name $STREAMNAME
