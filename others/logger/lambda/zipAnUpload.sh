#!/bin/sh
echo "HELLO"
LAMBDANAME=$1
echo "Compressing..."
zip -r archive.zip index.js node_modules
echo "Done compressing, uploading "$LAMBDANAME
aws lambda upload-function --function-name $LAMBDANAME --function-zip archive.zip --runtime nodejs --role arn:aws:iam::338535913256:role/tm_logger_lambda_exec_role --handler index.tm_logger_handler --mode event --description "tm lambda to dynamo" --timeout 60
echo "Done"



#aws lambda update-function-configuration --function-name tmLoggerToDynamo --handler tm_logger_handler
