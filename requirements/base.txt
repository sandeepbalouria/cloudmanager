Django

# ----- for Django Rest Framework
djangorestframework
Markdown
django-filter
django-jsonfield

django-announcements
django-appconf
django-compressor
django-debug-toolbar
django-email-confirmation
django-forms-bootstrap
django-htmlmin<=0.5.2
django-notification
django-pagination
django-ses
django-staticfiles
django-timezones
django-tokenapi

# ----- For debug-toolbar
sqlparse

# ----- Model migrations
South

# ----- for django-user-account app
six

PyJWT
PyYAML
argparse

# ----- AWS
boto
boto3

# ----- Databases
psycopg2
redis
SQLAlchemy

# ----- Async tasks
celery
#?django-celery

# ----- Rabbit message broker
amqp

salt==2014.7.2

# ----- tools
filechunkio
lxml
pytz==2012d
requests
email-reply-parser
uwsgi
httplib2

# ----- HDFS
pywebhdfs

# ----- Code
pep8
pylint
pylint-django
pylint-plugin-utils
